export const environment = {
  production: true,
  // api_url: 'https://joblitedemo.co.in/jcjoblite/api/',
  // baseUrl: 'https://joblitedemo.co.in/jcjoblite/api/',
  // server_url: 'https://joblitedemo.co.in/jcjoblite/',  
  api_url: 'https://admin.joblite.co.za/api/',
  baseUrl: 'https://admin.joblite.co.za/api/',
  server_url: 'https://admin.joblite.co.za/', 
  // callback_url: 'https://joblite-angular.vercel.app/',
  callback_url: 'https://www.joblite.co.za/',
  search_limit: 10, 
  page_counts: 5,  
};
