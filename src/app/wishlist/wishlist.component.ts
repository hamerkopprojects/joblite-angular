import { Component, OnInit,  ViewChild, ElementRef  } from '@angular/core';
import { ApiService } from '../_services/commonlisting/api.service';
import { Router } from '@angular/router';
import { CategoriesService} from './../_services/categories/categories.service'; 
import { ActivatedRoute } from "@angular/router";
import { environment } from 'src/environments/environment';
declare var jQuery:any;

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {
  jobCatagoryList = [];
  loggedIn: boolean; 
  settingsbody: any;
  popUpJobDetails: any;
  popUpi: any;
  status:any;
  jobId: any;
  count: any;
  dynamicLoginMessage: string;
  @ViewChild('myDivPopUp') myDivPopUp: ElementRef<HTMLElement>;

  constructor(private router: Router,
    private category: CategoriesService,
    private apiService: ApiService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    if(typeof localStorage.token !== 'undefined') {
      this.loggedIn = true;
    } else {
      this.loggedIn = false;      
    }  
    this.otherSettingsList();     
    this.favoriteJobs();    
  }

  otherSettingsList() {
    //this.SpinnerService.show();
    this.category.getOtherSettings().subscribe((res) => {
      this.settingsbody = res.other;
     // this.redirectTo('/');
    }, error => {
      //this.redirectTo('/');
    }
    );
  }

  createRange(number: any){
    if (isNaN(number)) {
      number = 0;
    } 
    return new Array(number);
  }

  //Get jobdeatails
  favoriteJobs() {
    this.apiService.favoriteJobs(localStorage.user_id)
      .subscribe(
        (data: any) => {
          console.log(data.data.resp);
          data.data.resp.forEach((value: any, key: any) => {
                            // } else {
            if (data.data.resp[key].job_intimation[0]?.status == 'donot_show') {
              data.data.resp.splice(key,1);
            }
          });            
          this.jobCatagoryList = data.data.resp;
          this.jobCatagoryList.forEach((value: any, key: any) => {
              value['cat_image_path']= environment.server_url+'uploads/job_category/' +value['cat_image_path'];
          }); 
        
        },
        error => {
          console.log(error);
        }
      );
  } 
  
    //Go to jog details page
    // doGotoJobDetailsPage(jobid){
    //   this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobid }});
    //   } 
    doGotoJobDetailsPage(job: any,i: any){
      this.popUpJobDetails = job;
      this.popUpi = i;
    }
      
      closeJob(jobId: any, i: any) {
        if (!this.loggedIn) {
          jQuery("[data-target='#myModal_login']").click();
        } else {
          this.jobCatagoryList.splice(i,1);
          this.apiService.closeJob(jobId,localStorage.user_id)
          .subscribe(
            (data: any) => {
            },
            error => {
              console.log(error);
            }
          );        
        }
      }
      
      acceptJob(jobId: any, i: any) {
        if (!this.loggedIn) {
          jQuery("[data-target='#myModal_login']").click();
        } else {
          if (this.jobCatagoryList[i].job_intimation[0]?.status == null || typeof this.jobCatagoryList[i].job_intimation[0] === 'undefined') {
            if(typeof this.jobCatagoryList[i].job_intimation[0] === 'undefined')    {
              this.jobCatagoryList[i].job_intimation[0] = [];
            }           
            this.jobCatagoryList[i].job_intimation[0].status = 'initiated';
            this.apiService.acceptJob(jobId,localStorage.user_id)
            .subscribe(
              (data: any) => {
              },
              error => {
                console.log(error);
              }
            );          
          }
        }
      }    
  
      addToWishList(jobId: any, i: any) {
        let is_favourite: string;
        if (!this.loggedIn) {
          jQuery("[data-target='#myModal_login']").click();
        } else {
          if (this.jobCatagoryList[i].job_intimation[0]?.is_jobbers_favourite == 'favourite') {
            if(typeof this.jobCatagoryList[i].job_intimation[0] === 'undefined')    {
              this.jobCatagoryList[i].job_intimation[0] = [];
            }           
            this.jobCatagoryList[i].job_intimation[0].is_jobbers_favourite = 'unfavourite';
            is_favourite = 'unfavourite';
            this.jobCatagoryList.splice(i,1);            
          } else {
            if(typeof this.jobCatagoryList[i].job_intimation[0] === 'undefined')    {
              this.jobCatagoryList[i].job_intimation[0] = [];
            }          
            this.jobCatagoryList[i].job_intimation[0].is_jobbers_favourite = 'favourite';          
            is_favourite = 'favourite';
          }
          this.popUpJobDetails = this.jobCatagoryList[i];
          this.apiService.addToWishList(jobId,localStorage.user_id,is_favourite)
          .subscribe(
            (data: any) => {
            },
            error => {
              console.log(error);
            }
          ); 
        }
      }  


      homeConfirmation(jobId: any, count: any,clickStausType:any,currentStatus:any, currentLocation:any) {
        let dataStorage: any = [jobId,count,clickStausType,currentStatus];
    
        if (currentStatus != 'initiated' || clickStausType == 'close' || clickStausType == 'like' || clickStausType == 'accept') {
    
          this.jobId = jobId;
          this.count = count;
          this.status = clickStausType;
          clickStausType == 'close' ? jQuery('#confirmationHomeText').html("Do you want to close?") : jQuery('#confirmationHomeText').html("Do you want to accept?");
          if(clickStausType == 'like'){
            if(this.jobCatagoryList[count].job_intimation[0]?.is_jobbers_favourite == 'favourite'){
              jQuery('#confirmationHomeText').html("Do you want to remove this item from favourits?")
            }
            else{
              jQuery('#confirmationHomeText').html("Do you want to add this item to favourits?")
    
            }  
          } 
          // let el: HTMLElement = this.myDivPopUp.nativeElement;
          // el.click();
          if (this.loggedIn) {      
            if (clickStausType == 'like')   {
              this.addToWishList(this.jobId,this.count);
            } else {
              jQuery('#confirmationHomePopup').modal('show');
            }
          } else {
            localStorage.setItem("jobDetailsjobId",jobId);
            if (clickStausType == 'accept') {
              this.dynamicLoginMessage = 'In order to apply this lite job you need to Log In / Sign Up';
            }
            if (clickStausType == 'close') {
              this.dynamicLoginMessage = 'In order to close this lite job you need to Log In / Sign Up';          
            }
            if (clickStausType == 'like') {
              this.dynamicLoginMessage = 'In order to shortlist/taken off from shortlist this lite job you need to Log In / Sign Up';      
            }    
           
            jQuery("#mymodalsignupmain").click();     
          }
        }
      }
  
      homeConfirmationClick() {
        // if (!this.loggedIn) {
        //   jQuery("#mymodalsignup").click();
        // } else {
    
          jQuery('#confirmationHomePopup').modal('hide');
    
          if (this.status == 'accept') {
            this.acceptJob(this.jobId,this.count)
          }
          else if (this.status == 'close') {
            this.closeJob(this.jobId,this.count);
          }
          else if(this.status  == 'like') {
            this.addToWishList(this.jobId,this.count);
          }
    //    }    
      }

}
