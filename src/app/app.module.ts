import { BrowserModule } from '@angular/platform-browser';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

//import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpclientService } from './_services/wrapper/httpclient.service';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
//import { HomeModule } from './home/home.module';
import { NgxSpinnerModule } from "ngx-spinner";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { APP_ROUTES } from './app.routes';
import { ProfileComponent } from './profile/profile.component';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatNativeDateModule} from '@angular/material/core';
import {MatSliderModule} from '@angular/material/slider'
import { FileUploadModule } from 'ng2-file-upload';
import { JobberprofileComponent } from './jobberprofile/jobberprofile.component';
import { AgmCoreModule } from '@agm/core';
import { HeaderloginComponent } from './headerlogin/headerlogin.component';
import { VerifyComponent } from './verify/verify.component';
import { UsersuccessComponent } from './usersuccess/usersuccess.component';
import { HeaderlogoutComponent } from './headerlogout/headerlogout.component';
import { FooterComponent } from './footer/footer.component';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { JobpostComponent } from './jobpost/jobpost.component';
import { LabelModule } from '@progress/kendo-angular-label';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { MyjobpostsComponent } from './myjobposts/myjobposts.component';
import { ToIsoPipe } from './app.dateformat';
import { JobdetailsComponent } from './jobdetails/jobdetails.component';
import { JobcategoriesComponent } from './jobcategories/jobcategories.component';
import { JobcategorieslistComponent } from './jobcategorieslist/jobcategorieslist.component';
import { MyjobsComponent } from './myjobs/myjobs.component';
import { JobapplicantsComponent } from './jobapplicants/jobapplicants.component';
import { ApplicantdetailsComponent } from './applicantdetails/applicantdetails.component';
import { JobbermytaskComponent } from './jobbermytask/jobbermytask.component';
import { PersonelinfoComponent } from './personelinfo/personelinfo.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { TasklistComponent } from './tasklist/tasklist.component';
import { MyjobberjobsComponent } from './myjobberjobs/myjobberjobs.component';
import { JobberatortasklistComponent } from './jobberatortasklist/jobberatortasklist.component';
import { SkiplocationComponent } from './skiplocation/skiplocation.component';
import { NotificationComponent } from './notification/notification.component';
import { MyjobslinkComponent } from './myjobslink/myjobslink.component';
import { PaycallbackComponent } from './paycallback/paycallback.component';
import { PaycallbackjobstartComponent } from './paycallbackjobstart/paycallbackjobstart.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { TermsandconditionsComponent } from './termsandconditions/termsandconditions.component';
import { ScopeincreaseComponent } from './scopeincrease/scopeincrease.component';
import { NewadvertComponent } from './newadvert/newadvert.component';
import { RenewadvertComponent } from './renewadvert/renewadvert.component';
import { ContinueapplicantComponent } from './continueapplicant/continueapplicant.component';
import { PaycallbacktipComponent } from './paycallbacktip/paycallbacktip.component';
import { PaycallbackidComponent } from './paycallbackid/paycallbackid.component';
import { PaycallbacklicenseComponent } from './paycallbacklicense/paycallbacklicense.component';
import { LinkedinloginComponent } from "./linkedinlogin/linkedinlogin.component";
import { ToastrModule } from 'ngx-toastr';
import {
  FacebookLoginProvider,
  SocialLoginModule,
  SocialAuthServiceConfig,
  GoogleLoginProvider,
} from 'angularx-social-login';

const googleLoginOptions = {
  scope: 'profile email',
  plugin_name:'login' //you can use any name here
}; 

@NgModule({
  declarations: [
    AppComponent,
    ToIsoPipe,
    HomeComponent,
    ProfileComponent,
    JobberprofileComponent,
    HeaderloginComponent,
    VerifyComponent,
    HeaderlogoutComponent,
    UsersuccessComponent,
    FooterComponent,
    JobpostComponent,
    MyjobpostsComponent,
    JobdetailsComponent,
    JobcategoriesComponent,
    JobcategorieslistComponent,
    MyjobsComponent,
    JobapplicantsComponent,
    ApplicantdetailsComponent,
    JobbermytaskComponent,
    PersonelinfoComponent,
    WishlistComponent,
    TasklistComponent,
    MyjobberjobsComponent,
    JobberatortasklistComponent,
    SkiplocationComponent,
    NotificationComponent,
    MyjobslinkComponent,
    PaycallbackComponent,
    PaycallbackjobstartComponent,
    PrivacypolicyComponent,
    TermsandconditionsComponent,
    ScopeincreaseComponent,
    NewadvertComponent,
    RenewadvertComponent,
    ContinueapplicantComponent,
    PaycallbacktipComponent,
    PaycallbackidComponent,
    PaycallbacklicenseComponent,
    LinkedinloginComponent
  ],
  imports: [
    BrowserModule,
    InfiniteScrollModule,
    //CommonModule,
    //HomeModule,
    NgxSpinnerModule ,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule, 
    ReactiveFormsModule,
    SocialLoginModule,
    HttpClientModule,    
    RouterModule.forRoot(APP_ROUTES),
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatSliderModule,        
    FileUploadModule,
    HttpClientJsonpModule, 
    DropDownsModule,
    LabelModule,
    InputsModule,       
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyCZ-Zb3HafjrZEti-SzMtp5BjhAWOAjwGI',
    //   libraries: ['places']
    // })
  ],
  providers: [
    HttpclientService,
    // {provide: LocationStrategy, useClass: HashLocationStrategy},
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '722652052077-jfevith1ahkik2tu32mf7ea1o3l2cgbb.apps.googleusercontent.com',
              googleLoginOptions
            )
          },          
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('625828118711208'),
          },
        ],
      } as SocialAuthServiceConfig,
    },        
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
