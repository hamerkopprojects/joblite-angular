import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobberprofileComponent } from './jobberprofile.component';

describe('JobberprofileComponent', () => {
  let component: JobberprofileComponent;
  let fixture: ComponentFixture<JobberprofileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobberprofileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobberprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
