import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UsersuccessComponent } from './usersuccess/usersuccess.component';
import { ProfileComponent } from './profile/profile.component';
import {JobberprofileComponent } from './jobberprofile/jobberprofile.component';
import {JobpostComponent } from './jobpost/jobpost.component';
import {MyjobpostsComponent } from './myjobposts/myjobposts.component';
import {HeaderloginComponent } from './headerlogin/headerlogin.component';
import {HeaderlogoutComponent } from './headerlogout/headerlogout.component';
import {FooterComponent } from './footer/footer.component';
import {VerifyComponent } from './verify/verify.component';
import {JobdetailsComponent } from './jobdetails/jobdetails.component';
import { JobcategoriesComponent } from './jobcategories/jobcategories.component';
import { JobcategorieslistComponent } from './jobcategorieslist/jobcategorieslist.component';

import {MyjobsComponent } from './myjobs/myjobs.component';
import {MyjobberjobsComponent } from './myjobberjobs/myjobberjobs.component';
import {JobapplicantsComponent } from './jobapplicants/jobapplicants.component';
import {ApplicantdetailsComponent } from './applicantdetails/applicantdetails.component';
import {JobbermytaskComponent } from './jobbermytask/jobbermytask.component';
import {PersonelinfoComponent } from './personelinfo/personelinfo.component';
import {WishlistComponent } from './wishlist/wishlist.component';
import { TasklistComponent } from './tasklist/tasklist.component';
import { JobberatortasklistComponent } from './jobberatortasklist/jobberatortasklist.component';
import { SkiplocationComponent } from './skiplocation/skiplocation.component';
import { NotificationComponent } from './notification/notification.component';
import { MyjobslinkComponent } from './myjobslink/myjobslink.component';
import { PaycallbackComponent } from './paycallback/paycallback.component';
import { PaycallbackidComponent } from './paycallbackid/paycallbackid.component';
import { PaycallbacklicenseComponent } from './paycallbacklicense/paycallbacklicense.component';
import { PaycallbacktipComponent } from './paycallbacktip/paycallbacktip.component';
import { PaycallbackjobstartComponent } from './paycallbackjobstart/paycallbackjobstart.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { TermsandconditionsComponent } from './termsandconditions/termsandconditions.component';
import { ScopeincreaseComponent } from './scopeincrease/scopeincrease.component';
import { NewadvertComponent } from './newadvert/newadvert.component';
import { RenewadvertComponent } from './renewadvert/renewadvert.component';
import { ContinueapplicantComponent } from './continueapplicant/continueapplicant.component';
import { LinkedinloginComponent } from "./linkedinlogin/linkedinlogin.component";
import { AuthGuard } from './_guard/auth.guard';


export const APP_ROUTES: Routes = [
    {path: '', component: HomeComponent},
    {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
    {path: 'jobdetails', component: JobdetailsComponent},
    {path: 'jobcategory', component: JobcategoriesComponent},
    {path: 'jobcategory-listing', component: JobcategorieslistComponent},
    {path: 'jobberprofile', component: JobberprofileComponent, canActivate: [AuthGuard]},
    {path: 'jobpost', component: JobpostComponent, canActivate: [AuthGuard]}, 
    {path: 'myjobposts', component: MyjobpostsComponent, canActivate: [AuthGuard]},     
    {path: 'jobpost/:id', component: JobpostComponent, canActivate: [AuthGuard]}, 
    {path: 'jobpost/:id/:renew', component: JobpostComponent, canActivate: [AuthGuard]}, 
    {path: 'headerlogin', component: HeaderloginComponent, canActivate: [AuthGuard]}, 
    {path: 'headerlogout', component: HeaderlogoutComponent}, 
    {path: 'footer', component: FooterComponent}, 
    {path: 'usersuccess', component: UsersuccessComponent},
    {path: 'verify/:code', component: VerifyComponent},
    {path: 'myjobs', component: MyjobsComponent, canActivate: [AuthGuard]},  
    {path: 'myjobberjobs', component: MyjobberjobsComponent, canActivate: [AuthGuard]}, 
    {path: 'myjobs/:tab', component: MyjobsComponent, canActivate: [AuthGuard]},  
    {path: 'myjobberjobs/:tab', component: MyjobberjobsComponent, canActivate: [AuthGuard]},        
    {path: 'jobapplicants/:id', component: JobapplicantsComponent, canActivate: [AuthGuard]},
    {path: 'applicantdetails/:id', component: ApplicantdetailsComponent, canActivate: [AuthGuard]}, 
    {path: 'jobbermytask', component: JobbermytaskComponent, canActivate: [AuthGuard]},
    {path: 'personelinfo', component: PersonelinfoComponent, canActivate: [AuthGuard]}, 
    {path: 'applicantinfo/:id', component: PersonelinfoComponent, canActivate: [AuthGuard]},     
    {path: 'wishlist', component: WishlistComponent, canActivate: [AuthGuard]}, 
    {path: 'tasklist/:id', component: TasklistComponent}, 
    {path: 'jobberatortasklist/:id', component: JobberatortasklistComponent},
    {path: 'skiplocation' , component: SkiplocationComponent},
    {path: 'notification' , component: NotificationComponent},
    {path: 'myjobslink' , component: MyjobslinkComponent},
    {path: 'paycallback/:id/:planid/:type', component: PaycallbackComponent},
    {path: 'paycallbackjobstart/:id/:custid/:status', component: PaycallbackjobstartComponent},
    {path: 'paycallbacktip/:id', component: PaycallbacktipComponent}, 
    {path: 'paycallbackid/:id/:firstname/:surname/:idcardnumber/:type', component: PaycallbackidComponent},
    {path: 'paycallbacklicense/:id/:firstname/:surname/:idcardnumber/:type', component: PaycallbacklicenseComponent},   
    {path: 'privacy-policy' , component: PrivacypolicyComponent},
    {path: 'terms-and-conditions' , component: TermsandconditionsComponent},
    {path: 'scopeincrease/:id' , component: ScopeincreaseComponent},
    {path: 'newadvert/:id' , component: NewadvertComponent}, 
    {path: 'continueapplicant/:id' , component: ContinueapplicantComponent},
    {path: 'renewadvert/:id' , component: RenewadvertComponent},  
    {path: 'linkedinlogin', component: LinkedinloginComponent },
    {path: '**', redirectTo: '' } // Wildcard route for redirection                   
];
