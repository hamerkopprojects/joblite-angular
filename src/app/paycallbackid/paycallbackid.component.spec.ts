import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaycallbackidComponent } from './paycallbackid.component';

describe('PaycallbackidComponent', () => {
  let component: PaycallbackidComponent;
  let fixture: ComponentFixture<PaycallbackidComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaycallbackidComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaycallbackidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
