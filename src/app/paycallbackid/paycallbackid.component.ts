import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../_services/commonlisting/api.service';
import { JobpostcallsService} from '../_services/jobpostcalls/jobpostcalls.service'; 
import { NotificationService } from './../_services/notification.service';
import { JobberprofileService } from './../_services/jobberprofile/jobberprofile.service';

declare var jQuery: any;

@Component({
  selector: 'app-paycallbackid',
  templateUrl: './paycallbackid.component.html',
  styleUrls: ['./paycallbackid.component.css']
})
export class PaycallbackidComponent implements OnInit {
  payment: boolean;
  titlemessage: string;
  constructor(
    private jobpostcalls: JobpostcallsService,
    private jobber: JobberprofileService,
    private notifyService : NotificationService,        
    private apiService: ApiService,     
    private router: Router,
    private route: ActivatedRoute    
  ) { }

  ngOnInit(): void {
    this.payment = true;
    this.titlemessage = '';
    //alert(this.route.snapshot.params.id);  
    //alert(this.route.snapshot.queryParamMap.get('id'));

    var data = {
      "id" : this.route.snapshot.queryParamMap.get('id')
    }    
    this.jobpostcalls.paymentCheck(data).subscribe((res) => {
      console.log(res.data);
      if (res.data.result.code == "000.100.110") {
        var data = {
          "customerId" : localStorage.getItem('user_id'),
          "jobpost_id" : this.route.snapshot.params.id,
          "cust_id" : this.route.snapshot.params.custid,
          "payment_id" : res.data.id,
          "payment_type" : res.data.paymentType,
          "payment_brand" : res.data.paymentBrand,  
          "amount" : res.data.amount, 
          "currency" : res.data.currency, 
          "descriptor" : res.data.descriptor, 
          "code" : res.data.result.code,   
          "result" : res.data.result, 
          "customer" : res.data.customer, 
          "card" : res.data.card, 
          "custom_parameters" : res.data.customParameters,  
          "buildNumber" : res.data.buildNumber, 
          "ndc" : res.data.ndc, 
          "payment_time" : res.data.timestamp, 
          "entire_response" : res.data                                                                    
        }    
        this.donePayment(data);
      } else {
        this.redirectTo('/'+this.route.snapshot.params.type);    
      }

    }, error => {
      console.log('error');
      console.log(error);
      //this.redirectTo('/');
    }
    ); 
      
  }

  donePayment(data:any) {

    this.apiService.idpay(data).subscribe((response: any) => {
      //this.redirectTo('myjobs');  
      // let dataver = {
      //   "user" : {
      //     "firstName" : this.route.snapshot.params.firstname,
      //     "lastName" : this.route.snapshot.params.surname,
      //     "identifier" : this.route.snapshot.params.idcardnumber
      //   }
      // } 
      let dataver = {
          "firstName" : this.route.snapshot.params.firstname,
          "lastName" : this.route.snapshot.params.surname,
          "identifier" : this.route.snapshot.params.idcardnumber
      }
      this.jobber.idverifylatest(dataver).subscribe((resver) => {
        let idcard_verify: any;
        localStorage.setItem('user_type_click','linkedin');
        if (resver.data.code == 'GOOD') {
          idcard_verify = 'yes';
          let dataver = {
            "customerId" : localStorage.getItem('user_id'),
            "idcardnumber"  : this.route.snapshot.params.idcardnumber, 
            "idcard_verify" : idcard_verify           
          }         
          this.jobber.idcardupdate(dataver).subscribe((resver) => {
            jQuery('#idPaySuccessPopupStart').modal('show');  
              
      
          }, error => {
            jQuery('#idPayFailurePopupStart').modal('show'); 
          }
          );
        } else {
          idcard_verify = 'no';
          jQuery('#idPayFailurePopupStart').modal('show'); 
        }

            
      }, error => {
        jQuery('#idPayFailurePopupStart').modal('show');  
      }
      ); 
     
     },
  
       error => {
        this.redirectTo('/'+this.route.snapshot.params.type);    
       }
     );
  } 

  showToasterSuccess(){
    this.notifyService.showSuccess(this.titlemessage, "")
  }
  
  showToasterError(){
      this.notifyService.showError(this.titlemessage, "")
  }
  
  showToasterInfo(){
      this.notifyService.showInfo(this.titlemessage, "")
  }
  
  showToasterWarning(){
      this.notifyService.showWarning(this.titlemessage, "")
  }

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;
  } 
  
  confirmationListener(value:any) {
    this.payment = false;   
    jQuery('.modal-backdrop').remove(); 
    jQuery('#idPaySuccessPopupStart').modal('hide');
    // jQuery('#tipPaySuccessPopupStart').modal('hide');
    jQuery('#exampleModalTip').modal('hide');
    this.redirectTo('/'+this.route.snapshot.params.type);                  
   }

}
