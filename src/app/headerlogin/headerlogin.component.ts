import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JobberatorprofileService } from './../_services/jobberatorprofile/jobberatorprofile.service';
import { ApiService } from '../_services/commonlisting/api.service';
import { environment } from 'src/environments/environment';
import { NotificationService } from './../_services/notification.service';

declare var jQuery: any;

@Component({
  selector: 'app-headerlogin',
  templateUrl: './headerlogin.component.html',
  styleUrls: ['./headerlogin.component.css']
})
export class HeaderloginComponent implements OnInit {
  @Output() confirmationModal : EventEmitter<string> = new EventEmitter();
  titlemessage: string;
  userType : string; 
  userName: string;
  userEmail: string;
  image:any;
  getImageUrl :any;
  ImageUrl :any;
  notificationsList: any;
  jobTypenotificationsList: any;
  jobTypeFilternotificationsList: any;  
  noticount: any;  
  dispMessage: any; 
  messageEmpty: boolean;
  selectMessageEmpty: boolean;
  cancellationData: any = [];
  reasonselected: any;
  location: any;
  maplocation: any;
  loccoordinates: string;
  isPost: string;
  constructor( 
    private jobberator: JobberatorprofileService,    
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private notifyService : NotificationService) { }

  ngOnInit(): void {
    this.loccoordinates = localStorage.getItem('loccoordinates');
    this.maplocation = localStorage.getItem('maplocation'); 
    this.titlemessage = '';
    window.onscroll = function() {myFunction()};
    
    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;
    
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }       
    this.reasonselected = '';
    this.dispMessage = '';
    this.messageEmpty = false;
    this.selectMessageEmpty = false;
    this.userType = '';
    this.userName = '';
    this.userEmail = '';
    this.noticount = ''; 
    this.location = '';  
    this.ImageUrl='assets/images/Default_Image_Thumbnail.jpg'; 
    this.customerDetails(); 
    this.cancellationReasons();    

    var data = {
      "customers_id": localStorage.getItem('user_id')
    }
    this.jobberator.jobberatorStepOneDetails(data).subscribe((res) => {
      this.isPost =  res?.data?.jobberator?.tab1;
    }, error => {
      //this.redirectTo('/');
    }
    );
  }

  showToasterSuccess(){
    this.notifyService.showSuccess(this.titlemessage, "")
  }
  
  showToasterError(){
      this.notifyService.showError(this.titlemessage, "")
  }
  
  showToasterInfo(){
      this.notifyService.showInfo(this.titlemessage, "")
  }
  
  showToasterWarning(){
      this.notifyService.showWarning(this.titlemessage, "")
  }

  reasonSelected(selected: any) {
    this.reasonselected = selected;
  }

  cancellationReasons() {
    this.apiService.cancellationReasons().subscribe((response: any) => {
      response.data.cancellationreasons.forEach((value: any, key: any) => {
        //this.languageData.push(value.lang[0].name);
        this.cancellationData[value.lang[0].autosuggest_id] = value.lang[0].name;
      });   
    },

      error => {
        //this.redirectTo('/');
      }
    );

  }  

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user_id');
    localStorage.removeItem('user_type');
    localStorage.removeItem('localJobPostId');
    localStorage.removeItem('localplanchange');
    localStorage.removeItem('maplocation');
    localStorage.removeItem('user_type_click');
    localStorage.removeItem('user_email');
    localStorage.removeItem('linked_verify_back_page');
    this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
    // this.redirectTo('/');
    location.href = '/';
    return false;
  }

  notificationViewMyPage(notificationLogId: any,url: any){
    var data = {
      "job_intimation_log_id" : notificationLogId
    }
    this.jobberator.notificationView(data).subscribe((res) => {
      this.redirectTo(url);
      return false;
    }, error => {
      //this.redirectTo('/');
    }
    );    
  }

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;
  }

  profileChange(type: string) {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }    
    this.jobberator.profileChange(data).subscribe((res) => {
      localStorage.setItem('user_type','both');
      this.redirectTo(type);
    }, error => {
      //this.redirectTo('/');
    }
    );  
  }  

  notificationList() {
    var param = {
      "customers_id" : localStorage.getItem('user_id'),
      "user_type" : this.userType
    }      
    this.apiService.notificationList(param)
    .subscribe(
      (data: any) => {
        this.noticount = data.data.count;
        this.jobTypenotificationsList = data.data.jobtypedata;
        this.jobTypenotificationsList.forEach((value: any, key: any) => {
          this.jobTypenotificationsList[key]['username'] = value['cust_details']['first_name']+" "+value['cust_details']['last_name']; 
          this.jobTypenotificationsList[key]['comment'] = value['status']+" job type "+value['other'];          
          // if (value['cust_details']['profile_img'] == null) {
            this.jobTypenotificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
          // } else {
          //   this.jobTypenotificationsList[key]['profile_img'] = environment.server_url+'' +value['cust_details']['profile_img'];
          // }           
        }); 
        
        this.jobTypeFilternotificationsList = data.data.jobtypeprofiledata;
        this.jobTypeFilternotificationsList.forEach((value: any, key: any) => {
          this.jobTypeFilternotificationsList[key]['username'] = value['cust_details']['first_name']+" "+value['cust_details']['last_name']; 
          this.jobTypeFilternotificationsList[key]['comment'] = value['status']+" job type "+value['other'];          
          // if (value['cust_details']['profile_img'] == null) {
            this.jobTypeFilternotificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
          // } else {
          //   this.jobTypenotificationsList[key]['profile_img'] = environment.server_url+'' +value['cust_details']['profile_img'];
          // }           
        });

                          
        this.notificationsList = data.data.notification_info;
        this.notificationsList.forEach((value: any, key: any) => {
          //this.respbody[key]['details'][0]['image_path'] = environment.server_url+'uploads/job_category/' +this.respbody[key]['details'][0]['image_path']; 

          if (value['status'] == 'jobberator_accepted') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" accepted your job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }              
            }              
          }

          if (value['status'] == 'started') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " started job";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }              
            }              
          } 
                    
          if (value['status'] == 'jobberator_rejected') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" rejected your job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }              
            }              
          } 
          if (value['status'] == 'jobberator_approved') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" approved your job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }              
            }              
          }
          if (value['status'] == 'jobberator_disapproved') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" disapproved your job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }              
            }              
          } 
          if (value['status'] == 'jobberator_disputed') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" disputed your job done";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }              
            }              
          }                         
          if (value['status'] == 'jobber_accepted') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobber_details']['last_name']+" accepted your job accept";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              if (value['jobber_details']['user_type'] == 'normal' || value['jobber_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobber_details']['user_type'] == 'linkedin' || value['jobber_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img']+'?rand='+x;
                }
              }               
            }              
          }
          if (value['status'] == 'jobber_rejected') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = value['jobber_details']['last_name']+" rejected your job accept";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              if (value['jobber_details']['user_type'] == 'normal' || value['jobber_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobber_details']['user_type'] == 'linkedin' || value['jobber_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img']+'?rand='+x;
                }
              }               
            }              
          }  
          if (value['status'] == 'jobber_disputed') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = value['jobber_details']['last_name']+" disputed your job done";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              if (value['jobber_details']['user_type'] == 'normal' || value['jobber_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobber_details']['user_type'] == 'linkedin' || value['jobber_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img']+'?rand='+x;
                }
              }               
            }              
          }

          if (value['status'] == 'initiated') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = " applied";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              if (value['jobber_details']['user_type'] == 'normal' || value['jobber_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobber_details']['user_type'] == 'linkedin' || value['jobber_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img']+'?rand='+x;
                }
              }               
            }              
          } 

          if (value['status'] == 'rated' && value['user_type'] == 'jobber') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = " rated";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              if (value['jobber_details']['user_type'] == 'normal' || value['jobber_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobber_details']['user_type'] == 'linkedin' || value['jobber_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img']+'?rand='+x;
                }
              }               
            }              
          }  
          
          if (value['status'] == 'rated' && value['user_type'] == 'jobberator') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];              
            this.notificationsList[key]['comment'] = " rated";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              } else {
                // this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'];
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }               
            }              
          } 
          
          
                      
          if (value['status'] == 'started' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobber started task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobberator_disputed' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobberator disputed task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobber_disputed' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobber disputed task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobberator_rejected' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobberator rejected task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobberator_accepted' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobberator accepted task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobber_accepted' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobber accepted task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobber_rejected' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobber rejected task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'completed' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobberator completed task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobberator_canceled' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobberator canceled task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobber_accept_cancellation' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobber accepted cancellation task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobber_cant_reach_agreement' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobber coudn't reach agreement for the task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobberator_accepted_cant_reach' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobberator accepted can't reach task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobberator_job_canceled' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobberator cancelled job";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }

          if (value['status'] == 'jobber_accepted_job_cancellation' && value['task_id'] !== '') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " jobber accepted job cancellation task";
            this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
          }
                   
          
        });  
        this.notificationsList = this.notificationsList.splice(0,10);      
      },
      error => {

      }
    );    
  }  

  customerDetails() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobberator.jobberatorStepOneDetails(data).subscribe((res) => {
      this.location = res.data.customerInfo.location;
      this.userType = res.data.customerInfo.type;
      this.userName = res.data.customerInfo.name;
      this.userEmail = res.data.customerInfo.email;
      this.image = environment.server_url + res.data.customerInfo.profile_img;
      this.getImageUrl = res.data.customerInfo.profile_img;
      if(this.getImageUrl != null && this.getImageUrl != '' && this.getImageUrl != 'null')
      {
          // this.ImageUrl = environment.server_url + res.data.customerInfo.profile_img;     
          if (res.data.customerInfo.user_type == 'normal' || res.data.customerInfo.profile_img.includes('uploads/user_profile/')) {
            this.ImageUrl = environment.server_url + res.data.customerInfo.profile_img;  
          } else {
            let x = Math.floor((Math.random() * 10000) + 1);
            // Remove backslashes from the image URL
            if (res.data.customerInfo.user_type == 'linkedin' || res.data.customerInfo.user_type == 'facebook') {
              this.ImageUrl = res.data.customerInfo.profile_img.replace(/\//g, '/');
            } else {
              this.ImageUrl = res.data.customerInfo.profile_img+'?rand='+x;
            }
          }           
      }
      else{
        this.ImageUrl='assets/images/Default_Image_Thumbnail.jpg';  
      }
      this.notificationList();

    }, error => {
      //this.redirectTo('/');
    }
    );
  } 
  
  onConfirmationCancel(type: any) {
    this.confirmationModal.emit(type);
  }    

  onConfirmationClickDispute() {
    if (this.dispMessage == '' || this.reasonselected == '') {
      if (this.dispMessage == '') {
        this.messageEmpty = true; 
      } else {
        this.messageEmpty = false; 
      }
      if (this.reasonselected == '') {
        this.selectMessageEmpty = true;          
      } else {
        this.selectMessageEmpty = false; 
      }
      return false;
    } else {
      this.messageEmpty = false; 
      this.selectMessageEmpty = false;             
      this.confirmationModal.emit(this.reasonselected+'$^&*#@'+this.dispMessage);
    }
  }

  paymentConfirm(type: any) {
    jQuery('#tipPaySuccessPopupStart').modal('hide');    
    this.confirmationModal.emit(type);
  } 
  
  idpaymentConfirm(type: any) {
    jQuery('#idPaySuccessPopupStart').modal('hide');    
    this.confirmationModal.emit(type);
  }


  onConfirmationClickDisputeJobber() {
    if (this.dispMessage == '' ) {
        this.messageEmpty = true; 
      return false;
    } else {
      this.messageEmpty = false; 
    
      this.confirmationModal.emit(this.dispMessage);
    }
  }  

  choiceOnDisputeFinal(type: any) {
    this.confirmationModal.emit(type);
  }   

  onConfirmationClick() {
      this.confirmationModal.emit(this.dispMessage);
  }

  reloadJobberatorScreen() {
    jQuery('#confirmationPopup').modal('hide');
    jQuery('#confirmationPopupNotStart').modal('hide');
    jQuery('#otherPopup').modal('hide');
    //this.redirectTo('/myjobs/t2'); 
    this.confirmationModal.emit('activereload');   
  }

  //Go to jog details page
  doGotoJobDetailsPage(jobid: any){
    this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobid }});
    return false;
  } 

  redirectToMyjobs() {
    jQuery('#otherPopup').modal('hide');     
    this.redirectTo('/myjobs');   
  }
  
  //notification view
  notificationView(jobid: any, notificationLogId: any, link: string){
    var data = {
      "job_intimation_log_id" : notificationLogId
    }
    this.jobberator.notificationView(data).subscribe((res) => {
      //this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobid }});
      this.router.navigate([''+link+'']);
      return false;
    }, error => {
      //this.redirectTo('/');
    }
    );    
  }
  
    //notification view task
    notificationViewTask(jobid: any, notificationLogId: any, link: string){
      var data = {
        "job_intimation_log_id" : notificationLogId
      }
      this.jobberator.notificationViewTask(data).subscribe((res) => {
        // this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobid }});
        this.router.navigate([''+link+'']);
        return false;
      }, error => {
        //this.redirectTo('/');
      }
      );    
    }
  
  jobtypeNotificationView(jobid: any, notificationLogId: any){
    var data = {
      "job_intimation_log_id" : notificationLogId
    }
    this.jobberator.jobtypeNotificationView(data).subscribe((res) => {
      this.router.navigate(['/jobpost/'+jobid]);
      return false;
    }, error => {
      //this.redirectTo('/');
    }
    );    
  }  
  
  jobtypeProfileNotificationView(notificationLogId: any){
    var data = {
      "job_intimation_log_id" : notificationLogId
    }
    this.jobberator.jobtypeProfileNotificationView(data).subscribe((res) => {
      this.router.navigate(['/jobberprofile']);
      return false;
    }, error => {
      //this.redirectTo('/');
    }
    );    
  }  

}
