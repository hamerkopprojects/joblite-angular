import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JobpostcallsService} from './../_services/jobpostcalls/jobpostcalls.service'; 
declare var jQuery: any;

@Component({
  selector: 'app-paycallback',
  templateUrl: './paycallback.component.html',
  styleUrls: ['./paycallback.component.css']
})
export class PaycallbackComponent implements OnInit {
  payment: boolean;
  constructor(
    private jobpostcalls: JobpostcallsService,      
    private router: Router,
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.payment = true;
    //alert(this.route.snapshot.params.id);  
    //alert(this.route.snapshot.queryParamMap.get('id'));

    var data = {
      "id" : this.route.snapshot.queryParamMap.get('id')
    }    
    this.jobpostcalls.paymentCheck(data).subscribe((res) => {
      console.log(res.data);
      if (res.data.result.code == "000.100.110") {
        var data = {
          "customerId" : localStorage.getItem('user_id'),
          "jobpost_id" : this.route.snapshot.params.id,
          "plan_id" : this.route.snapshot.params.planid,
          "payment_id" : res.data.id,
          "payment_type" : res.data.paymentType,
          "payment_brand" : res.data.paymentBrand,  
          "amount" : res.data.amount, 
          "currency" : res.data.currency, 
          "descriptor" : res.data.descriptor, 
          "code" : res.data.result.code,   
          "result" : res.data.result, 
          "customer" : res.data.customer, 
          "card" : res.data.card, 
          "custom_parameters" : res.data.customParameters,  
          "buildNumber" : res.data.buildNumber, 
          "ndc" : res.data.ndc, 
          "payment_time" : res.data.timestamp, 
          "entire_response" : res.data,
          "advert_type" : this.route.snapshot.params.type,                                                                    
        }    
        this.jobpostcalls.planSave(data).subscribe((res) => {
    
          //this.redirectTo('myjobs');       
          jQuery('#paySuccessPopup').modal('show');
          localStorage.removeItem('localJobPostId');
          localStorage.removeItem('localplanchange');
    
        }, error => {
          console.log('error');
          console.log(error);
    
        }
        );      
      } else {
        //this.redirectTo('jobpost/'+this.route.snapshot.params.id+'/renew');
      }

    }, error => {
      console.log('error');
      console.log(error);
      //this.redirectTo('/');
    }
    ); 
      
  }

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;
  } 
  
  confirmationListener(value:any) {
    this.payment = false;
    jQuery('#paySuccessPopup').modal('hide');
    this.redirectTo('myjobs');     
                 
   }  

}
