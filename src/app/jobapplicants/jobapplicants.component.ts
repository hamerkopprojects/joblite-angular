import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../_services/commonlisting/api.service';
import { environment } from 'src/environments/environment';
import { JobberatorprofileService } from './../_services/jobberatorprofile/jobberatorprofile.service';
declare var jQuery: any;


@Component({
  selector: 'app-jobapplicants',
  templateUrl: './jobapplicants.component.html',
  styleUrls: ['./jobapplicants.component.css']
})
export class JobapplicantsComponent implements OnInit {

  applicantSearch: any;
  appliedStatus: boolean;
  jobPostId: any;
  planId: any;
  jobList: any;
  jobDataList: any;
  profileimg: any;
  username: string;
  jobinterest: any = [];
  namearray: any = [];
  location: any;
  //favourite: any;
  totalResults: any;
  previous: any;
  next: any;
  image: any;




  isCategorySearch: boolean;
  searchPage: number;
  page_counts: any;
  start: any;
  currentPrevious: any;
  currentNext: any;
  currentCount: any;
  checkCurrentCount: any;

  jobintId: any;
  userId: any;
  status: any;


  totalResponseCount: any;
  profileImgUrl = environment.server_url;

  jobIdReceived: any;
  userIdReceived: any;
  statusReceived: any;
  countReceived: any;
  clickTypeReceived: any;
  advtPlanDetails: any;
  catList: any;




  constructor(private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private jobberator: JobberatorprofileService) { }

  ngOnInit(): void {

    this.appliedStatus = true;
    this.applicantSearch = '';
    this.advtPlanDetails = [];
    this.jobPostId = this.route.snapshot.params.id;
    this.isCategorySearch = true;
    this.searchPage = 1;
    this.previous = 0;
    this.next = 0;
    this.currentPrevious = 0;
    this.currentNext = 0;
    this.page_counts = environment.page_counts;
    console.log(this.page_counts);
    this.start = 1;
    this.currentCount = 0;
    this.checkCurrentCount = 0;

    this.doGetJobDetails();    
    this.jobApplicantsList();
    this.jobberatorJobCategoryDetails();
    this.doGetJobListbyCatFilter();

  }

  createRange(number: any){
    if (isNaN(number)) {
      number = 0;
    } 
    return new Array(number);
  }

  doGetJobDetails() {
    this.apiService.doGetJobDetails(this.jobPostId)
      .subscribe(
        (data: any) => {
          this.planId = data.jobpostInfo.advertice_plan_settings_id;
          this.planSettings();
        },
        error => {
          console.log(error);
        }
      );
      }  

  planSettings() {
    var data = {
      "plan_id": this.planId
    }
    this.apiService.getPlanSettings(data).subscribe((res) => {
      this.advtPlanDetails = res.data.plan;
    }, error => {
      //this.redirectTo('/');
    }
    );



  }  

  jobberatorJobApplicantsStatusChange(jobintIdReceived: any, userIdReceived: any, statusReceived: any) {

    this.apiService.jobberatorJobApplicantsStatusChange(jobintIdReceived, userIdReceived, statusReceived).subscribe((response: any) => {
      this.jobApplicantsList();
      this.jobberatorJobCategoryDetails();
      this.doGetJobListbyCatFilter();
    },

      error => {
        //this.redirectTo('/');
      }
    );

  }
  range1(i: any, start: any) { return i >= start ? this.range1(i - 1, start).concat(i) : [] }

  pagination(value: any) {
    this.searchPage = value;
    // this.jobType=value;  
    if (!this.isCategorySearch) {
      //this.doGetJobListbyJobtypeFilters();
    } else {
      this.doGetJobListbyCatFilter();
    }
  }

  gonext(value: any) {
    this.searchPage = value;
    this.start = value;
    // this.jobType=value;  
    if (!this.isCategorySearch) {
      //this.doGetJobListbyJobtypeFilters();
    } else {
      this.doGetJobListbyCatFilter();
    }
  }

  goprev(value: any) {
    this.searchPage = value;
    this.start = value - environment.page_counts + 1;
    // this.jobType=value;  
    if (!this.isCategorySearch) {
      //this.doGetJobListbyJobtypeFilters();
    } else {
      this.doGetJobListbyCatFilter();
    }
  }


  doGetJobListbyCatFilter() {


    this.apiService.fetchJobApplicatsList(this.jobPostId, this.searchPage)
      .subscribe(
        (response: any) => {
          // data.data.resp.forEach((value: any, key: any) => {
          //   if (data.data.resp[key].job_intimation[0]?.status == 'donot_show') {
          //     data.data.resp.splice(key,1);
          //   }
          // });  
          response.data.data.forEach((value: any, key: any) => {
            if (response.data.data[key].status != 'initiated' && response.data.data[key].status != 'donot_show') {
              this.appliedStatus = false;
            }
            if (response.data.data[key].customer?.user_type == 'normal') {
              response.data.data[key].customer.profile_img = this.profileImgUrl + response.data.data[key].customer.profile_img;
            }
          });         
          this.jobList = response.data.data;
          this.catList = response.data.job_cats;
          this.totalResponseCount = response.data.count;
          this.checkCurrentCount = this.totalResponseCount / environment.search_limit


          if (this.start == 1) {
            this.previous = 0;
          }
          if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount > 1) {
            this.currentCount = environment.page_counts;
            if (this.checkCurrentCount > environment.page_counts) {
              this.next = this.currentCount + 1;
            }
          } else if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount <= 1) {
            this.currentCount = this.checkCurrentCount;
          }

          if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount > 1) {
            this.currentCount = environment.page_counts;
            if (this.checkCurrentCount > environment.page_counts) {
              this.next = this.currentCount + 1;
            }
          } else if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount <= 1) {
            this.currentCount = this.checkCurrentCount;
          }

          if (this.start > 1 && this.start == this.searchPage) {
            this.previous = this.start - 1;
            this.currentCount = this.start + (environment.page_counts - 1);
            if (this.currentCount >= this.totalResponseCount) {
              this.currentCount = this.totalResponseCount;
              this.next = 0;
            }

          }

          if (this.start > 1 && this.start !== this.searchPage) {
            this.currentCount = this.start + (environment.page_counts - 1);
            if (this.currentCount >= this.totalResponseCount) {
              this.currentCount = this.totalResponseCount;
            }
          }
          if (this.currentCount < 1) {
            this.currentCount = 1;
          }
          this.currentNext = this.currentCount + 1;
          this.currentPrevious = this.start - 1;

          this.totalResults = this.range1(this.currentCount, this.start);

        },
        error => {
          console.log(error);
        }
      );
  }

  jobApplicantsList() {

    this.apiService.fetchJobApplicatsList(this.jobPostId, this.searchPage).subscribe((response: any) => {
      response.data.data.forEach((value: any, key: any) => {
        if (response.data.data[key].status != 'initiated' && response.data.data[key].status != 'donot_show') {
          this.appliedStatus = false;
        }
        if (response.data.data[key].customer?.user_type == 'normal') {
          response.data.data[key].customer.profile_img = this.profileImgUrl + response.data.data[key].customer.profile_img;
        }
      }); 
      this.jobList = response.data.data;
      this.jobList.forEach((value: any, key: any) => {
        this.jobList[key].customer.languages = '';
        this.jobList[key].customer.languagespref = '';
        this.jobList[key].customer.langDetails.forEach((v: any, k: any) => {
          if (this.jobList[key].customer.languages == '') {
            this.jobList[key].customer.languages = this.jobList[key].customer.langDetails[k].langs[0].name;
            this.jobList[key].customer.languagespref = this.jobList[key].customer.langDetails[k].langs[0].name + '(' + this.jobList[key].customer.langDetails[k].prefs[0].name +')';
          }
          if (this.jobList[key].customer.languages != '') {
            this.jobList[key].customer.languages = this.jobList[key].customer.languages + ',' + this.jobList[key].customer.langDetails[k].langs[0].name;
            this.jobList[key].customer.languagespref = this.jobList[key].customer.languagespref + ',' + this.jobList[key].customer.langDetails[k].langs[0].name+ '(' + this.jobList[key].customer.langDetails[k].prefs[0].name +')';
          }
        });

        this.jobList[key].customer.quali = '';
        this.jobList[key].customer.insti = '';
        this.jobList[key].customer.eduDetails.forEach((v: any, k: any) => {
          if (this.jobList[key].customer.quali == '') {
            this.jobList[key].customer.quali = this.jobList[key].customer.eduDetails[k].quali[0].name;
            this.jobList[key].customer.insti = this.jobList[key].customer.eduDetails[k].quali[0].name + '(' + this.jobList[key].customer.eduDetails[k].insti[0].name +')';
          }
          if (this.jobList[key].customer.quali != '') {
            this.jobList[key].customer.quali = this.jobList[key].customer.quali + ',' + this.jobList[key].customer.eduDetails[k].quali[0].name;
            this.jobList[key].customer.insti = this.jobList[key].customer.insti + ',' + this.jobList[key].customer.eduDetails[k].quali[0].name+ '(' + this.jobList[key].customer.eduDetails[k].insti[0].name +')';
          }
        });
      });
      this.jobDataList = response.data.jobData;
      //console.log("list", this.jobList)
    },

      error => {
        //this.redirectTo('/');
      }
    );
  }

  jobberatorJobCategoryDetails() {
    var data = {
      "customers_id": localStorage.getItem('user_id')
    }
    this.jobberator.jobberatorJobCategoryDetails(data).subscribe((res) => {
      this.jobinterest = res.data.jobtypes.job_interests
      var arr = Object.entries(this.jobinterest).map(([type, value]) => ({ type, value }));
      this.jobinterest = arr;
      this.namearray = [];
      for (let i = 0; i < this.jobinterest.length; i++) {
        this.namearray.push(this.jobinterest[i].value[0].name)
      }
      console.log("name", this.namearray);
    }, error => {
      //this.redirectTo('/');
    }
    );



  }

  closeJob(jobintIdReceived: any, userIdReceived: any, statusReceived: any, count: any) {
    //this.jobList.splice(count,1); 
    this.jobberatorJobApplicantsStatusChange(jobintIdReceived, userIdReceived, statusReceived);
  }


  acceptJob(jobintIdReceived: any, userIdReceived: any, statusReceived: any, count: any) {
    this.jobberatorJobApplicantsStatusChange(jobintIdReceived, userIdReceived, statusReceived);
    this.jobList[count].status = 'jobberator_accepted';
  }

  acceptJobConfirmation(jobintIdReceived: any, userIdReceived: any, statusReceived: any, count: any, clicktype: any, currentJobStatus: any) {
    if (currentJobStatus != 'jobberator_accepted' || clicktype == 'close') {

      localStorage.setItem('jobintIdReceived', jobintIdReceived);
      localStorage.setItem('userIdReceived', userIdReceived);
      localStorage.setItem('statusReceived', statusReceived);
      localStorage.setItem('count', count);
      localStorage.setItem('clicktype', clicktype);

      clicktype == 'close' ? jQuery('#confirmationText').html("Do you want to close?") : jQuery('#confirmationText').html("Do you want to accept?");
      if(clicktype == 'wishtype'){
        if(this.jobList[count].is_jobberator_favourite == 'favourite'){
          jQuery('#confirmationText').html("Do you want to remove this item from favourits?");
        }
        else{
          jQuery('#confirmationText').html("Do you want to add this item to favourits?");
        }
      }
      jQuery('#confirmationPopup').modal('show')
    }

  }
  confirmationListener(value: any) {

    this.jobIdReceived = localStorage.getItem('jobintIdReceived');
    this.userIdReceived = localStorage.getItem('userIdReceived');
    this.statusReceived = localStorage.getItem('statusReceived');
    this.countReceived = localStorage.getItem('count');
    this.clickTypeReceived = localStorage.getItem('clicktype');
    jQuery('#confirmationPopup').modal('hide');

    if (this.clickTypeReceived == 'accept') {
      this.acceptJob(this.jobIdReceived, this.userIdReceived, this.statusReceived, this.countReceived)
    }
    else if (this.clickTypeReceived == 'close') {
      this.closeJob(this.jobIdReceived, this.userIdReceived, this.statusReceived, this.countReceived);
    }
    else if(this.clickTypeReceived == 'wishtype'){
      this.addToWishList(this.jobIdReceived,this.countReceived,this.statusReceived,this.countReceived);

    }
    localStorage.removeItem('jobintIdReceived');
    localStorage.removeItem('userIdReceived');
    localStorage.removeItem('statusReceived');
    localStorage.removeItem('count');
    localStorage.removeItem('clicktype');

  }

  addToWishList(jobintIdReceived: any, userIdReceived: any, statusReceived: any,i:any) {
    let is_favourite: string;

   statusReceived = statusReceived == "favourite" ? "unfavourite" : "favourite";
      if (this.jobList[i].is_jobberator_favourite == 'favourite') {
        if(typeof this.jobList[i] === 'undefined')    {
          this.jobList[i] = [];
        }           
        this.jobList[i].is_jobberator_favourite = 'unfavourite';
        is_favourite = 'unfavourite';
      } else {
        if(typeof this.jobList[i] === 'undefined')    {
          this.jobList[i] = [];
        }          
        this.jobList[i].is_jobberator_favourite = 'favourite';          
        is_favourite = 'favourite';
      }
      this.updateJobberatorApplicantFavourite(jobintIdReceived, userIdReceived, statusReceived);

      }

      updateJobberatorApplicantFavourite(jobintId:any, customerId:any, is_favourite:any) {
        this.apiService.updateJobberatorApplicantFavourite(jobintId, customerId, is_favourite)
      .subscribe(
        (data: any) => {
          console.log(data);
        },
        error => {
          console.log(error);
        }
      ); 

      }
  

  applicantDetails(uri: any) {
    localStorage.setItem('applicant_job_id',this.jobPostId);
    this.redirectTo(uri);
  }      

  redirectTo(uri: string) {
    this.router.navigate([uri]);
    return false;
  }

}
