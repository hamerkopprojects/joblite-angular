import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScopeincreaseComponent } from './scopeincrease.component';

describe('ScopeincreaseComponent', () => {
  let component: ScopeincreaseComponent;
  let fixture: ComponentFixture<ScopeincreaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScopeincreaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScopeincreaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
