import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { JobberatorprofileService } from './../_services/jobberatorprofile/jobberatorprofile.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { FileUploader, FileLikeObject } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoriesService} from './../_services/categories/categories.service'; 
import { JobpostcallsService} from './../_services/jobpostcalls/jobpostcalls.service'; 
import { prepareEventListenerParameters } from '@angular/compiler/src/render3/view/template';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { NotificationService } from './../_services/notification.service';

declare var jQuery: any;

@Component({
  selector: 'app-scopeincrease',
  templateUrl: './scopeincrease.component.html',
  styleUrls: ['./scopeincrease.component.css']
})
export class ScopeincreaseComponent implements OnInit {

  @ViewChild('tabDiv1') tabDiv1: ElementRef<HTMLElement>;
  @ViewChild('tabDiv2') tabDiv2: ElementRef<HTMLElement>;
  @ViewChild('tabDiv3') tabDiv3: ElementRef<HTMLElement>;  
  @ViewChild('tabDiv4') tabDiv4: ElementRef<HTMLElement>;  
  @ViewChild('tabDiv5') tabDiv5: ElementRef<HTMLElement>; 
  @ViewChild('tabDiv6') tabDiv6: ElementRef<HTMLElement>;    

  @ViewChild('inperson') inperson: ElementRef<HTMLElement>; 
  @ViewChild('online') online: ElementRef<HTMLElement>;
  loadAPI: Promise<any>; 

  public filter: string;
  tab2Completed: boolean; 
  tab3Completed: boolean; 
  tab4Completed: boolean;
  tab5Completed: boolean; 
  categorySearch: any;
  // public source: Array<{ text: string, value: number }> = [
  //     { text: 'Small', value: 1 },
  //     { text: 'Medium', value: 2 },
  //     { text: 'Large', value: 3 }
  // ];  
  public source: any; 
  public data: Array<{ text: string, value: number }>; 
  url = 'https://test.oppwa.com/v1/paymentWidgets.js?checkoutId='; 
  jobProfSelectErrorMessage: string;
  amount: any; 
  movetab1status: boolean;  
  activeTab: string;
  jobPostId: any;
  oldJobPostId: any;
  tab2jobPostId: any;
  tab3jobPostId: any;
  tab4jobPostId: any;
  tab5jobPostId: any;
  minDob: Date;

  taskjobPostId: any;
  imagejobPostId: any;  
  renew: any;
  renewStatus: boolean;
  jobPostImageId: any;  
  imageUploadErrorMessage: string;
  defaultKey: string;

  jobarea: string;
  jobareaErrorMessage: string;  
  jobtitle: string;
  jobtitleErrorMessage: string;
  jobdescription: string;
  jobdescriptionErrorMessage: string; 
  taskErrorMessage: string; 
  startdate: any;
  startdateErrorMessage: string; 
  enddate: any;
  enddateErrorMessage: string;    
  jobPostPhotos: any; 
  location: string;
  locErrorMessage = ''; 
  taskName: string;
  taskNameErrorMessage: string;
  taskDesc: string;
  taskDescErrorMessage: string;
  taskPerc: string; 
  taskPercErrorMessage: string;
  checkoutId: any;
  shopperResultUrl: any;
  selectedValue: any;

  public uploader: FileUploader = new FileUploader({});
  formjd = new FormGroup({
    startdatepicker: new FormControl('', [Validators.required]),  
    enddatepicker: new FormControl('', [Validators.required]),       
    jobtitle: new FormControl('', [Validators.required]),
    jobdescription: new FormControl('', [Validators.required]),
    // location: new FormControl('', [Validators.required]),    
  }); 

  projectbudget: string;
  projectbudgetErrorMessage: string;   
  jobTaskList: any;
  jobTaskFirstList: any;  
  // formt = new FormGroup({
  //   projectbudget: new FormControl('', [Validators.required]),
  //   task_name:  new FormControl([], [Validators.required]),
  //   task_desc:  new FormControl([], [Validators.required]),
  //   percentage:  new FormControl([], [Validators.required]),        
  // }); 

  yearofexpns: string;
  yearofexpnsErrorMessage: string;    
  // formam = new FormGroup({
  //   yearofexpns: new FormControl('', [Validators.required])
  // }); 
  
  defaultTask: boolean;
  
  categoryAll: any;
  catbody: any;
  catArray: any;
  catOther: any;
  catSelectErrorMessage: string; 
  jobSelectErrorMessage: string; 
  jtypebody: any;
  jobTypes: any;
  tempJobTypes: any; 
  jobTypesArray: any;
  selectedJobs: any;

  higherLevelEducationData: any;
  higherErrorMessage: any;
  tempHighrlevel: any;
  skillData: any;
  skillErrorMessage: any;
  tempSkill: any;


  planData: any; 
  planFeatures: any;
  planId: string;
  oldPlanId: string;
  tempPlan: any;
  planErrorMessage: any;

  showAdvancedMatching: any;
  showPayment: boolean;
  plannameSelected: string;

  showJobTypes: any; 

  loccoordinates: string;

  center: any;
  defaultBounds: any;
  input: any;
  mapoptions: any;
  autocomplete: any;  
  titlemessage: string;

  constructor(private SpinnerService: NgxSpinnerService,
    private category: CategoriesService, 
    private jobpostcalls: JobpostcallsService,   
    private router: Router,
    private route: ActivatedRoute,
    private notifyService : NotificationService) { 
      //this.data = this.source.slice(0);      
    }

  ngOnInit(): void {
    this.titlemessage = '';
    this.tab2Completed = false;
    this.tab3Completed = false;
    this.tab4Completed = false;
    this.tab5Completed = false;
    this.defaultTask = false;
    this.taskErrorMessage = '';
    this.categorySearch = '';
    this.selectedValue = [];
    //this.data = this.source.slice(0); 
    this.source = [];      
    this.jobProfSelectErrorMessage = '';
    this.amount = '';  
    this.activeTab = 'jbtr1';
    this.movetab1status = false; 

    const today = new Date();
    this.minDob = new Date(
      today.getFullYear(),
      today.getMonth(),
      today.getDate()
    );

    this.jobarea = '';
    this.jobareaErrorMessage = '';
    this.jobtitle = '';
    this.jobtitleErrorMessage = '';
    this.jobdescription = '';
    this.jobdescriptionErrorMessage = '';
    this.startdate = '';
    this.startdateErrorMessage = ''; 
    this.enddate = '';
    this.enddateErrorMessage = '';  
    this.jobPostImageId = '';
    this.location = '';
    this.locErrorMessage = '';    
    this.imageUploadErrorMessage = '';

    this.taskName = '';
    this.taskNameErrorMessage = '';
    this.taskDesc = '';
    this.taskDescErrorMessage = '';    
    this.taskPerc = '';
    this.taskPercErrorMessage = '';    

    this.tab2jobPostId = '';
    this.tab3jobPostId = '';
    this.tab4jobPostId = '';
    this.tab5jobPostId = '';
    this.taskjobPostId = '';
    this.imagejobPostId = '';
    this.jobTaskList = [];

    this.jobPostId = this.route.snapshot.params.id;
    this.oldJobPostId = this.route.snapshot.params.id;    
    this.renew = this.route.snapshot.params.renew;
    if (this.renew == 'renew') {
      this.renewStatus = true;
    } else {
      this.renewStatus = false;
    }
    this.jobPostPhotos = [];    
    if (this.jobPostId == undefined) {
      this.jobPostId = '';
    } else {
      this.jobpostphotoslist();
      this.getJobDetails();
      this.getTaskDetails();
    }


    setTimeout(() => {
      this.center = { lat: 26.2041, lng: -28.0473 };
      // Create a bounding box with sides ~10km away from the center point
      this.defaultBounds = {
        north: this.center.lat + 0.1,
        south: this.center.lat - 0.1,
        east: this.center.lng + 0.1,
        west: this.center.lng - 0.1,
      };
      this.input = document.getElementById("pac-input") as HTMLInputElement;
      this.mapoptions = {
        bounds: this.defaultBounds,
        componentRestrictions: { country: "za" },
        fields: ["address_components", "geometry", "icon", "name"],
        strictBounds: false,
        types: ["establishment"],
      };
      
      this.autocomplete = new google.maps.places.Autocomplete(this.input, this.mapoptions);  
      
      this.autocomplete.addListener("place_changed", () => {
    
        const place = this.autocomplete.getPlace();
        this.loccoordinates = place.geometry.location.lat()+
        ','+place.geometry.location.lng();       
        this.location = place.name+', '+place.address_components[1].long_name
        +', '+place.address_components[5].long_name
        +', '+place.address_components[5].short_name
        +', '+place.address_components[6].short_name; 
      }); 
      
    }, 1000)

    this.uploader.onAfterAddingFile = (file) => {
      this.setPreview(file);
     };      
         

    this.projectbudget = '';
    this.projectbudgetErrorMessage = ''; 
    this.jobTaskList = []; 
    
    this.catArray = [];
    this.catOther = [];
    this.jtypebody = [];
    this.jobTypes = [];
    this.tempJobTypes = []; 
    this.jobTypesArray = []; 
    this.selectedJobs = [];
    this.catSelectErrorMessage = '';
    this.jobSelectErrorMessage = '';

    this.higherLevelEducationData = [];
    this.higherErrorMessage = '';
    this.tempHighrlevel = [];
    this.skillData = [];
    this.skillErrorMessage = '';
    this.tempSkill = [];    
    this.yearofexpns = '';
    this.yearofexpnsErrorMessage = '';  
    
    this.planData = []; 
    this.planFeatures = [];
    this.planId = ''; 
    this.tempPlan = []; 
    this.planErrorMessage = '';  
    this.showAdvancedMatching = false;
    this.showPayment = false;
    this.plannameSelected = ''; 
    this.showJobTypes = [];   
    this.categoryList();  
    this.educationList();
    this.planList();
    this.getAdvancedMatchingDetails(); 
    if (this.jobPostId !== '') {    
      this.getPaymentDetails();
    } 
    this.matchingCriteriaDetails();    
    if (this.renewStatus) {
      this.shopperResultUrl = environment.callback_url+'paycallback/'+this.jobPostId+'/'+this.planId+'/renew'; 
    } else {
      this.shopperResultUrl = environment.callback_url+'paycallback/'+this.jobPostId+'/'+this.planId+'/advert'; 
    }
  
  }

  public addNew(j: any, jobCatId: any, job_types: any): void {
    let data = {
      "customers_id" : localStorage.getItem('user_id'),
      "name" : this.filter,
      "job_cat_id" : jobCatId,
      "jobpost_id" : this.jobPostId,          
    }

    this.jobpostcalls.addJobType(data).subscribe((res) => {
      this.jobTypesArray[j].push({
        text: this.filter,
        value: res.id
      });
      this.source[j].push({
        text: this.filter,
        value: res.id
      });    
      //this.matchingCriteriaDetails();       
      this.handleFilter(this.filter,j,job_types);
      //this.valueChange(this.filter,j,job_types);
    }, Error => {
      this.SpinnerService.hide();
    }
    );    
  }

  public handleFilter(value: any,j: any, job_types: any) {
      let defaultSelect = [];
      defaultSelect['text'] = 'Search matching job types';
      defaultSelect['value'] = '';              
      this.selectedValue[this.defaultKey] = defaultSelect;     
      this.filter = value;
      //this.source = this.jobTypesArray[j];
      this.jobTypesArray[j] = this.source[j].filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }  


  public valueChange(value: any,j: any, job_types: any) {
    this.tempJobTypes = this.jobTypes;
    let keydynamic: any; 
    job_types.forEach((val: any, key: any) => {
      if (val[0]?.id == this.selectedValue[j].value) {
        keydynamic = key;
      }   
    });  
    if (this.jobTypes[j]['job_types'].hasOwnProperty(keydynamic)) {
      this.showJobTypes[j] = true;
      this.jobTypes[j]['job_types'][keydynamic][0]['checked'] = true; 
      this.jobTypes[j]['job_cats']['visibility'] = true;      
    } else {
      this.matchingCriteriaDetails();
    
    }   
    let defaultSelect = [];
    defaultSelect['text'] = 'Search matching job types';
    defaultSelect['value'] = '';              
    this.selectedValue[this.defaultKey] = defaultSelect; 
  }  


  loadScript() {
    let node = document.createElement('script');
    node.src = this.url+''+this.checkoutId;
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }  

  jobareacheck(type: string) {
    if (this.jobarea == '') {
      this.jobarea = type;
    } else if(this.jobarea == type) {
      this.jobarea = '';
    } else {
      this.jobarea = type;
    }
  }

  public setPreview(file:any) {
    //console.log("file",file);
    file.withCredentials = false;
    let fr = null;
    fr = new FileReader();
    fr.onload = () => {

    }
    fr.readAsDataURL(file._file);
    this.upload();
  }  

  getFiles(): FileLikeObject[] {
    return this.uploader.queue.map((fileItem) => {
      return fileItem.file;
    });
  }

  upload() {
    let files = this.getFiles();
    //console.log(files);
    let file = files[files.length - 1];
    console.log(file);    
    // files.forEach((file,index) => {
      let formData = new FormData();
      formData.append('images', file.rawFile, file.name);
      formData.append('customerId', localStorage.getItem('user_id'));
    
      if (this.tab2jobPostId == "") {
        this.imagejobPostId =  this.jobPostId;
      }
    
      if (this.tab2jobPostId != "") {
        this.imagejobPostId =  this.tab2jobPostId;   
      }      
      if (this.jobPostImageId == '') {
        this.jobpostcalls.upload(formData, this.imagejobPostId).subscribe((res) => {
          this.imagejobPostId = res.jobpost_id;
          if (this.renewStatus) {
            this.shopperResultUrl = environment.callback_url+'paycallback/'+this.imagejobPostId+'/'+this.planId+'/renew'; 
          } else {
            this.shopperResultUrl = environment.callback_url+'paycallback/'+this.imagejobPostId+'/'+this.planId+'/advert'; 
          }        
          this.imageUploadErrorMessage = '';          
          this.jobPostPhotos = res.photos[0].photos; 
          this.jobPostPhotos.forEach((value: any, key: any) => {
            if (this.jobPostPhotos[key]['image_path'] == null) {
              this.jobPostPhotos[key]['image_path'] = 'assets/images/catagerios_06.png';
            } else {
              this.jobPostPhotos[key]['image_path'] = environment.server_url+'' +this.jobPostPhotos[key]['image_path'];
            }
          });         
    
        }, Error => {
          this.imageUploadErrorMessage = Error.error.error.msg;;          
          this.SpinnerService.hide();
        }
        );
      } else {
        formData.append('image_id', this.jobPostImageId);        
        this.jobpostcalls.uploadupdate(formData, this.imagejobPostId).subscribe((res) => {
          this.imageUploadErrorMessage = '';          
          this.jobPostImageId = '';
          this.jobPostPhotos = res.photos[0].photos; 
          this.jobPostPhotos.forEach((value: any, key: any) => {
            if (this.jobPostPhotos[key]['image_path'] == null) {
              this.jobPostPhotos[key]['image_path'] = 'assets/images/catagerios_06.png';
            } else {
              this.jobPostPhotos[key]['image_path'] = environment.server_url+'' +this.jobPostPhotos[key]['image_path'];
            }
          });         
    
        }, Error => {
          this.imageUploadErrorMessage = Error.error.error.msg;;
          this.SpinnerService.hide();
        }
        );        
      }

    // });
  }

  deleteImage(id: any) {
    if (this.tab2jobPostId == "") {
      this.imagejobPostId =  this.jobPostId;
    }
  
    if (this.tab2jobPostId != "") {
      this.imagejobPostId =  this.tab2jobPostId;   
    }

    var data = {
      "image_id" : id,
      "jobpost_id" : this.imagejobPostId
    }    
    this.jobpostcalls.deleteImage(data).subscribe((res) => {
      this.jobPostPhotos = [];
      // this.jobPostPhotos = res.photos[0].photos; 
      // this.jobPostPhotos.forEach((value: any, key: any) => {
      //   if (this.jobPostPhotos[key]['image_path'] == null) {
      //     this.jobPostPhotos[key]['image_path'] = 'assets/images/catagerios_06.png';
      //   } else {
      //     this.jobPostPhotos[key]['image_path'] = environment.server_url+'' +this.jobPostPhotos[key]['image_path'];
      //   }
      // }); 
    }, error => {
      //this.redirectTo('/');
    }
    );    
  }  

  setImageId(id: any) {
    this.jobPostImageId = id;
  }

  showToasterSuccess(){
    this.notifyService.showSuccess(this.titlemessage, "")
  }
  
  showToasterError(){
      this.notifyService.showError(this.titlemessage, "")
  }
  
  showToasterInfo(){
      this.notifyService.showInfo(this.titlemessage, "")
  }
  
  showToasterWarning(){
      this.notifyService.showWarning(this.titlemessage, "")
  }

  addNewTask() {
    let flag = true;
    this.defaultTask = true;

    if (this.taskName == '') {
      this.taskNameErrorMessage = 'Please enter task';
      flag = false;
    } else {
      this.taskNameErrorMessage = '';
    }

    if (this.taskDesc == '') {
      this.taskDescErrorMessage = 'Please enter task description';
      flag = false;
    } else {
      this.taskDescErrorMessage = '';
    }

    // if (this.taskPerc == '') {
    //   this.taskPercErrorMessage = 'Please select your task percentage';
    //   flag = false;
    // } else {
    //   this.taskPercErrorMessage = '';
    // }
    
    if (this.projectbudget == '' || this.projectbudget == null) {
      // this.projectbudgetErrorMessage = "Project budget is mandatory";
      // flag = false;
      this.projectbudgetErrorMessage = '';      
    } else {
      this.projectbudgetErrorMessage = '';
    }  
    
    
    // if (this.tab2jobPostId == "") {
    //   this.taskjobPostId =  this.jobPostId;
    // }
  
    // if (this.tab2jobPostId != "") {
    //   this.taskjobPostId =  this.tab2jobPostId;   
    // }    

    if (this.taskjobPostId != '' && flag) {
      let data = {
        "customerId" : localStorage.getItem('user_id'),
        "jobpost_id" : this.taskjobPostId,
        "task_name"  : this.taskName,
        "task_desc"  : this.taskDesc,  
        "task_perc"  : this.taskPerc,
        "project_budget" :  this.projectbudget     
      }

      this.jobpostcalls.jobTaskSave(data).subscribe((res) => {
        this.defaultTask = false;
        this.jobTaskList = [];
        this.jobTaskList = res.tasks;
        this.taskName = '';
        this.taskDesc = '';
        this.taskPerc = '';
        this.projectbudgetErrorMessage = '';
        this.taskErrorMessage = '';
        this.titlemessage = 'Added task successfully';
        this.showToasterSuccess();

      }, Error => {
        this.SpinnerService.hide();
      }
      );
    } else if (this.taskjobPostId == '' && flag) {
      let data = {
        "customerId" : localStorage.getItem('user_id'),
        "task_name"  : this.taskName,
        "task_desc"  : this.taskDesc,  
        "task_perc"  : this.taskPerc,
        "project_budget" :  this.projectbudget,
        "enhance_type" : 'scopeincrease'     
      }

      this.jobpostcalls.jobTaskSaveWithoutJobId(data).subscribe((res) => {
        this.defaultTask = false;
        this.jobPostId = res.id;
        this.taskjobPostId =  this.jobPostId;
        this.tab2jobPostId = this.jobPostId;
        this.jobTaskList = [];
        this.jobTaskList = res.tasks;
        this.taskName = '';
        this.taskDesc = '';
        this.taskPerc = '';
        this.projectbudgetErrorMessage = '';
        this.taskErrorMessage = '';
        this.titlemessage = 'Added task successfully';
        this.showToasterSuccess();
      }, Error => {
        this.SpinnerService.hide();
      }
      );      
    }
   
  }

  addNewTaskOpen() {
    this.defaultTask = true;    
  }

  removeEmptyTask() {
    this.defaultTask = false;      
  }

  addNewTaskDirect() {
    let flag = true;

    if (this.taskName == '') {
      this.taskNameErrorMessage = 'Please enter task';
      flag = false;
    } else {
      this.taskNameErrorMessage = '';
    }

    if (this.taskDesc == '') {
      this.taskDescErrorMessage = 'Please enter task description';
      flag = false;
    } else {
      this.taskDescErrorMessage = '';
    }
    
    if (this.projectbudget == '' || this.projectbudget == null) {
      this.projectbudgetErrorMessage = '';      
    } else {
      this.projectbudgetErrorMessage = '';
    }    


    if (this.tab2jobPostId != '' && flag) {
      let data = {
        "customerId" : localStorage.getItem('user_id'),
        "jobpost_id" : this.tab2jobPostId,
        "task_name"  : this.taskName,
        "task_desc"  : this.taskDesc,  
        "task_perc"  : this.taskPerc,
        "project_budget" :  this.projectbudget     
      }

      this.jobpostcalls.jobTaskSave(data).subscribe((res) => {
        this.defaultTask = false;
        // this.tab4Completed = true;
        this.jobTaskList = [];
        this.jobTaskList = res.tasks;
        this.taskName = '';
        this.taskDesc = '';
        this.taskPerc = '';
        this.projectbudgetErrorMessage = '';
        this.taskErrorMessage = ''; 
        this.jobDetailsSave();   

      }, Error => {
        this.SpinnerService.hide();
      }
      );
    } else if (this.tab2jobPostId == '' && flag) {
      console.log('inner2');
      this.defaultTask = false;
      let data = {
        "customerId" : localStorage.getItem('user_id'),
        "task_name"  : this.taskName,
        "task_desc"  : this.taskDesc,  
        "task_perc"  : this.taskPerc,
        "project_budget" :  this.projectbudget,
        "enhance_type" : 'scopeincrease'     
      }

      this.jobpostcalls.jobTaskSaveWithoutJobId(data).subscribe((res) => {
        this.tab2jobPostId = res.id;
        localStorage.setItem('localJobPostId', this.tab2jobPostId);
        this.jobTaskList = [];
        this.jobTaskList = res.tasks;
        this.taskName = '';
        this.taskDesc = '';
        this.taskPerc = '';
        this.projectbudgetErrorMessage = '';
        this.taskErrorMessage = ''; 
        this.jobDetailsSave();

      }, Error => {
        this.SpinnerService.hide();
      }
      );      
    }
   
  }


  removeTask(taskId: any) {
    if (this.tab2jobPostId == "") {
      this.taskjobPostId =  this.jobPostId;
    }
  
    if (this.tab2jobPostId != "") {
      this.taskjobPostId =  this.tab2jobPostId;   
    } 
    if (this.taskjobPostId != '') {
      let data = {
        "customerId" : localStorage.getItem('user_id'),
        "jobpost_id" : this.taskjobPostId,
        "task_id"  : taskId     
      }
      this.jobpostcalls.jobTaskRemove(data).subscribe((res) => {
        this.jobTaskList = [];
        this.jobTaskList = res.tasks;
      }, Error => {
        this.SpinnerService.hide();
      }
      );
    } 
  } 
  
  updateTask(taskId: any,key: any) {
    let flag = true;

    if (this.jobTaskList[key].task_name == '' || this.jobTaskList[key].task_name == null) {
      flag = false;
    }

    if (this.jobTaskList[key].task_desc == '' || this.jobTaskList[key].task_desc == null) {
      flag = false;
    }

    // if (this.jobTaskList[key].percentage == '' || this.jobTaskList[key].percentage == null) {
    //   flag = false;
    // }

    if (this.tab2jobPostId == "") {
      this.taskjobPostId =  this.jobPostId;
    }
  
    if (this.tab2jobPostId != "") {
      this.taskjobPostId =  this.tab2jobPostId;   
    }    
    
    if (this.jobPostId != '' && flag) {
      let data = {
        "customerId" : localStorage.getItem('user_id'),
        "jobpost_id" : this.taskjobPostId,
        "task_id"  : taskId,
        "task_name"  : this.jobTaskList[key].task_name,
        "task_desc"  : this.jobTaskList[key].task_desc,  
        "task_perc"  : this.jobTaskList[key].percentage              
      }

      this.jobpostcalls.jobTaskUpdate(data).subscribe((res) => {
        this.jobTaskList = [];
        this.jobTaskList = res.tasks;
      }, Error => {
        this.SpinnerService.hide();
      }
      );
    } 
  }  

  jobDetailsSave() {
    // if (this.jobTaskList.length == 0 || this.defaultTask) {
    //   this.addNewTaskDirect();
    // } else {
    let flag = true;

    if (this.jobtitle == '') {
      this.jobtitleErrorMessage = 'Please enter your job title';
      flag = false;
    } else {
      this.jobtitleErrorMessage = '';
    }

    if (this.jobdescription == '') {
      this.jobdescriptionErrorMessage = 'Please enter your job description';
      flag = false;
    } else {
      this.jobdescriptionErrorMessage = '';
    }

    if (this.jobTaskList.length == 0 || this.defaultTask) {
      if (this.taskName == '') {
        this.taskNameErrorMessage = 'Please enter task';
        flag = false;
      } else {
        this.taskNameErrorMessage = '';
      }

      if (this.taskDesc == '') {
        this.taskDescErrorMessage = 'Please enter task description';
        flag = false;
      } else {
        this.taskDescErrorMessage = '';
      }
    }

    // if (this.startdate == '' || !this.startdate) {
    //   this.startdateErrorMessage = 'Please select your start date';
    //   flag = false;
    // } else {
    //   this.startdateErrorMessage = '';
    // }

    // if (this.enddate == '' || !this.enddate) {
    //   this.enddateErrorMessage = 'Please select your end date';
    //   flag = false;
    // } else {
    //   this.enddateErrorMessage = '';
    // } 
    
    if (this.jobarea == '') {
      this.jobareaErrorMessage = 'Please select any work type';
      flag = false;   
    } else {
      this.jobareaErrorMessage = '';    
    }

    // if (this.location == '') {
    //   this.locErrorMessage = 'Please enter your location';
    //   flag = false;
    // } else {
    //   this.locErrorMessage = '';
    // }    


    if (flag) {
      // this.location = this.latitude + ',' + this.longitude;
      if (this.tab2jobPostId == '') {
        let data = {
          "customerId" : localStorage.getItem('user_id'),
          "work_type" : this.jobarea,
          "job_title"  : this.jobtitle,
          "job_desc"  : this.jobdescription, 
          "plan_id" : this.planId, 
          "oldjobpost_id" : this.jobPostId  
          // "location"  : this.location,      
          // "start_date"        : this.startdate.getFullYear()+ '-' + ('0' + (this.startdate.getMonth()+1)).slice(-2)+ '-' +('0' + (this.startdate.getDate())).slice(-2),
          // "end_date"        : this.enddate.getFullYear()+ '-' + ('0' + (this.enddate.getMonth()+1)).slice(-2)+ '-' +('0' + (this.enddate.getDate())).slice(-2),        
        }

        this.jobpostcalls.jobDetailsSave(data).subscribe((res) => {
          this.tab2jobPostId = res.id;
          this.tab2Completed = true;
          if (this.jobTaskList.length == 0 || this.defaultTask) {
            this.addNewTaskDirect();
          }
          if (this.renewStatus) {
            this.shopperResultUrl = environment.callback_url+'paycallback/'+this.jobPostId+'/'+this.planId+'/renew'; 
          } else {
            this.shopperResultUrl = environment.callback_url+'paycallback/'+this.jobPostId+'/'+this.planId+'/advert'; 
          }     
 
          if (this.movetab1status) {
            this.movetab1status = false;
           
            setTimeout(() => {
              let el: HTMLElement = this.tabDiv3.nativeElement;
              el.click();
            }, 1000)
          }
        }, Error => {
          this.startdateErrorMessage = '';
          this.enddateErrorMessage = '';
          if (Error.error.error.msg.includes("start")) {
            this.startdateErrorMessage = Error.error.error.msg;
          }
          if (Error.error.error.msg.includes("end")) {
            this.enddateErrorMessage = Error.error.error.msg;
          }        
          this.SpinnerService.hide();
        }
        );
      } else {
        let data = {
          "jobpost_id" : this.tab2jobPostId,
          "work_type" : this.jobarea,
          "job_title"  : this.jobtitle,
          "job_desc"  : this.jobdescription,  
          "plan_id" : this.planId, 
          "oldjobpost_id" : this.jobPostId 
          // "location"  : this.location,      
          // "start_date"        : this.startdate.getFullYear()+ '-' + ('0' + (this.startdate.getMonth()+1)).slice(-2)+ '-' +('0' + (this.startdate.getDate())).slice(-2),
          // "end_date"        : this.enddate.getFullYear()+ '-' + ('0' + (this.enddate.getMonth()+1)).slice(-2)+ '-' +('0' + (this.enddate.getDate())).slice(-2),        
        }

        this.jobpostcalls.jobDetailsUpdate(data).subscribe((res) => {
          this.tab2Completed = true;
          if (this.jobTaskList.length == 0 || this.defaultTask) {
            this.addNewTaskDirect();
          }
          if (this.movetab1status) {
            this.movetab1status = false;
            setTimeout(() => {
              let el: HTMLElement = this.tabDiv3.nativeElement;
              el.click();
            }, 1000)
          }
        }, Error => {
          this.startdateErrorMessage = '';
          this.enddateErrorMessage = '';
          if (Error.error.error.msg.includes("start")) {
            this.startdateErrorMessage = Error.error.error.msg;
          }
          if (Error.error.error.msg.includes("end")) {
            this.enddateErrorMessage = Error.error.error.msg;
          }        
          this.SpinnerService.hide();
        }
        );        

      }
    }
    return false;
  //}
  }

  taskDetailsSave() {
    let flag = true;
    
    if (this.projectbudget == '' || this.projectbudget == null) {
      this.projectbudgetErrorMessage = "Project budget is mandatory";
      flag = false;
    } else {
      this.projectbudgetErrorMessage = '';
    } 
    
    if (this.startdate == '' || !this.startdate) {
      this.startdateErrorMessage = 'Please select your start date';
      flag = false;
    } else {
      this.startdateErrorMessage = '';
    }


    if (this.enddate == '' || !this.enddate) {
      this.enddateErrorMessage = 'Please select your end date';
      flag = false;
    } else {
      this.enddateErrorMessage = '';
    } 

    if (this.location == '') {
      this.locErrorMessage = 'Please enter your location';
      flag = false;
    } else {
      this.locErrorMessage = '';
    }  
    
    if (this.tab2jobPostId == "") {
      this.tab4jobPostId =  this.jobPostId;
    }
  
    if (this.tab2jobPostId != "") {
      this.tab4jobPostId =  this.tab2jobPostId;   
    }     

    if (this.tab4jobPostId != '' && flag) {
      let data = {
        "customerId" : localStorage.getItem('user_id'),
        "jobpost_id" : this.tab4jobPostId,
        "project_budget" :  this.projectbudget,
        "plan_id" : this.planId,
        "location"  : this.location,  
        "loc_coordinates" : this.loccoordinates,    
        "start_date"        : this.startdate.getFullYear()+ '-' + ('0' + (this.startdate.getMonth()+1)).slice(-2)+ '-' +('0' + (this.startdate.getDate())).slice(-2),
        "end_date"        : this.enddate.getFullYear()+ '-' + ('0' + (this.enddate.getMonth()+1)).slice(-2)+ '-' +('0' + (this.enddate.getDate())).slice(-2),                      
      }

      this.jobpostcalls.taskDetailsSave(data).subscribe((res) => {
        this.tab4Completed = true;
        this.taskName = '';
        this.taskDesc = '';
        this.taskPerc = '';
        this.projectbudgetErrorMessage = '';
        this.movetab1status = false;
        if (this.plannameSelected != 'Premium' ) {
          
          if (this.jobTaskList.length != 0) {          
            var data = {
              "oldjobpost_id" : this.oldJobPostId,
              "jobpost_id" : this.tab4jobPostId,
              "plan_id" : this.planId                                                                 
            }    
            this.jobpostcalls.planSaveWithoutPayment(data).subscribe((res) => {
        
              this.redirectTo('myjobs');       
        
            }, error => {
              console.log('error');
              console.log(error);
        
            }
          );
          } else {
            this.addNewTask();          
            setTimeout(() => {
              let el: HTMLElement = this.tabDiv2.nativeElement;
              el.click();    
            }, 1000)         
          }  
          
        } else {
          setTimeout(() => {
            let el: HTMLElement = this.tabDiv5.nativeElement;
            el.click();   
          }, 1000) 
        }       

      }, Error => {
        this.SpinnerService.hide();
      }
      );
    }   
  }
  

  categoryList() {
    //this.SpinnerService.show();
    this.category.getCategories().subscribe((res) => {
      this.catbody = res.data.category;
      this.catbody.forEach((value: any, key: any) => {
        if (this.catbody[key]['details'][0]['image_path'] == null) {
          this.catbody[key]['details'][0]['image_path'] = 'assets/images/catagerios_06.png';
        } else {
          this.catbody[key]['details'][0]['image_path'] = environment.server_url+'uploads/job_category/' +this.catbody[key]['details'][0]['image_path'];
        }
      });
      this.categoryAll = this.catbody;
    }, error => {
    }
    );
  }  

  jobpostphotoslist() {
    //this.SpinnerService.show();
    var data = {
      "jobpost_id" : this.jobPostId
    }    
    this.jobpostcalls.jobpostphotoslist(data).subscribe((res) => {
      this.jobPostPhotos = res.data.photos[0].photos;
      this.jobPostPhotos.forEach((value: any, key: any) => {
        if (this.jobPostPhotos[key]['image_path'] == null) {
          this.jobPostPhotos[key]['image_path'] = 'assets/images/catagerios_06.png';
        } else {
          this.jobPostPhotos[key]['image_path'] = environment.server_url+'' +this.jobPostPhotos[key]['image_path'];
        }
      }); 

    }, error => {
      //this.redirectTo('/');
    }
    );
  }

  getJobDetails() {
    //this.SpinnerService.show();
    var data = {
      "jobpost_id" : this.jobPostId
    }    
    this.jobpostcalls.getJobDetails(data).subscribe((res) => {

      if (res.jobpostInfo.data.work_type != null) {
        if (res.jobpostInfo.data.work_type == 'inperson') {        
          let el: HTMLElement = this.inperson.nativeElement;
          el.click();
        }
        if (res.jobpostInfo.data.work_type == 'online') {        
          let el: HTMLElement = this.online.nativeElement;
          el.click();
        }        
      }

      if (res.jobpostInfo.data.job_title != null) {
        this.jobtitle = res.jobpostInfo.data.job_title;
      } 
      
      if (res.jobpostInfo.data.job_desc != null) {
        this.jobdescription = res.jobpostInfo.data.job_desc;
      } 

      if (res.jobpostInfo.data.location != null) {
        this.location = res.jobpostInfo.data.location;
      }  
    
      if (res.jobpostInfo.data.loc_coordinates != null) {  
        this.loccoordinates = res.jobpostInfo.data.loc_coordinates;
      }
      
      if (res.jobpostInfo.data.start_date == null) {
        this.startdate = '';
      } else {
        var resDate = res.jobpostInfo.data.start_date.split("-");
        // this.startdate = new Date(resDate[0], resDate[1] - 1, resDate[2]);
        this.startdate = '';
      }  
      
      if (res.jobpostInfo.data.end_date == null) {
        this.enddate = '';
      } else {
        var resDate = res.jobpostInfo.data.end_date.split("-");
        // this.enddate = new Date(resDate[0], resDate[1] - 1, resDate[2]);
        this.enddate = '';
      }        

    }, error => {
      //this.redirectTo('/');
    }
    );
  } 
  
  
  getTaskDetails() {
    //this.SpinnerService.show();
    var data = {
      "jobpost_id" : this.jobPostId
    }    
    this.jobpostcalls.getTaskDetails(data).subscribe((res) => {
      if (res.jobpostInfo.data.project_budget != null) {
        this.projectbudget = res.jobpostInfo.data.project_budget;
      } 
      this.jobTaskFirstList = res.jobpostInfo.tasks;

    }, error => {
      //this.redirectTo('/');
    }
    );
  }  



  getJobTypes(catId: any) {
    if(typeof this.catArray[catId] !== 'undefined') {
      delete this.catArray[catId]; 
    } else {
      this.catArray[catId] = catId;
    }

    this.catArray.forEach((value: any, key: any) => {
      if(key !== catId) {
        delete this.catArray[key];
      }
    }); 

     
    let tempCatArray = [];
  
    tempCatArray = this.catArray.filter(function (el: any) {
      return el != null;
    });

    this.selectedJobs.forEach((value: any, key: any) => {
      if(typeof this.catArray[key] === 'undefined') {
        delete this.selectedJobs[key];
      }
    });    

    var data = {
      "customers_id" : localStorage.getItem('user_id'),
      "job_category_id" : this.catArray
    }
    this.category.getJobtypeWithCategoryDetails(data).subscribe((res) => {
      this.jtypebody = res.data.jobtypes.admin_added;

      this.jobTypes = this.jtypebody;


      this.jobTypes.forEach((valuefirst: any, keyfirst: any) => {
        this.jobTypes[keyfirst]['job_cats']['visibility'] = false;
        if (this.jobTypes[keyfirst]['job_cats']['image_path'] == null) {
          this.jobTypes[keyfirst]['job_cats']['image_path'] = 'assets/images/catagerios_06.png';
        } else {
          this.jobTypes[keyfirst]['job_cats']['image_path'] = environment.server_url+'uploads/job_category/' +this.jobTypes[keyfirst]['job_cats']['image_path'];
        }
        this.jobTypesArray[keyfirst] = [];
        let tempitem = [];                 
        valuefirst.job_types.forEach((value: any, key: any) => {
          tempitem[key] = [];          
          value.forEach((valuet: any, keyt: any) => {
            if (valuet.customers_id == null || valuet.customers_id == localStorage.getItem('user_id')) {
              let defaultSelect = [];
              tempitem[key]['text'] = valuet.lang[0].name;
              tempitem[key]['value'] = valuet.id;            
              this.jobTypesArray[keyfirst] = tempitem;
              this.source[keyfirst] = tempitem;
              // defaultSelect['text'] = valuet.lang[0].name;
              // defaultSelect['value'] = valuet.id;
              defaultSelect['text'] = 'Search matching job types';
              defaultSelect['value'] = '';  
              this.defaultKey =  keyfirst;           
              this.selectedValue[keyfirst] = defaultSelect;
            }
          });

          if(typeof this.selectedJobs[valuefirst.job_cats.job_cate_id] === 'undefined')    {
            this.selectedJobs[valuefirst.job_cats.job_cate_id] = [];
          }    
   
          let n = this.selectedJobs[valuefirst.job_cats.job_cate_id].includes(value[0]?.id);
          if (n) {
            this.showJobTypes[keyfirst] = true;
            this.jobTypes[keyfirst]['job_types'][key][0]['checked'] = true;
            this.jobTypes[keyfirst]['job_cats']['visibility'] = true;
          }          
        });
    });
    console.log(this.selectedJobs);
      console.log(this.jobTypes);
     // this.redirectTo('/');
    }, error => {
      //this.redirectTo('/');
    }
    );
  }


  getJobTypesWithoutSet(jobData: any) {
    var data = {
      "customers_id" : localStorage.getItem('user_id'),
      "job_category_id" : this.catArray
    }

    this.category.getJobtypeWithCategoryDetails(data).subscribe((res) => {
      this.jtypebody = res.data.jobtypes.admin_added;

      this.jobTypes = this.jtypebody;

      this.jobTypes.forEach((valuefirst: any, keyfirst: any) => {
          this.jobTypesArray[keyfirst] = []; 
          let tempitem = [];                  
          valuefirst.job_types.forEach((value: any, key: any) => {
            tempitem[key] = [];
            value.forEach((valuet: any, keyt: any) => {
              if (valuet.customers_id == null || valuet.customers_id == localStorage.getItem('user_id')) {
                let defaultSelect = [];
                tempitem[key]['text'] = valuet.lang[0].name;
                tempitem[key]['value'] = valuet.id;            
                this.jobTypesArray[keyfirst] = tempitem;
                this.source[keyfirst] = tempitem;
                // defaultSelect['text'] = valuet.lang[0].name;
                // defaultSelect['value'] = valuet.id;
                defaultSelect['text'] = 'Search matching job types';
                defaultSelect['value'] = '';  
                this.defaultKey =  keyfirst;                                 
                this.selectedValue[keyfirst] = defaultSelect;             
              }
            });               
          }); 


          this.jobTypes[keyfirst]['job_cats']['visibility'] = false;        
          if (this.jobTypes[keyfirst]['job_cats']['image_path'] == null) {
            this.jobTypes[keyfirst]['job_cats']['image_path'] = 'assets/images/catagerios_06.png';
          } else {
            this.jobTypes[keyfirst]['job_cats']['image_path'] = environment.server_url+'uploads/job_category/' +this.jobTypes[keyfirst]['job_cats']['image_path'];
          }          
      });

      console.log(this.jobTypes);
      console.log(jobData);
      jobData.forEach((value: any, key: any) => {
        //value.job_cate_id;
        this.setJobsWithoutSet(value.autosuggest_id,value.job_cat_id,value.proficiency);
      }); 

     }, error => {
       //this.redirectTo('/');
     }
     );     
  } 
  
  setJobs(jobCatId: any,j: any,jobId: any,i: any) {
    this.showJobTypes[j] = false;
    this.jobTypes[j]['job_types'][i][0]['checked'] = false;
    this.jobTypes[j]['job_cats']['visibility'] = false;    

    this.jobTypes[j]['job_types'].forEach((valuefirst: any, keyfirst: any) => {
        if (valuefirst[0]['checked'] == true) {
          this.showJobTypes[j] = true;
          this.jobTypes[j]['job_cats']['visibility'] = true;                     
        }
    }); 

    // if(typeof this.jobTypes[j]['job_types'][i][0]['checked'] !== 'undefined') {
    //   delete this.jobTypes[j]['job_types'][i][0]['checked']; 
    //   delete this.selectedJobs[jobCatId][jobId];
    //   this.jobTypes[j]['job_cats']['visibility'] = false;  
    //   this.jobTypes.forEach((valuefirst: any, keyfirst: any) => {
    //     valuefirst.job_types.forEach((value: any, key: any) => {
    //       if (this.jobTypes[keyfirst]['job_types'][key][0]['checked'] == true) {
    //         this.jobTypes[keyfirst]['job_cats']['visibility'] = true;           
    //       }
    //     });
    //   });           
    // } else {
    //   this.jobTypes[j]['job_types'][i][0]['checked'] = true;
    //   this.jobTypes[j]['job_cats']['visibility'] = true;       
    //   if(typeof this.selectedJobs[jobCatId] === 'undefined')    {
    //     this.selectedJobs[jobCatId] = [];
    //   }      
    //   this.selectedJobs[jobCatId][jobId] = jobId
    // }

    // let shoTabThreeFlag = false;
    // this.selectedJobs.forEach((value: any, key: any) => {
    //   let tempSelectedJobs = [];

    //   tempSelectedJobs = value.filter(function (el: any) {
    //     return el != null;
    //   });

    // });      
   
  }  


setJobsWithoutSet(jobId: any, jobCatId: any, proficiency: any) {
  this.jobTypes.forEach((valuefirst: any, keyfirst: any) => {
      valuefirst.job_types.forEach((value: any, key: any) => {
        if (value[0].id == jobId && valuefirst.job_cats.job_cate_id == jobCatId) {
          this.showJobTypes[keyfirst] = true;
          this.jobTypes[keyfirst]['job_types'][key][0]['checked'] = true;
          this.jobTypes[keyfirst]['job_types'][key][0]['proficiency'] = proficiency;
          this.jobTypes[keyfirst]['job_cats']['visibility'] = true;           
        } else if(this.tempJobTypes.length !== 0) {
          if (this.tempJobTypes[keyfirst]['job_types'].hasOwnProperty(key)) {          
            if (this.tempJobTypes[keyfirst]['job_types'][key][0].hasOwnProperty('checked')) {
              if (this.tempJobTypes[keyfirst]['job_types'][key][0]['checked'] == true) {
                this.showJobTypes[keyfirst] = true;                
                this.jobTypes[keyfirst]['job_types'][key][0]['checked'] = true;
                this.jobTypes[keyfirst]['job_types'][key][0]['proficiency'] = proficiency;
                this.jobTypes[keyfirst]['job_cats']['visibility'] = true;           
              }
            }
          }
        }

      });
  });

  if(typeof this.selectedJobs[jobCatId] === 'undefined')    {
    this.selectedJobs[jobCatId] = [];
    this.selectedJobs[jobCatId][jobId] = jobId;      
  } else {
    this.selectedJobs[jobCatId][jobId] = jobId;      
  }
  //this.selectedJobs[jobCatId][jobId] = jobId;
}

matchingCriteriaDetails() {
  if (this.tab3jobPostId == "") {
    this.tab3jobPostId =  this.jobPostId;
  }


  var data = {
    "customers_id" : localStorage.getItem('user_id'),
    "jobpost_id" : this.tab3jobPostId
  }
  this.catArray = [];
  this.catOther = [];
  this.jobpostcalls.matchingCriteriaDetails(data).subscribe((res) => {
    res.data.jobcats.forEach((value: any, key: any) => {

      this.catArray[value.job_cate_id] = value.job_cate_id;
      this.catOther[value.job_cate_id] = value.other;      
    });   
    this.getJobTypesWithoutSet(res.data.jobs); 

  }, error => {
    //this.redirectTo('/');
  }
  );
}

matchingCriteriaSave() {
  let flag = true;
  let tempSelectedJobs = [];
  let tempCatArray = [];

  tempCatArray = this.catArray.filter(function (el: any) {
    return el != null;
  });
  //tempCatArray = this.catArray;

  

  if (tempCatArray.length === 0) {
    this.catSelectErrorMessage = 'Please select atleast one job category';
    flag = false;
  } else {
    this.catSelectErrorMessage = '';
  }

  tempSelectedJobs = this.selectedJobs.filter(function (el: any) {
    return el != null;
  });

  //tempSelectedJobs = this.selectedJobs;

  if (tempSelectedJobs.length === 0) {
    // this.jobSelectErrorMessage = 'Please select atleast one job type';
    // flag = false;
    this.jobSelectErrorMessage = '';
  } else {
    this.jobSelectErrorMessage = '';
  }

  if (this.tab2jobPostId != '' && flag) {

    var data = {
      "customerId" : localStorage.getItem('user_id'),
      "jobpost_id" : this.tab2jobPostId,
      // "job_type" : tempSelectedJobs,
      "job_category_id"   : tempCatArray
    }

    this.jobpostcalls.matchingCriteriaSave(data).subscribe((res) => {
      this.tab3Completed = true;
      //this.jobberatorStepThreeDetails();
      setTimeout(() => {
        let el: HTMLElement = this.tabDiv4.nativeElement;
        el.click();
      }, 1000)

    }, error => {

    }
    );
  }
  return false;
}

getAdvancedMatchingDetails() {
  //this.SpinnerService.show();
  if (this.tab5jobPostId == "") {
    this.tab5jobPostId =  this.jobPostId;
  }  
  var data = {
    "jobpost_id" : this.tab5jobPostId
  }    
  this.jobpostcalls.getAdvancedMatchingDetails(data).subscribe((res) => {
    this.higherLevelEducationData.forEach((value: any, key: any) => {
      this.tempHighrlevel[key] = false;
    });    
    res.jobpostInfo.higherlevel.forEach((value: any, key: any) => {
      this.tempHighrlevel[value.autosuggest_id] = true; 
    }); 

    this.skillData.forEach((value: any, key: any) => {
      this.tempSkill[key] = false;
    });    
    res.jobpostInfo.skill.forEach((value: any, key: any) => {
      this.tempSkill[value.autosuggest_id] = true; 
    }); 
 
    this.yearofexpns = res.jobpostInfo.data.year_of_exp;  
    //res.jobpostInfo.higherlevel

  }, error => {
    //this.redirectTo('/');
  }
  );
} 

planChange(planchange: any,planname: any, amount: any) {
  this.amount = amount;  
  if (planname == 'Premium') {
    this.showAdvancedMatching = true;
  } else {
    this.showAdvancedMatching = false;    
  }
  this.plannameSelected = planname;
  this.planId = planchange;
  if (this.renewStatus) {
    this.shopperResultUrl = environment.callback_url+'paycallback/'+this.jobPostId+'/'+this.planId+'/renew'; 
  } else {
    this.shopperResultUrl = environment.callback_url+'paycallback/'+this.jobPostId+'/'+this.planId+'/advert'; 
  }   
  this.tempPlan = []
  this.planData.forEach((value: any, key: any) => {
    if(value.id == this.planId) {
      this.tempPlan[value.id] = true;
    } else {
      this.tempPlan[value.id] = false;
    }
  });   
}

getPaymentDetails() {
  //this.SpinnerService.show();
  var data = {
    "jobpost_id" : this.jobPostId
  }    
  this.jobpostcalls.getPaymentDetails(data).subscribe((res) => {
    this.amount = res.jobpostInfo.plan_perc;
    this.planId = res.jobpostInfo.advertice_plan_settings_id;
    this.oldPlanId = this.planId;
    if (this.renewStatus) {
      this.shopperResultUrl = environment.callback_url+'paycallback/'+this.jobPostId+'/'+this.planId+'/renew'; 
    } else {
      this.shopperResultUrl = environment.callback_url+'paycallback/'+this.jobPostId+'/'+this.planId+'/advert'; 
    }    
    this.tempPlan = []
    this.planData.forEach((value: any, key: any) => {
      if(value.id == this.planId) {
        this.tempPlan[value.id] = true;
        this.plannameSelected = value.plan;
        if (value.plan == 'Premium') {
          this.showAdvancedMatching = true;
        } else {
          this.showAdvancedMatching = false;    
        }        
      } else {
        this.tempPlan[value.id] = false;
      }
    });     

    if (this.renew == 'renew' && this.renewStatus) {
   
      let el: HTMLElement = this.tabDiv1.nativeElement;
      el.click();
 
    }

  }, error => {

  }
  );  
}

adverticementPlanNext() {
  let flag = true;
  if (this.planId == '' || this.planId == null) {
    this.planErrorMessage = 'Please select a plan';
    flag = false;
  } else {
    this.planErrorMessage = '';
  }
  
  if (flag) {
    // setTimeout(() => {
      let el: HTMLElement = this.tabDiv2.nativeElement;
      el.click(); 
    // }, 1000)
  }
  return false;
}

adverticementPlanSave() {
  let flag = true;
  if (this.planId == '' || this.planId == null) {
    this.planErrorMessage = 'Please select a plan';
    flag = false;
  } else {
    this.planErrorMessage = '';
  }
  
  if (this.jobPostId != '' && flag) {
    let planname = '';
    let percentage = '';
    this.planData.forEach((value: any, key: any) => {
      if(value.id == this.planId) {
        planname = value.plan;
        percentage = value.percentage;
      }
    });

    var data = {
      "customerId" : localStorage.getItem('user_id'),
      "jobpost_id" : this.jobPostId,
      "plan_id" : this.planId,
      "plan" : planname,
      "percentage" : percentage
    }

    this.jobpostcalls.adverticementPlanSave(data).subscribe((res) => {
      // let el: HTMLElement = this.tabDiv5.nativeElement;
      // el.click();
      // if (this.plannameSelected == 'Premium') {
      //   this.nextPrevChange('movetab4prev')
      // } else {
        //this.redirectTo('/myjobs');
        this.checkoutId = res.data.id;      
        this.loadAPI = new Promise((resolve) => {
          this.loadScript();
        });         
        this.showPayment = true;  
        jQuery('#payConfirmationPopup').modal('show');         
        // let el: HTMLElement = this.tabDiv6.nativeElement;
        // el.click();             
      // }

    }, error => {

    }
    );
  }
  return false;
}


confirmationListener(value:any) {
  jQuery('#payConfirmationPopup').modal('hide');
  let el: HTMLElement = this.tabDiv6.nativeElement;
  el.click();                             
 }

advancedMatchingSave() {
  let flag = true;
  this.jobProfSelectErrorMessage = '';

  if (this.yearofexpns == '' || this.yearofexpns == null) {
    this.yearofexpnsErrorMessage = 'Please enter your years of experience';
    flag = false;
  } else {
    this.yearofexpnsErrorMessage = '';
  }

  let passHigher = [];
  this.tempHighrlevel.forEach((value: any, key: any) => {
    if (value == true) {
      passHigher[key] = key;
    }
  }); 

  let passSkill = [];
  this.tempSkill.forEach((value: any, key: any) => {
    if (value == true) {
      passSkill[key] = key;
    }
  }); 

  let passHigherNew = passHigher.filter(function (el: any) {
    return el != null;
  });
  
  let passSkillNew = passSkill.filter(function (el: any) {
    return el != null;
  });  

  if (passHigherNew.length == 0) {
    this.higherErrorMessage = 'Please select your higher level education';
    flag = false;
  } else {
    this.higherErrorMessage = '';
  }

  if (passSkillNew.length == 0) {
    // this.skillErrorMessage = 'Please select your skill set';    
    // flag = false;
    this.skillErrorMessage = '';    
  } else {
    this.skillErrorMessage = '';
  }

  let tempSelectedJobs = [];

  tempSelectedJobs = this.selectedJobs.filter(function (el: any) {
    return el != null;
  });  



  let jobsToPass = [];
  this.jobTypes.forEach((valuefirst: any, keyfirst: any) => {
     
    valuefirst.job_types.forEach((value: any, key: any) => {
 
      if(typeof value[0] !== 'undefined')    { 
        if(typeof value[0]['checked'] !== 'undefined')    { 
          if(value[0]['checked'] == true)    { 
            if(typeof jobsToPass[valuefirst.job_cats.job_cate_id] === 'undefined')    {             
              jobsToPass[valuefirst.job_cats.job_cate_id] = [];  
            }              
            if(typeof value[0]['proficiency'] !== 'undefined')    {
              jobsToPass[valuefirst.job_cats.job_cate_id][value[0]['id']] = value[0]['proficiency'];
              if (value[0]['proficiency'] == '0') {
                this.jobProfSelectErrorMessage = "Please give proficiency for all selected job types";
                flag = false;                   
              }
            } else {
              jobsToPass[valuefirst.job_cats.job_cate_id][value[0]['id']] = ''; 
              this.jobProfSelectErrorMessage = "Please give proficiency for all selected job types";
              flag = false;           
            }
          }
        }   
      }   
    });
  }); 

  let catotherflag = false;
  this.catOther.forEach((value: any, key: any) => {
    if (value != null && value != '') {
      catotherflag = true;
    }      
  });     
  

  if (jobsToPass.length === 0 && catotherflag === false) {
    this.jobSelectErrorMessage = 'Please select atleast one job type or enter your job type';
    flag = false;
  } else {
    this.jobSelectErrorMessage = '';
  }  
  
  
  if (this.tab2jobPostId == "") {
    this.tab5jobPostId =  this.jobPostId;
  }

  if (this.tab2jobPostId != "") {
    this.tab5jobPostId =  this.tab2jobPostId;   
  }   

  if (this.tab5jobPostId != '' && flag) {
    var data = {
      "customerId" : localStorage.getItem('user_id'),
      "jobpost_id" : this.tab5jobPostId,
      "year_of_exp" : this.yearofexpns,
      "higherlevel_education" : passHigherNew,
      // "skill" : passSkillNew,
      "plan_id" : this.planId,
      // "job_type" : tempSelectedJobs  
      "job_type" : jobsToPass, 
      "cat_other" : this.catOther         
    }

    this.jobpostcalls.advancedMatchingSave(data).subscribe((res) => {
      //this.redirectTo('/myjobs');
      this.tab5Completed = true;
      this.checkoutId = res.data.id;    
      if (catotherflag) {
        jQuery('#otherPopup').modal('show'); 
      } else {
        // this.loadAPI = new Promise((resolve) => {
        //   this.loadScript();
        // });   
        // if (this.renewStatus) {
        //   this.shopperResultUrl = environment.callback_url+'paycallback/'+this.jobPostId+'/'+this.planId+'/renew'; 
        // } else {
        //   this.shopperResultUrl = environment.callback_url+'paycallback/'+this.jobPostId+'/'+this.planId+'/advert'; 
        // }             
        // this.showPayment = true; 
        // jQuery('#payConfirmationPopup').modal('show');       
        // let el: HTMLElement = this.tabDiv6.nativeElement;
        // el.click(); 
        if (this.jobTaskList.length != 0) {
          var data = {
            "jobpost_id" : this.tab5jobPostId,
            "plan_id" : this.planId, 
            "oldjobpost_id" : this.oldJobPostId                                                                
          }    
          this.jobpostcalls.planSaveWithoutPayment(data).subscribe((res) => {
      
            this.redirectTo('myjobs');       
            // jQuery('#paySuccessPopup').modal('show');
      
          }, error => {
            console.log('error');
            console.log(error);
      
          }
          );
        } else {
          this.addNewTask();           
          let el: HTMLElement = this.tabDiv2.nativeElement;
          el.click();          
        }
        
      }

    }, error => {

    }
    );
  }
  return false;
}


  tabChange(tab: string) {
    //alert(tab);
    this.renewStatus = false;
    this.activeTab = tab;
    // if (tab == 'jbtr1') {
    //   this.getPaymentDetails();
    // }     

    if (tab == 'jbtr3') {
      this.matchingCriteriaDetails();
    }

    if (tab == 'jbtr5') {
      this.getAdvancedMatchingDetails();
    }  
    
    // if (tab == 'jbtr5') {
    //   this.getPaymentDetails();
    // }     
  }

  nextPrevChange(tab: string) {
    if (tab == 'movetab1') {
      let el: HTMLElement = this.tabDiv1.nativeElement;
      el.click();
    }
    if (tab == 'movetab3') {
      this.movetab1status = true;
      this.jobDetailsSave();
      // let el: HTMLElement = this.tabDiv2.nativeElement;
      // el.click();
    }

    if (tab == 'movetab3prev') {
      let el: HTMLElement = this.tabDiv3.nativeElement;
      el.click();
    }    

    if (tab == 'movetab2prev') {
      let el: HTMLElement = this.tabDiv2.nativeElement;
      el.click();    
    }
    if (tab == 'movetab4') {
      this.taskDetailsSave();
    }

    if (tab == 'movetab3prev') {
      let el: HTMLElement = this.tabDiv3.nativeElement;
      el.click();    
    }    

    // if (tab == 'movetab4prev') {
    //   let el: HTMLElement = this.tabDiv4.nativeElement;
    //   el.click();    
    // }   
    
    if (tab == 'movetab5prev') {
      let el: HTMLElement = this.tabDiv4.nativeElement;
      el.click();    
    }    

    return false;
  } 
  
  educationList() {
    this.jobpostcalls.educationList().subscribe((res) => {
      // res.data.nqf.forEach((value: any, key: any) => {
      //   this.nqfData[value.lang[0].autosuggest_id] = value.lang[0].name;
      // });

      // res.data.qualification.forEach((value: any, key: any) => {
      //   this.qualificationData[value.lang[0].autosuggest_id] = value.lang[0].name;
      // });

      res.data.higherLevelEducation.forEach((value: any, key: any) => {
        this.higherLevelEducationData[value.lang[0].autosuggest_id] = value.lang[0].name;
      });

      this.higherLevelEducationData.forEach((value: any, key: any) => {
        this.tempHighrlevel[key] = false;
      });       


      res.data.skill.forEach((value: any, key: any) => {
        this.skillData[value.lang[0].autosuggest_id] = value.lang[0].name;
      });
      
      this.skillData.forEach((value: any, key: any) => {
        this.tempSkill[key] = false;
      });        

      // res.data.institution.forEach((value: any, key: any) => {
      //   this.institutionData[value.lang[0].autosuggest_id] = value.lang[0].name;
      // });
         

    }, error => {
      //this.redirectTo('/');
    }
    );
  }
  
  
  planList() {
    this.jobpostcalls.planList().subscribe((res) => {
      this.planData = res.data;
      this.planFeatures = res.planfeature;

      // res.planfeature.forEach((value: any, key: any) => {
       // this.planFeatures[value.advertice_plan_settings_id] = value.lang[0].name;
      // });      
      // res.data.nqf.forEach((value: any, key: any) => {
      //   this.nqfData[value.lang[0].autosuggest_id] = value.lang[0].name;
      // });

      // res.data.qualification.forEach((value: any, key: any) => {
      //   this.qualificationData[value.lang[0].autosuggest_id] = value.lang[0].name;
     // });

    }, error => {
      //this.redirectTo('/');
    }
    );
  }  

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;
  } 
  
  expertLevel(level: number, j: number, i: number) {
    this.jobTypes[j]['job_types'][i][0]['proficiency'] = level;
  }


}
