import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../_services/commonlisting/api.service';
import { JobpostcallsService} from './../_services/jobpostcalls/jobpostcalls.service'; 
declare var jQuery: any;

@Component({
  selector: 'app-paycallbacktip',
  templateUrl: './paycallbacktip.component.html',
  styleUrls: ['./paycallbacktip.component.css']
})
export class PaycallbacktipComponent implements OnInit {
  payment: boolean;
  constructor(
    private jobpostcalls: JobpostcallsService,       
    private apiService: ApiService,     
    private router: Router,
    private route: ActivatedRoute    
  ) { }

  ngOnInit(): void {
    this.payment = true;
    //alert(this.route.snapshot.params.id);  
    //alert(this.route.snapshot.queryParamMap.get('id'));

    var data = {
      "id" : this.route.snapshot.queryParamMap.get('id')
    }    
    this.jobpostcalls.paymentCheck(data).subscribe((res) => {
      console.log(res.data);
      if (res.data.result.code == "000.100.110") {
        var data = {
          "customerId" : localStorage.getItem('user_id'),
          "jobpost_id" : this.route.snapshot.params.id,
          "cust_id" : this.route.snapshot.params.custid,
          "payment_id" : res.data.id,
          "payment_type" : res.data.paymentType,
          "payment_brand" : res.data.paymentBrand,  
          "amount" : res.data.amount, 
          "currency" : res.data.currency, 
          "descriptor" : res.data.descriptor, 
          "code" : res.data.result.code,   
          "result" : res.data.result, 
          "customer" : res.data.customer, 
          "card" : res.data.card, 
          "custom_parameters" : res.data.customParameters,  
          "buildNumber" : res.data.buildNumber, 
          "ndc" : res.data.ndc, 
          "payment_time" : res.data.timestamp, 
          "entire_response" : res.data                                                                    
        }    
        this.donePayment(data);
      } else {
        this.redirectTo('/myjobs/t3');
      }

    }, error => {
      console.log('error');
      console.log(error);
      //this.redirectTo('/');
    }
    ); 
      
  }

  donePayment(data:any) {

    this.apiService.tippay(data).subscribe((response: any) => {
      //this.redirectTo('myjobs');   
      jQuery('#tipPaySuccessPopupStart').modal('show');    
      
     },
  
       error => {
        this.redirectTo('/myjobs/t3');
       }
     );
  } 

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;
  } 
  
  confirmationListener(value:any) {
    this.payment = false;   
    jQuery('.modal-backdrop').remove(); 
    jQuery('#tipPaySuccessPopupStart').modal('hide');
    // jQuery('#tipPaySuccessPopupStart').modal('hide');
    jQuery('#exampleModalTip').modal('hide');
    this.redirectTo('/myjobs/t3');                  
   }

}
