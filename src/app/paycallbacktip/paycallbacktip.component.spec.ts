import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaycallbacktipComponent } from './paycallbacktip.component';

describe('PaycallbacktipComponent', () => {
  let component: PaycallbacktipComponent;
  let fixture: ComponentFixture<PaycallbacktipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaycallbacktipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaycallbacktipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
