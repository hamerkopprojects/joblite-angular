import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { JobberprofileService } from './../_services/jobberprofile/jobberprofile.service';
import { JobberatorprofileService } from './../_services/jobberatorprofile/jobberatorprofile.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { FileUploader, FileLikeObject } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from './../_services/notification.service';
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { Subscription } from 'rxjs';
// import { MapsAPILoader } from '@agm/core';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';

declare var jQuery: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @ViewChild('tabDiv1') tabDiv1: ElementRef<HTMLElement>;
  @ViewChild('tabDiv2') tabDiv2: ElementRef<HTMLElement>;
  @ViewChild('tabDiv3') tabDiv3: ElementRef<HTMLElement>;

  linkedInCredentials = {
    clientId: "77gg45kc43nbzy",
    // redirectUrl: "https://www.joblite.co.za/linkedinlogin",
    redirectUrl: environment.callback_url +'linkedinlogin',
    // scope: "r_liteprofile%20r_emailaddress%20w_member_social" // To read basic user profile data and email
    scope: "r_liteprofile%20r_emailaddress" // To read basic user profile data and email
  };
  user_type: any;
  idResultUrl: any;
  url = 'https://test.oppwa.com/v1/paymentWidgets.js?checkoutId='; 
  loadAPI: Promise<any>; 
  licenseResultUrl: any;
  checkoutId: any;
  langList: any; 
  titlemessage: string;
  stepData: any;
  width: any;
  height: any;
  movetab1status: boolean;
  latitude: any;
  longitude: any;
  zoom: number;
  address: string;
  private geoCoder: any;
  @ViewChild('search')
  public searchElementRef: ElementRef;

  activeTab: string;
  respbody: any;
  defaultClick: boolean;
  userType: string;
  firstname: string;
  fnameErrorMessage: string;
  street: string;
  streetErrorMessage: string;
  complex: string;
  complexErrorMessage: string;
  surnameErrorMessage: string;
  surname: string;
  personelinfo: string;
  location: string;
  loccoordinates: string;
  locErrorMessage = '';
  maxDob: Date;
  dob: any;
  dobErrorMessage: string;
  genderData: any;
  genderErrorMessage: any;
  genderselected: any;
  raceData: any;
  raceErrorMessage = '';
  raceselected: any;
  languageData: any;
  langErrorMessage = '';
  languageselected: any;
  proficiencyData: any;
  profErrorMessage = '';
  proficiencyselected: any;
  countryData: any;
  countryErrorMessage = '';
  countryselected: any;  
  provinceData: any;
  provinceErrorMessage = '';
  provinceselected: any;

  cityData: any;
  cityErrorMessage = '';
  cityselected: any;

  // suburbData: any;
  suburbErrorMessage = '';
  suburbselected: any;
  //imgData : any;
  profileImg: any;
  viewType: boolean = false;
  maxFileSize = 5 * 1024 * 1024;
  public uploader: FileUploader = new FileUploader(
    {
      maxFileSize: this.maxFileSize
    }
  );
  // let d = new Date();
  // d.setFullYear(2020, 11, 3); 
  form = new FormGroup({
    picker: new FormControl('', [Validators.required]),
    firstname: new FormControl('', [Validators.required]),
    surname: new FormControl('', [Validators.required]),
    street: new FormControl('', [Validators.required]),
    complex: new FormControl('', [Validators.required]),
    personelinfo: new FormControl('', [Validators.required]),    
    location: new FormControl('', [Validators.required]),
    genderselected: new FormControl('', [Validators.required]),
    languageselected: new FormControl('', [Validators.required]),
    raceselected: new FormControl('', [Validators.required]),
    proficiencyselected: new FormControl('', [Validators.required]),
    countryselected: new FormControl('', [Validators.required]),
    provinceselected: new FormControl('', [Validators.required]),
    cityselected: new FormControl('', [Validators.required]),
    suburbselected: new FormControl('', [Validators.required])
  });

  myControl = new FormControl();
  suburbData: Observable<string[]>;
  options:  string[];  

  phone: string;
  idcardnumber: string;
  licensenumber: string;  
  idcard_verify: string;
  license_verify: string;
  linkedin_verify: string;
  facebook_verify: string;
  google_verify: string;
  phone_verify: string;
  otpcheck: boolean; 
  otpval: string; 
  otpErrorMessage: string;
  facebook: string;
  google: string;
  linkedin: string;    
  twitter: string;  
  phoneErrorMessage = '';
  idCard: string; 
  dlCard: string;
  public idcarduploader: FileUploader = new FileUploader({});
  public dlcarduploader: FileUploader = new FileUploader({});

  formveri = new FormGroup({
    phone: new FormControl('', [Validators.required]),
    otpval: new FormControl(''),    
    idcardnumber: new FormControl(''),
    licensenumber: new FormControl(''),    
    facebook: new FormControl(''),
    google: new FormControl(''),
    linkedin: new FormControl(''),
    twitter: new FormControl('')
  });


  bankname: string;
  branch: string;
  accountno: string;    
  accounttype: string;  
  bnameErrorMessage = '';
  branchErrorMessage = '';
  accountnoErrorMessage = '';
  accounttypeErrorMessage = '';    
  formbank = new FormGroup({
    bankname: new FormControl('', [Validators.required]),
    branch: new FormControl('', [Validators.required]),
    accountno: new FormControl('', [Validators.required]),
    accounttype: new FormControl('', [Validators.required])
  });

  defaultTask: boolean;

  center: any;
  defaultBounds: any;
  input: any;
  mapoptions: any;
  autocomplete: any;
  private authSubscription: Subscription;

  constructor(private SpinnerService: NgxSpinnerService,
                      private jobberator: JobberatorprofileService,
                      private notifyService : NotificationService, 
                      private authService: SocialAuthService,
                      private jobber: JobberprofileService,
                      private router: Router,
                      private route: ActivatedRoute,
                      // private mapsAPILoader: MapsAPILoader,
                      private ngZone: NgZone                      
                      ) {
                       }

  ngOnDestroy(): void {
    // Unsubscribe from the subscription to prevent memory leaks
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.defaultTask = false;
    this.user_type = '';
    this.authSubscription = this.authService.authState.subscribe((user) => {

      let userType: any;
      userType = localStorage.getItem('user_type_click');
      if (localStorage.getItem('user_email') == user.email) {
        if (userType == 'facebook') {
          let dataver = {
            "customerId" : localStorage.getItem('user_id'),
            "username"  : user.photoUrl            
          }         
          this.jobber.facebookverify(dataver).subscribe((resver) => {
            console.log(resver);
            this.facebook_verify = 'yes'; 
            this.titlemessage = 'Verified facebook account successfully';
            this.showToasterSuccess(); 
            return false;
            //this.jobberatorStepTwoDetails();             
      
          }, error => {
            alert(error.Error);
            this.SpinnerService.hide();
          }
          ); 
          return false;
        }


        if (userType == 'google') {
          let dataver = {
            "customerId" : localStorage.getItem('user_id'),
            "username"  : user.photoUrl            
          }         
          this.jobber.googleverify(dataver).subscribe((resver) => {
            console.log(resver);  
            this.google_verify = 'yes';
            this.titlemessage = 'Verified google account successfully';
            this.showToasterSuccess(); 
            return false;
            //this.jobberatorStepTwoDetails();             

          }, error => {
            alert(error.Error);
            this.SpinnerService.hide();
          }
          ); 
        }
      } else {
        this.titlemessage = 'Your email is not matching';
        this.showToasterError(); 
        return false;        
      }
    });

    this.idResultUrl = environment.callback_url+'/paycallbackid/'+localStorage.getItem('user_id');
    this.licenseResultUrl = environment.callback_url+'/paycallbacklicense/'+localStorage.getItem('user_id');  
    this.langList = [];
    this.stepsSaved(); 
    this.titlemessage = '';
    this.stepData = [];

    this.center = { lat: -26.195246, lng: 28.034088 };
    // Create a bounding box with sides ~10km away from the center point
    this.defaultBounds = {
      north: this.center.lat + 0.1,
      south: this.center.lat - 0.1,
      east: this.center.lng + 0.1,
      west: this.center.lng - 0.1,
    };
    this.input = document.getElementById("pac-input") as HTMLInputElement;
    this.mapoptions = {
      bounds: this.defaultBounds,
      componentRestrictions: { country: "za" },
      fields: ["address_components", "geometry", "icon", "name"],
      strictBounds: false,
      types: ["establishment"],
    };
    
    this.autocomplete = new google.maps.places.Autocomplete(this.input, this.mapoptions);  
    
    this.autocomplete.addListener("place_changed", () => {
  
    const place = this.autocomplete.getPlace();
      this.loccoordinates = place.geometry.location.lat()+
      ','+place.geometry.location.lng();       
      this.location = place.name+', '+place.address_components[1].long_name
      +', '+place.address_components[5].long_name
      +', '+place.address_components[5].short_name
      +', '+place.address_components[6].short_name; 
    }); 

    this.otpcheck = false;
    this.otpval = '';
    this.otpErrorMessage = '';
    this.width = '';
    this.height = '';
    this.movetab1status = false;
    this.activeTab = 'jbtr1';
    this.defaultClick = true;
    this.userType = '';
    this.firstname = '';
    this.fnameErrorMessage = '';
    this.surnameErrorMessage = '';
    this.surname = '';
    this.street = '';
    this.streetErrorMessage = '';
    this.complex = '';
    this.complexErrorMessage = '';
    this.location = '';
    this.locErrorMessage = '';
    this.dob = '';
    const today = new Date();
    this.maxDob = new Date(
      today.getFullYear() - 12,
      today.getMonth(),
      today.getDate()
    );
    this.dobErrorMessage = '';
    this.genderData = ['male','female'];
    this.genderErrorMessage = '';
    this.genderselected = '';
    this.raceData = [];
    this.raceErrorMessage = '';
    this.raceselected = '';
    this.languageData = [];
    this.langErrorMessage = '';
    this.languageselected = '';
    this.proficiencyData = [];
    this.profErrorMessage = '';
    this.proficiencyselected = '';

    this.countryData = [];
    this.countryErrorMessage = '';
    this.countryselected = '';

    this.provinceData = [];
    this.provinceErrorMessage = '';
    this.provinceselected = '';

    this.cityData = [];
    this.cityErrorMessage = '';
    this.cityselected = '';

    // this.suburbData = [];
    this.options = [];
    this.suburbErrorMessage = '';
    this.suburbselected = '';

    this.profileImg = 'assets/images/image_upload.png';
    this.personelinfo = '';

    this.langProficiencyRaceList();
    this.uploader.onAfterAddingFile = (file) => {
      this.setPreview(file);
     };

    this.uploader.onWhenAddingFileFailed = (item, filter) => {
      let message = '';
      switch (filter.name) {
        case 'fileSize':
          message = 'Warning ! \nThe uploaded file \"' + item.name + '\" is ' + this.formatBytes(item.size) + ', this exceeds the maximum allowed size of ' + this.formatBytes(this.maxFileSize);
          break;
        default:
          message = 'Error trying to upload file '+item.name;
          break;
      }
  
      // alert(message);
      this.titlemessage = message;  
      this.showToasterError(); 
    };


     this.phone = '';
     this.idcardnumber = '';
     this.licensenumber = '';
     this.idcard_verify = 'no';
     this.phone_verify = 'no';
     this.license_verify = 'no';
     this.linkedin_verify = 'no';
     this.facebook_verify = 'no';
     this.google_verify = 'no';
     this.facebook = '';
     this.google = '';
     this.linkedin = '';
     this.twitter = '';  
     this.idCard = '';
     this.dlCard = '';
     this.idcarduploader.onAfterAddingFile = (file) => {
      this.setIdPreview(file);
     };

     this.dlcarduploader.onAfterAddingFile = (file) => {
      this.setDlPreview(file);
     };

     this.bankname = '';
     this.branch = '';
     this.accountno = '';
     this.accounttype = '';
     this.jobberatorStepThreeDetails();  
     this.educationListFunc();   
    //this.jobberatorStepOneDetails(); 
    if(this.userType == 'jobber'){
      this.viewType = true;
    }
  }


  addNewLangOpen() {
    this.defaultTask = true;    
  }

  removeEmptyTask() {
    this.defaultTask = false;      
  }

  formatBytes(bytes: any, decimals?: any) {
    if (bytes == 0) return '0 Bytes';
    const k = 1024,
      dm = decimals || 2,
      sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  loadScript() {
    let node = document.createElement('script');
    node.src = this.url+''+this.checkoutId;
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }  



  educationListFunc() {
    
    var data = {
      "customerId" : localStorage.getItem('user_id')
    }    
    this.jobber.langProfList(data).subscribe((res) => {
      this.langList = [];
      this.langList = res.langs;   

    }, error => {
      //this.redirectTo('/');
    }
    );
  }

    

  addNewLang() {
    let flag = true;
    this.defaultTask = true;

    if (this.languageselected == '') {
      this.langErrorMessage = 'Please select your language';
      flag = false;
    } else {
      this.langErrorMessage = '';
    }

    if (this.proficiencyselected == '') {
      this.profErrorMessage = 'Please select your proficiency';
      flag = false;
    } else {
      this.profErrorMessage = '';
    }


 

      let data = {
        "customerId" : localStorage.getItem('user_id'),
        "lang_id"  : this.languageselected,
        "prof_id"  : this.proficiencyselected           
      }

      if (flag) {
      
        this.jobber.langPrefAdd(data).subscribe((res) => {
          this.defaultTask = false;
          this.langList = [];
          this.langList = res.langs;  
          this.languageselected = '';
          this.proficiencyselected = '';
        }, error => {
          this.SpinnerService.hide();
        }
        ); 
      }     
   
  }


  removeLang(mainId: any) {
    let data = {
      "customerId" : localStorage.getItem('user_id'),
      "id"  : mainId     
    }
    this.jobber.langJobberRemove(data).subscribe((res) => {
      this.langList = [];
      this.langList = res.langs;
    }, Error => {
      this.SpinnerService.hide();
    }
    );
  } 

  updateLanguage(mainId: any,key: any) {
    let flag = true;

    if (this.langList[key].lang_autosuggest_id == '' || this.langList[key].lang_autosuggest_id == null) {
      flag = false;
    }

    if (this.langList[key].pref_autosuggest_id == '' || this.langList[key].pref_autosuggest_id == null) {
      flag = false;
    }


    if (flag) {    
      let data = {
        "customerId" : localStorage.getItem('user_id'),
        "id"  : mainId,
        "lang_id"  : this.langList[key].lang_autosuggest_id,
        "pref_id"  : this.langList[key].pref_autosuggest_id             
      }

      this.jobber.langJobberUpdate(data).subscribe((res) => {
        this.langList = [];
        this.langList = res.langs; 
      }, Error => {
        this.SpinnerService.hide();
      }
      );
    }
  }

  stepsSaved() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobber.stepsSaved(data).subscribe((res) => {
      this.stepData = res;

    }, error => {
      //this.redirectTo('/');
    }
    );
  }

  private _filter(value: string): string[] {
    console.log(value);
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }   

  public setPreview(file:any) {
    console.log("file",file);
    file.withCredentials = false;
    let fr = null;
    fr = new FileReader();
    fr.onload = () => {
      //console.log(fr.result);
      this.profileImg = fr.result;
    }
    fr.readAsDataURL(file._file);
    this.upload();
  }

  public setIdPreview(file:any) {
    console.log("file",file);
    file.withCredentials = false;
    let fr = null;
    fr = new FileReader();
    fr.onload = () => {
      //console.log(fr.result);
      this.idCard = fr.result;
    }
    fr.readAsDataURL(file._file);
    this.idcardupload();
  }

  public setDlPreview(file:any) {
    console.log("file",file);
    file.withCredentials = false;
    let fr = null;
    fr = new FileReader();
    fr.onload = () => {
      //console.log(fr.result);
      this.dlCard = fr.result;
    }
    fr.readAsDataURL(file._file);
    this.dlcardupload();
  }

  getFiles(): FileLikeObject[] {
    return this.uploader.queue.map((fileItem) => {
      return fileItem.file;
    });
  }

  // uploadBeginning(evt: any) {
  //   let image:any = evt.target.files[0];
  //   console.log(image);
  //   let fr = new FileReader();
  //   fr.onload = () => { // when file has loaded
  //     var img = new Image();
  //     img.onload = () => {
  //         this.width = img.width;
  //         this.height = img.height;
  //     };
  //   }
  //   console.log(fr);
  // }

  upload() {
    let id = localStorage.getItem('user_id');
    let files = this.getFiles();
    let file = files[files.length - 1];
    // files.forEach((file,index) => {
      let formData = new FormData();
      formData.append('profile_img', file.rawFile, file.name);
      this.jobberator.upload(formData, id).subscribe((res) => {
        console.log('success');
        console.log(res);
  
      }, error => {
        console.log('error');
        console.log(error);
        //this.redirectTo('/');
      }
      );
      // if(index == files.length-1){
      //  this.router.navigateByUrl('/myads');
      // }
    // });
  }

  getIdFiles(): FileLikeObject[] {
    return this.idcarduploader.queue.map((fileItem) => {
      return fileItem.file;
    });
  }

  getDlFiles(): FileLikeObject[] {
    return this.dlcarduploader.queue.map((fileItem) => {
      return fileItem.file;
    });
  }

  idcardupload() {
    let id = localStorage.getItem('user_id');
    let files = this.getIdFiles();
    files.forEach((file,index) => {
      let formData = new FormData();
      formData.append('id_img', file.rawFile, file.name);
      this.jobberator.idcardupload(formData, id).subscribe((res) => {
        console.log('success');
        console.log(res);
  
      }, error => {
        console.log('error');
        console.log(error);
        //this.redirectTo('/');
      }
      );
      // if(index == files.length-1){
      //  this.router.navigateByUrl('/myads');
      // }
    });
  }


  dlcardupload() {
    let id = localStorage.getItem('user_id');
    let files = this.getDlFiles();
    files.forEach((file,index) => {
      let formData = new FormData();
      formData.append('driving_licence', file.rawFile, file.name);
      this.jobberator.dlcardupload(formData, id).subscribe((res) => {
        console.log('success');
        console.log(res);
  
      }, error => {
        console.log('error');
        console.log(error);
        //this.redirectTo('/');
      }
      );
      // if(index == files.length-1){
      //  this.router.navigateByUrl('/myads');
      // }
    });
  }
  
  step1Save() {
    let flag = true;

    if (this.langList.length == 0 || this.defaultTask) {
      if (this.languageselected == '') {
        this.langErrorMessage = 'Please select your language';
        flag = false;
      } else {
        this.langErrorMessage = '';
      }
  
      if (this.proficiencyselected == '') {
        this.profErrorMessage = 'Please select your proficiency';
        flag = false;
      } else {
        this.profErrorMessage = '';
      }
      this.addNewLang();
    }

    if (this.firstname == '') {
      this.fnameErrorMessage = 'Please enter your first name';
      flag = false;
    } else {
      this.fnameErrorMessage = '';
    }

    if (this.surname == '') {
      this.surnameErrorMessage = 'Please enter your last name';
      flag = false;
    } else {
      this.surnameErrorMessage = '';
    }

    if (this.street == '' || this.street == null) {
      this.streetErrorMessage = 'Please enter your street address';
      flag = false;
    } else {
      this.streetErrorMessage = '';
    }

    if (this.complex == '' || this.complex == null) {
      this.complexErrorMessage = 'Please enter your complex and unit number';
      flag = false;
    } else {
      this.complexErrorMessage = '';
    }

    if (this.dob == '' || !this.dob) {
      this.dobErrorMessage = 'Please select your dob';
      flag = false;
    } else {
      this.dobErrorMessage = '';
    }

    if (this.genderselected == '') {
      this.genderErrorMessage = 'Please select your gender';
      flag = false;
    } else {
      this.genderErrorMessage = '';
    }

    if (this.countryselected == '') {
      this.countryErrorMessage = 'Please select your country';
      flag = false;
    } else {
      this.countryErrorMessage = '';
    }

    if (this.provinceselected == '') {
      this.provinceErrorMessage = 'Please select your province';
      flag = false;
    } else {
      this.provinceErrorMessage = '';
    }

    if (this.cityselected == '') {
      this.cityErrorMessage = 'Please select your city';
      flag = false;
    } else {
      this.cityErrorMessage = '';
    }

    if (this.suburbselected == '') {
      this.suburbErrorMessage = 'Please select your suburb';
      flag = false;
    } else {
      if (this.options.includes(this.suburbselected)) {
        this.suburbErrorMessage = '';
      } else {
        this.suburbErrorMessage = 'Please select an available suburb';
        flag = false;    
      }
    }
    
    if (this.location == '') {
      this.locErrorMessage = 'Please enter your location';
      flag = false;
    } else {
      this.locErrorMessage = '';
    }
    

    if (flag) {
      // if (this.raceselected == '') {
      //   this.raceselected = [];
      // } else {
      //   if (typeof(this.raceselected) !== 'object') {
      //     this.raceselected = [this.raceselected];
      //   }
      // } 
  
      // if (this.languageselected == '') {
      //   this.languageselected = [];
      // } else {
      //   if (typeof(this.languageselected) !== 'object') {
      //     this.languageselected = [this.languageselected];
      //   }
      // }
  
      // if (this.proficiencyselected == '') {
      //   this.proficiencyselected = [];
      // } else {
      //   if (typeof(this.proficiencyselected) !== 'object') {
      //     this.proficiencyselected = [this.proficiencyselected];
      //   }
      // }

      let suburbselectedtopass = '';
      this.options.forEach((value: any, key: any) => {
        if (value == this.suburbselected) {
          suburbselectedtopass = key;
        }
      });       

      // this.location = this.latitude + ',' + this.longitude;

      var data = {
        "customerId" : localStorage.getItem('user_id'),
        "first_name" : this.firstname,
        "last_name"  : this.surname,
        "street_address" : this.street,
        "comp_unit_no" : this.complex,
        "dob"        : this.dob.getFullYear()+ '-' + ('0' + (this.dob.getMonth()+1)).slice(-2)+ '-' +('0' + (this.dob.getDate())).slice(-2),
        "gender"     : this.genderselected,
        "location"   : this.location,
        // "race"       : this.raceselected,
        // "language"   : this.languageselected,
        // "proficiency": this.proficiencyselected,
        "personal_info": this.personelinfo,
        "country_id": this.countryselected,
        "province_id": this.provinceselected,
        "city_id": this.cityselected,
        "suburb_id": suburbselectedtopass
      }
      //console.log(data);
      this.jobberator.step1Save(data).subscribe((res) => {
        this.stepData.data.jobberatorinfo.tab1 = '1';
        if (this.movetab1status) {
          this.movetab1status = false;
          setTimeout(() => {
            let el: HTMLElement = this.tabDiv2.nativeElement;
            el.click();
          }, 1000)
        }

      }, error => {
        console.log('error');
        console.log(error);
        //this.redirectTo('/');
      }
      );
    }
    return false;
  }

  step2Save() {
    let flag = true;


    if (this.phone == '' || this.phone == null) {
      this.phoneErrorMessage = 'Please enter your phone number';
      flag = false;
    } else {
      this.phoneErrorMessage = '';
    }

    if (this.twitter == null) {
      this.twitter = '';
    }

    if (this.linkedin == null) {
      this.linkedin = '';
    }

    if (this.facebook == null) {
      this.facebook = '';
    }

    if (this.google == null) {
      this.google = '';
    }

    if (this.idcardnumber == null) {
      this.idcardnumber = '';
    }
    
    if (this.licensenumber == null) {
      this.licensenumber = '';
    }    

    if (flag) {

      var data = {
        "customerId" : localStorage.getItem('user_id'),
        "phone" : this.phone,
        "twitter_url"   : this.twitter,
        "linkedin_url": this.linkedin,
        "facebook_url": this.facebook,
        "google_url": this.google,
        "idcardnumber": this.idcardnumber,
        "licensenumber": this.licensenumber,
        "phone_verify": this.phone_verify,
        "idcard_verify": this.idcard_verify,
        "license_verify": this.license_verify,
        "facebook_verify": this.facebook_verify,
        "google_verify": this.google_verify,
        "linkedin_verify": this.linkedin_verify
      }
      //console.log(data);
      this.jobber.step2Save(data).subscribe((res) => {
        console.log('success');
        console.log(res);
        //this.jobberatorStepThreeDetails();
        if (this.bankname == '') {
          this.redirectTo('/personelinfo');
        } else {
          let el: HTMLElement = this.tabDiv3.nativeElement;
          el.click();
        }
      }, error => {
        console.log('error');
        console.log(error);
        //this.redirectTo('/');
      }
      );
    }
    return false;
  }

  step3Save() {
    let flag = true;


    if (this.bankname == '' || this.bankname == null) {
      this.bnameErrorMessage = 'Please enter your bank name';
      flag = false;
    } else {
      this.bnameErrorMessage = '';
    }

    if (this.branch == '' || this.branch == null) {
      this.branchErrorMessage = 'Please enter your branch';
      flag = false;
    } else {
      this.branchErrorMessage = '';
    }

    if (this.accountno == '' ||  this.accountno == null) {
      this.accountnoErrorMessage = 'Please enter your account number';
      flag = false;
    } else {
      this.accountnoErrorMessage = '';
    }

    if (this.accounttype == '' ||  this.accounttype == null) {
      this.accounttypeErrorMessage = 'Please enter your account type';
      flag = false;
    } else {
      this.accounttypeErrorMessage = '';
    }

    if (flag) {

      var data = {
        "customerId" : localStorage.getItem('user_id'),
        "bank_name" : this.bankname,
        "bank_branch"   : this.branch,
        "account_no": this.accountno,
        "account_type": this.accounttype

      }
      //console.log(data);
      this.jobberator.step3Save(data).subscribe((res) => {
        console.log('success');
        console.log(res);
        this.redirectTo('/personelinfo');

      }, error => {
        console.log('error');
        console.log(error);
        //this.redirectTo('/');
      }
      );
    }
    return false;
  }

      // Get Current Location Coordinates
  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  private setChangedLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  markerDragEnd($event: google.maps.MouseEvent) {
    console.log($event);
    this.latitude = $event.latLng.lat();
    this.longitude = $event.latLng.lng();
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }

  langProficiencyRaceList() {
    this.jobberator.langProficiencyRaceList().subscribe((res) => {
      res;
      console.log(res);
      res.data.languages.forEach((value: any, key: any) => {
        //this.languageData.push(value.lang[0].name);
        this.languageData[value.lang[0].autosuggest_id] = value.lang[0].name;
      });

      res.data.race.forEach((value: any, key: any) => {
        //this.languageData.push(value.lang[0].name);
        this.raceData[value.lang[0].autosuggest_id] = value.lang[0].name;
      });

      res.data.proficiency.forEach((value: any, key: any) => {
        //this.languageData.push(value.lang[0].name);
        this.proficiencyData[value.lang[0].autosuggest_id] = value.lang[0].name;
      });

      res.data.country.forEach((value: any, key: any) => {
        //this.languageData.push(value.lang[0].name);
        this.countryData[value.lang[0].country_id] = value.lang[0].name;
      });
      
      res.data.province.forEach((value: any, key: any) => {
        //this.languageData.push(value.lang[0].name);
        this.provinceData[value.lang[0].province_id] = value.lang[0].name;
      });

      res.data.city.forEach((value: any, key: any) => {
        //this.languageData.push(value.lang[0].name);
        this.cityData[value.lang[0].city_id] = value.lang[0].name;
      });

      res.data.suburb.forEach((value: any, key: any) => {
        //this.languageData.push(value.lang[0].name);
        // this.suburbData[value.lang[0].suburb_id] = value.lang[0].name;
        this.options[value.lang[0].suburb_id] = value.lang[0].name;
        //this.options.push(value.lang[0].name);
      });

      this.suburbData = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );        

      this.jobberatorStepOneDetails(); 

    }, error => {
      //this.redirectTo('/');
    }
    );
  }

  jobberatorStepOneDetails() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobberator.jobberatorStepOneDetails(data).subscribe((res) => {
      this.respbody = res;
      console.log(this.respbody);
      //this.dob = res.data.customerInfo.dob;
      //this.dobval = '3/4/2021';
      if (res.data.customerInfo.dob == null || res.data.customerInfo.dob == 'null') {
        this.dob = '';
      } else {
        var resDate = res.data.customerInfo.dob.split("-");
        this.dob = new Date(resDate[0], resDate[1] - 1, resDate[2]);
      }

      if (res.data.customerInfo.gender == null || res.data.customerInfo.gender == 'null') {
        this.genderselected = '';
      } else {
        this.genderselected = res.data.customerInfo.gender;
      }

      if (res.data.customerLang.length > 0) {
        this.languageselected = res.data.customerLang[0].autosuggest_id;        
      } else {
        this.languageselected = '';
      }

      if (res.data.customerRace.length > 0) {
        this.raceselected = res.data.customerRace[0].autosuggest_id;        
      } else {
        this.raceselected = '';
      }

      if (res.data.customerProf.length > 0) {
        this.proficiencyselected = res.data.customerProf[0].autosuggest_id;        
      } else {
        this.proficiencyselected = '';
      }

      if (res.data.customerInfo.country_id == null || res.data.customerInfo.country_id == 'null') {
        this.countryselected = '';
      } else {
        this.countryselected = res.data.customerInfo.country_id;
      }


      if (res.data.customerInfo.province_id == null || res.data.customerInfo.province_id == 'null') {
        this.provinceselected = '';
      } else {
        this.provinceselected = res.data.customerInfo.province_id;
      }


      if (res.data.customerInfo.city_id == null || res.data.customerInfo.city_id == 'null') {
        this.cityselected = '';
      } else {
        this.cityselected = res.data.customerInfo.city_id;
      }


  
      if (res.data.customerInfo.suburb_id == null || res.data.customerInfo.suburb_id == 'null') {
        this.suburbselected = '';
      } else {
        //this.suburbselected = res.data.customerInfo.suburb_id;
        this.suburbselected = this.options[res.data.customerInfo.suburb_id];
      }



      // let d = new Date();
      // d.setFullYear(2020, 11, 3); 
      this.firstname = res.data.customerInfo.first_name;
      this.surname = res.data.customerInfo.last_name;
      this.location = res.data.customerInfo.location;

      if (res.data.customerInfo.street_address == 'null' || res.data.customerInfo.street_address == null) {
        this.street = '';  
      } else {
        this.street = res.data.customerInfo.street_address;
      }

      if (res.data.customerInfo.comp_unit_no == 'null' || res.data.customerInfo.comp_unit_no == null) {
        this.complex = '';  
      } else {
        this.complex = res.data.customerInfo.comp_unit_no;
      }

      if (res.data.customerInfo.personal_info == 'null' || res.data.customerInfo.personal_info == null) {
        this.personelinfo = '';  
      } else {
        this.personelinfo = res.data.customerInfo.personal_info;
      }

      if (res.data.customerInfo.profile_img == 'null' || res.data.customerInfo.profile_img == null) {
        this.profileImg = 'assets/images/image_upload.png';  
      } else {
        // this.profileImg = environment.server_url+res.data.customerInfo.profile_img;
        if (res.data.customerInfo.user_type == 'normal' || res.data.customerInfo.profile_img.includes('uploads/user_profile/')) {
          this.profileImg = environment.server_url + res.data.customerInfo.profile_img;  
        } else {
          let x = Math.floor((Math.random() * 10000) + 1);
          // Remove backslashes from the image URL
          if (res.data.customerInfo.user_type == 'linkedin' || res.data.customerInfo.user_type == 'facebook') {
            this.profileImg = res.data.customerInfo.profile_img.replace(/\//g, '/');
          } else {
            this.profileImg = res.data.customerInfo.profile_img+'?rand='+x;
          }
        }
      }

      this.user_type = res.data.customerInfo.user_type;

      if (res.data.customerInfo.type == 'null' || res.data.customerInfo.type == null) {
        this.userType = '';  
      } else {
        this.userType = res.data.customerInfo.type; 
      }

      /*
      if (res.data.customerInfo.location != 'null' && res.data.customerInfo.location != null && res.data.customerInfo.location != '') {
        // alert('entere');
        console.log(this.location);
        let latlong = this.location.split(",");
        console.log(latlong);
        this.latitude = Number(latlong[0]);
        this.longitude = Number(latlong[1]);
        //load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
          this.setChangedLocation();
          //this.setCurrentLocation();
          this.geoCoder = new google.maps.Geocoder;

          let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
          autocomplete.addListener("place_changed", () => {
            this.ngZone.run(() => {
              //get the place result
              let place: google.maps.places.PlaceResult = autocomplete.getPlace();

              //verify result
              if (place.geometry === undefined || place.geometry === null) {
                return;
              }

              //set latitude, longitude and zoom
              // this.latitude = place.geometry.location.lat();
              // this.longitude = place.geometry.location.lng();
              this.zoom = 12;
            });
          });
        });
        this.setChangedLocation();
      } else {

        //load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
          this.setCurrentLocation();
          this.geoCoder = new google.maps.Geocoder;

          let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
          autocomplete.addListener("place_changed", () => {
            this.ngZone.run(() => {
              //get the place result
              let place: google.maps.places.PlaceResult = autocomplete.getPlace();

              //verify result
              if (place.geometry === undefined || place.geometry === null) {
                return;
              }

              //set latitude, longitude and zoom
              this.latitude = place.geometry.location.lat();
              this.longitude = place.geometry.location.lng();
              this.zoom = 12;
            });
          });
        });
        this.setCurrentLocation();        
      } */

    if (this.defaultClick) {
      let el: HTMLElement = this.tabDiv1.nativeElement;
      el.click();
    }    
    this.defaultClick = false;
    
    let userType: any;
    userType = localStorage.getItem('user_type_click');
    if (userType == 'linkedin') {
     setTimeout(() => {
       let el: HTMLElement = this.tabDiv2.nativeElement;
       el.click();
       localStorage.removeItem('user_type_click');
     }, 1000);
   }

      // this.respbody.forEach((value: any, key: any) => {
      //   //this.respbody[key]['details'][0]['image_path'] = environment.server_url+'uploads/job_category/' +this.respbody[key]['details'][0]['image_path']; 
      // });
      // console.log(this.respbody);
     // this.redirectTo('/');
    }, error => {
      //this.redirectTo('/');
    }
    );
  }

  otp() {
    this.otpcheck = true;
    this.titlemessage = 'Please enter OTP recieved in phone';
    this.showToasterSuccess();
  }

  verify(type: string) {
    if (type == 'linkedin') {
      let dataver = {
        "customerId" : localStorage.getItem('user_id'),
        "username"  : this.linkedin            
      }         
      this.jobber.lnverify(dataver).subscribe((resver) => {
        console.log(resver);  
        this.jobberatorStepTwoDetails();             
  
      }, error => {
        alert(error.Error);
        this.SpinnerService.hide();
      }
      );       
    } else if (type == 'facebook') {
      let dataver = {
        "customerId" : localStorage.getItem('user_id'),
        "username"  : this.facebook            
      }         
      this.jobber.facebookverify(dataver).subscribe((resver) => {
        console.log(resver);  
        this.jobberatorStepTwoDetails();             
  
      }, error => {
        alert(error.Error);
        this.SpinnerService.hide();
      }
      );       
    } else if (type == 'google') {
      let dataver = {
        "customerId" : localStorage.getItem('user_id'),
        "username"  : this.google            
      }         
      this.jobber.googleverify(dataver).subscribe((resver) => {
        console.log(resver);  
        this.jobberatorStepTwoDetails();             

      }, error => {
        alert(error.Error);
        this.SpinnerService.hide();
      }
      );       
    }  else if (type == 'phone') {

      if (this.otpval == '1234') {
        let dataver = {
          "customerId" : localStorage.getItem('user_id'),
          "username"  : this.phone,
          "otp" : this.otpval            
        }         
        this.jobber.phoneverify(dataver).subscribe((resver) => {
          console.log(resver); 
          if (resver.valid == '1') { 
            this.titlemessage = 'Successfully verified';
            this.showToasterSuccess();
            this.jobberatorStepTwoDetails();  
          } else {
            this.phoneErrorMessage = 'Invalid number';
            this.titlemessage = 'Invalid number';  
            this.showToasterError();  
          }           
    
        }, error => {
          alert(error.Error);
          this.SpinnerService.hide();
        }
        ); 
  
        this.otpcheck = false; 
        this.otpval = '';
        this.otpErrorMessage = ''; 
      } else {
        this.otpcheck = true; 
        //this.otpErrorMessage = 'Please enter your recieved OTP';     
        this.titlemessage = 'OTP is invalid';
        this.showToasterError();    
      }
    
    } else {
      if (type == 'idcard') {
        // let dataver = {
        //   "user" : {
        //     "firstName" : "Gregory",
        //     "lastName" : "Oxley",
        //     "identifier" : "9312225037084"
        //   }
        // } 

        if (this.idcardnumber.trim() !== '') {
          let data = {};
          this.jobberator.idVerify(data).subscribe((res) => {
            this.checkoutId = res.data.id;
            this.loadAPI = new Promise((resolve) => {
              this.loadScript();
            }); 
            this.idResultUrl = environment.callback_url+'/paycallbackid/'+localStorage.getItem('user_id')+'/'+this.firstname+'/'+this.surname+'/'+this.idcardnumber+'/profile';
            jQuery('#idVerifyPopup').modal('show');
          }, Error => {
            this.SpinnerService.hide();
          }
          );
        } else {
          this.titlemessage = 'Id card number is empty';
          this.showToasterError();
        }

        
        /*let dataver = {
          "user" : {
            "firstName" : this.firstname,
            "lastName" : this.surname,
            "identifier" : this.idcardnumber
          }
        } 
        this.jobber.idverifylatest(dataver).subscribe((resver) => {
          let idcard_verify: any;
          if (resver.code == 'GOOD') {
            idcard_verify = 'yes';
          } else {
            idcard_verify = 'no';
          }
          let dataver = {
            "customerId" : localStorage.getItem('user_id'),
            "idcardnumber"  : this.idcardnumber, 
            "idcard_verify" : idcard_verify           
          }         
          this.jobber.idcardupdate(dataver).subscribe((resver) => {
            if (idcard_verify == 'yes') {
              this.titlemessage = 'Successfully verified';
              this.showToasterSuccess();
            } else {
              this.titlemessage = 'Not verified';  
              this.showToasterError();            
            }

            this.jobberatorStepTwoDetails();             
      
          }, error => {
            alert(error.Error);
            this.SpinnerService.hide();
          }
          );
              
        }, error => {
          alert(error.Error);
          this.SpinnerService.hide();
        }
        );*/




      }

      if (type == 'licens') {
        let data = {};
        this.jobberator.idVerify(data).subscribe((res) => {
          this.checkoutId = res.data.id;
          this.loadAPI = new Promise((resolve) => {
            this.loadScript();
          }); 
          this.idResultUrl = environment.callback_url+'/paycallbacklicense/'+localStorage.getItem('user_id')+'/'+this.firstname+'/'+this.surname+'/'+this.licensenumber+'/profile';
          jQuery('#idVerifyPopup').modal('show');
        }, Error => {
          this.SpinnerService.hide();
        }
        );
      } 


    // let data = {
    //   "email_address"  : 'info@joblite.co.za',
    //   "password"  : 'Joblite@01'            
    // }    
    // this.jobber.authverify(data).subscribe((res) => {
    //   console.log(res);  
    //   if (type == 'idcard') {
    //     let dataver = {
    //       "api_key"  : res.Result.API_KEY,
    //       "id_number"  : this.idcardnumber            
    //     }         
    //     this.jobber.idverify(dataver).subscribe((resver) => {
    //       alert(resver.Error);                
    
    //     }, error => {
    //       alert(error.Error);
    //       this.SpinnerService.hide();
    //     }
    //     ); 
    //   }   
      
    //   if (type == 'licens') {
    //     let dataver = {
    //       "api_key"  : res.Result.API_KEY,
    //       "license"  : this.licensenumber            
    //     }         
    //     this.jobber.lsverify(dataver).subscribe((resver) => {
    //       alert(resver.Error);                
    
    //     }, error => {
    //       alert(error.Error);
    //       this.SpinnerService.hide();
    //     }
    //     ); 
    //   }       

    // }, error => {
    //   this.SpinnerService.hide();
    // }
    // ); 

    }
  }


  showToasterSuccess(){
    this.notifyService.showSuccess(this.titlemessage, "")
  }
  
  showToasterError(){
      this.notifyService.showError(this.titlemessage, "")
  }
  
  showToasterInfo(){
      this.notifyService.showInfo(this.titlemessage, "")
  }
  
  showToasterWarning(){
      this.notifyService.showWarning(this.titlemessage, "")
  }

  editmode(type: any) {
    if (type == 'phone') {
      this.phone_verify = 'no';
    }
    if (type == 'idcardnumber') {
      this.idcard_verify = 'no';
    }
    if (type == 'licensecardnumber') {
      this.license_verify = 'no';
    }
    if (type == 'facebook') {
      this.facebook_verify = 'no';
    }
    if (type == 'google') {
      this.google_verify = 'no';
    }
    if (type == 'linkedin') {
      this.linkedin_verify = 'no';
    }    
  }

  jobberatorStepTwoDetails() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobberator.jobberatorStepTwoDetails(data).subscribe((res) => {
      this.respbody = res;
      console.log(this.respbody);

      if (res.data.customerInfo.id_img == 'null' || res.data.customerInfo.id_img == null) {
        this.idCard = '';  
      } else {
        this.idCard = environment.server_url+res.data.customerInfo.id_img;
      }


      if (res.data.customerInfo.driving_licence == 'null' || res.data.customerInfo.driving_licence == null) {
        this.dlCard = '';  
      } else {
        this.dlCard = environment.server_url+res.data.customerInfo.driving_licence;
      }
      this.phone = res.data.customerInfo.phone;
      this.facebook = res.data.customerInfo.facebook_url;
      this.google = res.data.customerInfo.google_url;
      this.linkedin = res.data.customerInfo.linkedin_url;
      this.twitter = res.data.customerInfo.twitter_url;
      this.idcardnumber = res.data.customerInfo.idcardnumber;
      this.licensenumber = res.data.customerInfo.licensenumber;  
      this.idcard_verify = res.data.customerInfo.idcard_verify;
      this.license_verify = res.data.customerInfo.license_verify;   
      this.linkedin_verify =  res.data.customerInfo.linkedin_verify;
      this.facebook_verify = res.data.customerInfo.facebook_verify;
      this.google_verify = res.data.customerInfo.google_verify;
      this.phone_verify = res.data.customerInfo.phone_verify; 

    }, error => {
      //this.redirectTo('/');
    }
    );
  }

  jobberatorStepThreeDetails() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobberator.jobberatorStepThreeDetails(data).subscribe((res) => {
      this.respbody = res;
      console.log(this.respbody);

      // if (res.data.customerInfo.id_img == 'null' || res.data.customerInfo.id_img == null) {
      //   this.idCard = '';  
      // } else {
      //   this.idCard = environment.server_url+res.data.customerInfo.id_img;
      // }


      // if (res.data.customerInfo.driving_licence == 'null' || res.data.customerInfo.driving_licence == null) {
      //   this.dlCard = '';  
      // } else {
      //   this.dlCard = environment.server_url+res.data.customerInfo.driving_licence;
      // }
      if (res.data.bankinfo == null) {
        this.bankname = '';
      } else {
        if (res.data.bankinfo.bank_name == null) {
          this.bankname = ''; 
        } else {
          this.bankname = res.data.bankinfo.bank_name; 
        }       
      }

      if (res.data.bankinfo == null) {
        this.branch = '';
      } else {
        this.branch = res.data.bankinfo.bank_branch;    
      }

      if (res.data.bankinfo == null) {
        this.accountno = '';
      } else {
        this.accountno = res.data.bankinfo.account_no;    
      }

      if (res.data.bankinfo == null) {
        this.accounttype = '';
      } else {
        this.accounttype = res.data.bankinfo.account_type;    
      }

    }, error => {
      //this.redirectTo('/');
    }
    );
  }

  genderSelected(selected: any) {
    this.genderselected = selected;
  }

  languageSelected(selected: any) {
    this.languageselected = selected;
  }

  raceSelected(selected: any) {
    this.raceselected = selected;
  }

  proficiencySelected(selected: any) {
    this.proficiencyselected = selected;
  }

  countrySelected(selected: any) {
    this.countryselected = selected;
  }

  provinceSelected(selected: any) {
    this.provinceselected = selected;
  }

  citySelected(selected: any) {
    this.cityselected = selected;
  }

  suburbSelected(selected: any) {
    this.suburbselected = selected;
  }

  tabChange(tab: string) {
    //alert(tab);
    this.activeTab = tab;
    if (tab == 'jbtr2') {
      this.jobberatorStepTwoDetails();
    }
    if (tab == 'jbtr3') {
      this.jobberatorStepThreeDetails();
    }
  }

  nextPrevChange(tab: string) {
    if (tab == 'movetab1') {
      let el: HTMLElement = this.tabDiv1.nativeElement;
      el.click();
    }
    if (tab == 'movetab2') {
      this.movetab1status = true;
      this.step1Save();
      // let el: HTMLElement = this.tabDiv2.nativeElement;
      // el.click();
    }
    if (tab == 'movetab3') {
      this.step2Save();
    }

    if (tab == 'complete') {
      this.redirectTo('/personelinfo');
    }
    
    return false;
  }

  profileChange(type: string) {
    this.redirectTo(type);
  }

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;
  }

  signInWithFB(): void {
    // this.user_type = 'facebook';
    localStorage.setItem('user_type_click', 'facebook');
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signInWithGoogle(): void {
    // this.user_type = 'google';
    localStorage.setItem('user_type_click', 'google');
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  linkedinlogin() {
    // window.location.href = `https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=${
    //   this.linkedInCredentials.clientId
    // }&redirect_uri=${this.linkedInCredentials.redirectUrl}&scope=${this.linkedInCredentials.scope}`;
    localStorage.setItem('user_type_click', 'linkedin');
    localStorage.setItem('linked_verify_back_page', 'profile');
    window.location.href = `https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=${
      this.linkedInCredentials.clientId
    }&redirect_uri=${this.linkedInCredentials.redirectUrl}&scope=${this.linkedInCredentials.scope}`;
  }

}
