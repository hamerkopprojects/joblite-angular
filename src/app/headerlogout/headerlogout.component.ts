import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { environment } from 'src/environments/environment';
import { CategoriesService} from './../_services/categories/categories.service'; 
import { UserService} from './../_services/user/user.service'; 
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
//import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from './../_services/notification.service';
declare var jQuery:any;

@Component({
  selector: 'app-headerlogout',
  templateUrl: './headerlogout.component.html',
  styleUrls: ['./headerlogout.component.css']
})
export class HeaderlogoutComponent implements OnInit {

  //@ViewChild('childModal') public childModal:ModalDirective;
  @ViewChild('myDiv') myDiv: ElementRef<HTMLElement>;
  linkedInCredentials = {
    clientId: "77gg45kc43nbzy",
    // redirectUrl: "https://www.joblite.co.za/linkedinlogin",
    redirectUrl: environment.callback_url +'linkedinlogin',
    // scope: "r_liteprofile%20r_emailaddress%20w_member_social" // To read basic user profile data and email
    scope: "r_liteprofile%20r_emailaddress" // To read basic user profile data and email
  };
  titlemessage: string;
  title = 'jcjoblite1111';
  respbody: any;
  signupStep1: boolean;
  signupStep2: boolean;
  forgotStep1: boolean;
  forgotStep2: boolean;
  loginStep: boolean;
  verificationStep: boolean;
  errorMessage: string;
  email: string;
  dynamicLoginMessage: string;

  userData: any;
  firstName: any;
  lastName: any;
  name: any;
  user_type: any;
  profile_img: any;

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  });
  signup2form = new FormGroup({
    first_name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    last_name: new FormControl(''),
    username: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)]),
    optradio: new FormControl('', [Validators.required]),
    signup_checkform: new FormControl('', [Validators.required])
  });

  signup3form = new FormGroup({
    optradio: new FormControl('', [Validators.required]),
    signup_checkform: new FormControl('', [Validators.required])
  });

  loginform = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  forgotform = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  });

  forgotpassform = new FormGroup({
    otp: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  
  otpform = new FormGroup({
    otp: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  maplocation: any;
  loccoordinates: string;
  
  constructor(private SpinnerService: NgxSpinnerService,
    private category: CategoriesService,
    private user: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: SocialAuthService,
    private notifyService : NotificationService) { }
  ngOnInit() { 
    this.loccoordinates = localStorage.getItem('loccoordinates');
    this.maplocation = localStorage.getItem('maplocation');  
    this.titlemessage = '';
    this.userData = '';
    this.authService.authState.subscribe((user) => {
      this.userData = user;
      this.email = user.email; 
      this.firstName = user.firstName;
      this.lastName = user.lastName;
      let x = Math.floor((Math.random() * 10000) + 1);
      this.name = user.name+''+x;
      this.profile_img = user.photoUrl;
      this.user_type = localStorage.getItem('user_type_click');
      this.signupStep2 = false;

      var data = {
        "email" : this.email,
        "location" : localStorage.getItem('maplocation'),
        "loc_coordinates" : localStorage.getItem('loccoordinates')
      }
        // this.orderdata
        this.user.emailCheck(data).subscribe((res)=>{
          this.errorMessage = '';
          this.signupStep1 = false;
          this.signupStep2 = true;
          this.loginStep = false; 
          this.verificationStep = false;  
          this.SpinnerService.hide();          
        },Error =>{
          this.errorMessage = '';          
          this.social_login_step();
        })      
    });      
    window.onscroll = function() {myFunction()};
    
    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;
    
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }       
    this.signupStep1 = true;
    this.signupStep2 = false;
    this.loginStep = false;
    this.verificationStep = false;
    this.email = '';
    this.errorMessage = '';
    this.dynamicLoginMessage = '';  
  }

  linkedinlogin() {
    // window.location.href = `https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=${
    //   this.linkedInCredentials.clientId
    // }&redirect_uri=${this.linkedInCredentials.redirectUrl}&scope=${this.linkedInCredentials.scope}`;
    window.location.href = `https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=${
      this.linkedInCredentials.clientId
    }&redirect_uri=${this.linkedInCredentials.redirectUrl}&scope=${this.linkedInCredentials.scope}`;
  }

  showToasterSuccess(){
    this.notifyService.showSuccess(this.titlemessage, "")
  }
  
  showToasterError(){
      this.notifyService.showError(this.titlemessage, "")
  }
  
  showToasterInfo(){
      this.notifyService.showInfo(this.titlemessage, "")
  }
  
  showToasterWarning(){
      this.notifyService.showWarning(this.titlemessage, "")
  }

  signup_step1() {

    if(this.form.valid){
      var data = {
        "email" : this.form.value.email,
        "location" : localStorage.getItem('maplocation'),
        "loc_coordinates" : localStorage.getItem('loccoordinates')
      }
        // this.orderdata
        this.user.emailCheck(data).subscribe((res)=>{
          this.email = this.form.value.email;
          if (res.status == 'otp') {
            this.signupStep1 = false;
            this.signupStep2 = false;
            this.loginStep = false;  
            this.verificationStep = true;
            this.errorMessage = ''; 
            this.titlemessage = 'An email with OTP sent to '+this.email;
            this.showToasterSuccess();  
          }

          if (res.status == 'signup2') {
            this.errorMessage = '';          
            this.email = this.form.value.email; 
            this.signupStep1 = false;
            this.signupStep2 = true;
            this.loginStep = false;
            this.verificationStep = false;            
          }

        },Error =>{
          this.errorMessage = Error.error.error.msg;
          this.SpinnerService.hide();
        })
    }
  }

  otp_verify() {

    if(this.otpform.valid){
      var data = {
        "email" : this.email,
        "otp" : this.otpform.value.otp
      }
      // this.orderdata
      this.user.verifyotp(data).subscribe((res)=>{
        this.signupStep1 = false;
        this.signupStep2 = true;
        this.loginStep = false;  
        this.verificationStep = false;
        this.errorMessage = ''; 
        this.titlemessage = 'Your email verified successfully';
        this.showToasterSuccess();  
        // jQuery("#mymodalsignup").click();  
        // jQuery("#mainclose").click();  
        //this.redirectTo('/usersuccess');
      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
    }
  }

  signup_step2() {

    if(this.signup2form.valid){
      var data = {
        "first_name" : this.signup2form.value.first_name,
        "last_name" : this.signup2form.value.last_name,
        "username" : this.signup2form.value.username,
        "email" : this.email,
        "password" : this.signup2form.value.password,
        "user_type" : this.signup2form.value.optradio,
        "location" : localStorage.getItem('maplocation'),
        "loc_coordinates" : localStorage.getItem('loccoordinates')
      }
      // this.orderdata
      this.user.createCustomer(data).subscribe((res)=>{
        /*this.signupStep1 = false;
        this.signupStep2 = false;
        this.loginStep = false;  
        this.verificationStep = true;
        this.errorMessage = ''; */ 
        // jQuery("#mymodalsignup").click();  
        // jQuery("#mainclose").click();  
        // this.redirectTo('/usersuccess');
        this.titlemessage = 'Signup completed successfully';
        this.showToasterSuccess();
        this.signupStep1 = false;
        this.signupStep2 = false;
        this.loginStep = true;  
        this.verificationStep = false;
        this.errorMessage = ''; 
        // this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
    }
  }


  signup_step3() {

    if(this.signup3form.valid){
      var data = {
        "first_name" : this.firstName,
        "last_name" : this.lastName,
        "username" : this.name,
        "email" : this.email,
        "user_type" : this.signup3form.value.optradio,
        "customer_type" : this.user_type,
        "profile_img" : this.profile_img,
        "location" : localStorage.getItem('maplocation'),
        "loc_coordinates" : localStorage.getItem('loccoordinates')
      }
      // this.orderdata
      this.user.createCustomerSocial(data).subscribe((res)=>{
        //this.redirectTo('/usersuccess');
        this.social_login_step();
      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
    }
  }    


  
  social_login_step() {
    let jobDetailsjobId:any;

      var data = {
        "email" : this.email,
        "user_type" : this.user_type
      }
      // this.orderdata
      this.user.sociallogin(data).subscribe((res)=>{
        //console.log(res.data.access_token);
        localStorage.setItem('token', res.data.access_token);
        localStorage.setItem('user_id', res.data.user.id);
        localStorage.setItem('user_type', res.data.user.type);

        jobDetailsjobId = localStorage.getItem('jobDetailsjobId');
        if(jobDetailsjobId != null){
          //this.redirectTo('/jobdetails');
          localStorage.removeItem("jobDetailsjobId");
          this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobDetailsjobId }});
          let el: HTMLElement = this.myDiv.nativeElement;
          el.click();
          return false;          
          //this.redirectTo('/jobdetails?job_id='+jobDetailsjobId);
        }else {
              if (res.data.user.type == 'jobber') {
                //this.redirectTo('/myjobberjobs');
                this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
              } else {
                //this.redirectTo('/myjobs');
                this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
              }  

        }
        

      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
  }

  forgotPasswordStep1(){
    this.forgotStep1 = true;
    this.forgotStep2 = false;
    this.signupStep1 = false;
    this.signupStep2 = false;
    this.loginStep = false;  
    this.verificationStep = false;
    this.errorMessage = '';  
  } 

  forgotPasswordStep2(){
    var data = {
      "email" : this.forgotform.value.email
    }
      // this.orderdata
      this.user.emailCheckDirect(data).subscribe((res)=>{
        this.email = this.forgotform.value.email;
        this.forgotStep1 = false;
        this.forgotStep2 = true;
        this.signupStep1 = false;
        this.signupStep2 = false;
        this.loginStep = false;  
        this.verificationStep = false;
        this.errorMessage = ''; 
        this.titlemessage = 'An email with OTP sent to '+this.email;
        this.showToasterSuccess();

      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
  } 

  change_password(){
    var data = {
      "email" : this.email,
      "otp" : this.forgotpassform.value.otp,
      "password" : this.forgotpassform.value.password
    }
      // this.orderdata
      this.user.changePassword(data).subscribe((res)=>{
        this.email = this.forgotform.value.email;
        this.forgotStep1 = false;
        this.forgotStep2 = false;
        this.signupStep1 = false;
        this.signupStep2 = false;
        this.loginStep = true;  
        this.verificationStep = false;
        this.errorMessage = ''; 
        this.titlemessage = 'Password changed successfully';
        this.showToasterSuccess();

      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })    
  }

  gotoLoginNew(){
    this.forgotStep1 = false;
    this.forgotStep2 = false;
    this.signupStep1 = false;
    this.signupStep2 = false;
    this.loginStep = true;
    this.verificationStep = false;  
    this.errorMessage = '';  
  }

  gotoSignupNew(){
    this.forgotStep1 = false;
    this.forgotStep2 = false;
    this.signupStep1 = true;
    this.signupStep2 = false;
    this.loginStep = false;  
    this.verificationStep = false;
    this.errorMessage = '';  
  } 

  gotoLogin(){
    this.forgotStep1 = false;
    this.forgotStep2 = false;
    this.signupStep1 = false;
    this.signupStep2 = false;
    this.loginStep = true;
    this.verificationStep = false;  
    this.errorMessage = '';  
    jQuery("#mymodalsignup").click();
    jQuery("#mainclose").click();    
  }

  gotoSignup(){
    this.forgotStep1 = false;
    this.forgotStep2 = false;
    this.signupStep1 = true;
    this.signupStep2 = false;
    this.loginStep = false;  
    this.verificationStep = false;
    this.errorMessage = '';  
    jQuery("#mymodalsignup").click();  
    jQuery("#mainclose").click();       
  }   

  login_step() {
    if(this.loginform.valid){
      var data = {
        "username" : this.loginform.value.name,
        "password" : this.loginform.value.password
      }
      // this.orderdata
      this.user.login(data).subscribe((res)=>{
        //console.log(res.data.access_token);
        localStorage.setItem('token', res.data.access_token);
        localStorage.setItem('user_id', res.data.user.id);
        localStorage.setItem('user_email', res.data.user.email);
        localStorage.setItem('user_type', res.data.user.type);
        localStorage.removeItem('loccoordinates');
        this.titlemessage = 'Logged in successfully';
        this.showToasterSuccess();
        if (res.data.user.type == 'jobber') {
          //this.redirectTo('/myjobberjobs');
          this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
        } else {
          //this.redirectTo('/myjobs');
          this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
        }
      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
    }
    return false;
  }

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));

    this.signupStep1 = false;
    this.signupStep2 = false;
    this.loginStep = false;  
    let el: HTMLElement = this.myDiv.nativeElement;
    el.click();
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;    
  }



  signInWithGoogle(): void {
    this.user_type = 'google';
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.user_type = 'facebook';
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authService.signOut();
  }

  notvalid() {
    this.errorMessage = 'Please enter a valid email';
    return false;
  }  

}
