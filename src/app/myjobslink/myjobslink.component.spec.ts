import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyjobslinkComponent } from './myjobslink.component';

describe('MyjobslinkComponent', () => {
  let component: MyjobslinkComponent;
  let fixture: ComponentFixture<MyjobslinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyjobslinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyjobslinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
