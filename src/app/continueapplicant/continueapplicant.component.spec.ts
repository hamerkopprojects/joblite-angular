import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContinueapplicantComponent } from './continueapplicant.component';

describe('ScopeincreaseComponent', () => {
  let component: ContinueapplicantComponent;
  let fixture: ComponentFixture<ContinueapplicantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContinueapplicantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContinueapplicantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
