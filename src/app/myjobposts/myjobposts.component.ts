import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JobpostcallsService} from './../_services/jobpostcalls/jobpostcalls.service'; 


@Component({
  selector: 'app-myjobposts',
  templateUrl: './myjobposts.component.html',
  styleUrls: ['./myjobposts.component.css']
})
export class MyjobpostsComponent implements OnInit {

  jobList: any;  
  constructor( 
    private jobpostcalls: JobpostcallsService,     
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.jobList = [];
    
    this.myJobsLists();
  }

  myJobsLists() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobpostcalls.myJobsLists(data).subscribe((res) => {
      this.jobList = res.data.joblist;
      this.jobList.forEach((value: any, key: any) => {
        if (value.work_type == 'inperson') {
          this.jobList[key].work_type = 'In-person';
        }
        if (value.work_type == 'online') {
          this.jobList[key].work_type = 'Online';
        } 
        if (value.status == 'active') {
          this.jobList[key].stage = 'Active';
        } else if (value.status == 'deactive') {
          this.jobList[key].stage = 'Canceled';
        } else  {
          this.jobList[key].stage = 'Not Completed';
        }                
      });       
    }, error => {
      //this.redirectTo('/');
    }
    );
  } 
  
  statusChange(status :any, key: any, jobid: any) {
    var data = {
      "customers_id" : localStorage.getItem('user_id'),
      "jobpost_id" : jobid,
      "status" : status
    }    
    this.jobpostcalls.statusChange(data).subscribe((res) => {
        this.jobList[key].status = status;
        if (status == 'deactive') {
          this.jobList[key].stage = 'Canceled';
        }
    }, error => {
      //this.redirectTo('/');
    }
    );
    return false;
  }

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;
  }  
}
