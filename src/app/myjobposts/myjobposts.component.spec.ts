import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyjobpostsComponent } from './myjobposts.component';

describe('MyjobpostsComponent', () => {
  let component: MyjobpostsComponent;
  let fixture: ComponentFixture<MyjobpostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyjobpostsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyjobpostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
