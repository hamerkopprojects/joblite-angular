import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyjobberjobsComponent } from './myjobberjobs.component';

describe('MyjobberjobsComponent', () => {
  let component: MyjobberjobsComponent;
  let fixture: ComponentFixture<MyjobberjobsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyjobberjobsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyjobberjobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
