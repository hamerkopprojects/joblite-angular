import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JobpostcallsService} from './../_services/jobpostcalls/jobpostcalls.service'; 
import { ApiService } from '../_services/commonlisting/api.service';
import { JobberatorprofileService } from './../_services/jobberatorprofile/jobberatorprofile.service';
import { environment } from 'src/environments/environment';


declare var jQuery: any;


@Component({
  selector: 'app-myjobberjobs',
  templateUrl: './myjobberjobs.component.html',
  styleUrls: ['./myjobberjobs.component.css']
})
export class MyjobberjobsComponent implements OnInit {
  @ViewChild('t2') t2: ElementRef<HTMLElement>; 
  @ViewChild('t3') t3: ElementRef<HTMLElement>; 
  server_url: string;
  popUpJobDetails: any;
  popUpi: any;
  jobberMoneyList: any; 
  jobberMoneyTempList: any;  
  totalout: any;
  searchpay: any;
  jobList: any; 
  jobsPendingLists: any;
  jobsActiveLists: any;
  currentDate: any; 
  userType: any;  
  jobCatagoryList: any;
  jobCompletedList: any;
  searchpaymentid: any;
  ratingJobId: any; 
  currentJob: any;

  
  jobberImg: any;
  jobberatorImg: any;  
  jobPendingId: any;
  customersId: any;
  status: any;
  count: any;
  noticount: any;
  notificationsList: any;
  ratingDoneCount: any;
  ratingBlankCount: any;
  ratingBlankCountJobber: any;
  ratingcomment: any;
  ratingcommentjobber: any;
  ratingEmpty: boolean;
  commentEmpty: boolean;
  popupJobName: any;
  tab: any;  
  constructor( 
    private jobpostcalls: JobpostcallsService,     
    private router: Router,
    private apiService: ApiService,    
    private route: ActivatedRoute,
    private jobberator: JobberatorprofileService) { }

  ngOnInit(): void {
    this.notificationsList = [];
    this.server_url = environment.server_url;
    this.jobCompletedList = [];
    this.jobsActiveLists = [];
    this.jobCatagoryList = [];
    this.totalout = 0;
    this.searchpay = '';        
    this.ratingDoneCount = 0;
    this.ratingBlankCount = 5;
    this.ratingBlankCountJobber = 5;    
    this.ratingJobId = ''; 
    this.currentJob = '';
    this.jobberImg = '';
    this.jobberatorImg = '';    
    this.ratingcomment = '';
    this.ratingcommentjobber = '';    
    this.ratingEmpty = false;
    this.commentEmpty = false;
    this.jobList = [];
    this.jobsPendingLists = [];
    this.userType = localStorage.getItem('user_type');
    this.popupJobName = '';   
    //this.tab = ""; 
    this.myJobsLists();
    this.myJobsPendingLists();
    this.myJobsActiveLists();
    //this.favoriteJobs();
    this.myJobsCompletedList();
    this.customerDetails();
    this.myMoneyList();  
    this.tab = this.route.snapshot.params.tab;
    if (this.tab == 't2' ) {
      setTimeout(() => {
        let el: HTMLElement = this.t2.nativeElement;
        el.click();
      }, 2500) 
    }

    if (this.tab == 't3' ) {
      setTimeout(() => {
        let el: HTMLElement = this.t3.nativeElement;
        el.click();
      }, 2500) 
    }  
  }

  createRange(number: any){
    if (isNaN(number)) {
      number = 0;
    } 
    return new Array(number);
  } 

  myJobsLists() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobpostcalls.myJobsLists(data).subscribe((res) => {
      this.jobList = res.data.joblist;
      this.jobList.forEach((value: any, key: any) => {
        if (value.work_type == 'inperson') {
          this.jobList[key].work_type = 'In-person';
        }
        if (value.work_type == 'online') {
          this.jobList[key].work_type = 'Online';
        } 
        var date = new Date();
        this.currentDate = date.toISOString().slice(0, 19).replace('T', ' ');        
        if (value.status == 'active') {
          this.jobList[key].stage = 'Active';
        } else if (value.status == 'deactive') {
          this.jobList[key].stage = 'Canceled';
        } else  {
          this.jobList[key].stage = 'Not Completed';
        }                
      });       
    }, error => {
      //this.redirectTo('/');
    }
    );
  } 

  myJobsPendingLists() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobpostcalls.myJobsJobberPendingLists(data).subscribe((res) => {
      this.jobsPendingLists = res.data.joblist;
      this.jobsPendingLists.forEach((value: any, key: any) => {
        if (value.work_type == 'inperson') {
          this.jobsPendingLists[key].work_type = 'In-person';
        }
        if (value.work_type == 'online') {
          this.jobsPendingLists[key].work_type = 'Online';
        } 
        var date = new Date();
        this.currentDate = date.toISOString().slice(0, 19).replace('T', ' ');        
        if (value.status == 'active') {
          this.jobsPendingLists[key].stage = 'Active';
        } else if (value.status == 'deactive') {
          this.jobsPendingLists[key].stage = 'Canceled';
        } else  {
          this.jobsPendingLists[key].stage = 'Not Completed';
        }
        this.jobsPendingLists[key].cat_image_path = environment.server_url+'uploads/job_category/' +value['cat_image_path'];                
      });       
    }, error => {
      //this.redirectTo('/');
    }
    );
  } 
  
  myJobsActiveLists() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobpostcalls.myJobsJobberActiveLists(data).subscribe((res) => {
      this.jobsActiveLists = res.data.joblist;
      this.jobsActiveLists.forEach((value: any, key: any) => {
        if (value.work_type == 'inperson') {
          this.jobsActiveLists[key].work_type = 'In-person';
        }
        if (value.work_type == 'online') {
          this.jobsActiveLists[key].work_type = 'Online';
        } 
        var date = new Date();
        this.currentDate = date.toISOString().slice(0, 19).replace('T', ' ');        
        if (value.status == 'active') {
          this.jobsActiveLists[key].stage = 'Active';
        } else if (value.status == 'deactive') {
          this.jobsActiveLists[key].stage = 'Canceled';
        } else  {
          this.jobsActiveLists[key].stage = 'Not Completed';
        }  
        this.jobsActiveLists[key].cat_image_path = environment.server_url+'uploads/job_category/' +value['cat_image_path'];              
      });       
    }, error => {
      //this.redirectTo('/');
    }
    );
  }  
  
  statusChange(status :any, key: any, jobid: any) {
    var data = {
      "customers_id" : localStorage.getItem('user_id'),
      "jobpost_id" : jobid,
      "status" : status
    }    
    this.jobpostcalls.statusChange(data).subscribe((res) => {
        this.jobList[key].status = status;
        if (status == 'deactive') {
          this.jobList[key].stage = 'Canceled';
        }
    }, error => {
      //this.redirectTo('/');
    }
    );
    return false;
  }

  jobberatorJobApplicantsStatusChange(jobintIdReceived:any,userIdReceived:any,statusReceived:any){
    //let status = 'jobberator_accepted';
   // var data = {
   //   "job_intimation_id": this.jobintId,
   //   "customers_id" : this.userId,
   //   "status" : status
    
     
   // }
   this.apiService.jobberJobApplicantsStatusChange(jobintIdReceived, userIdReceived, statusReceived).subscribe((response: any) => {
    this.myJobsLists();
    this.myJobsPendingLists();
    this.myJobsActiveLists();
    console.log(statusReceived);
    if (response.status == 'failed' ) {
      jQuery('#confirmationPopupAlreadyStarted').modal('show');
    }
    if(statusReceived == 'jobber_accepted') {
      setTimeout(() => {
        let el: HTMLElement = this.t2.nativeElement;
        el.click();
      }, 1000) 
    }
   },

     error => {
       //this.redirectTo('/');
     }
   );

  }  

  closeJob(jobintIdReceived:any,userIdReceived:any,statusReceived:any) {

    this.jobberatorJobApplicantsStatusChange(jobintIdReceived,userIdReceived,statusReceived);   
     
    }
  
  acceptJob(jobintIdReceived:any,userIdReceived:any,statusReceived:any, count: any) {

    this.jobberatorJobApplicantsStatusChange(jobintIdReceived,userIdReceived,statusReceived);   
    this.jobList[count].customers.status = 'jobber_accepted';
  } 
  
  taskTo(uri:string, status:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    localStorage.setItem('jobber_task_user_status',status);
    this.router.navigate([uri]);
    return false;
  }

  redirectTo(uri:string){
    localStorage.removeItem('jobber_task_user_status');
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;
  } 

  favoriteJobs() {
    this.apiService.favoriteJobs(localStorage.user_id)
    .subscribe(
      (data: any) => {
        data.data.resp.forEach((value: any, key: any) => {
          if (data.data.resp[key].job_intimation[0]?.status == 'donot_show') {
            data.data.resp.splice(key,1);
          }
        });            
        this.jobCatagoryList = data.data.resp;
        //console.log("jobcatlist",this.jobCatagoryList)
      
      },
      error => {
        console.log(error);
      }
    );

  }

  myJobsCompletedList(){

    this.apiService.myJobberJobsCompletedList(localStorage.user_id)
    .subscribe(
      (response) => {
                  
        this.jobCompletedList = response.data.joblist;
        this.tab = this.route.snapshot.params.tab;
        if (this.tab == 't3' ) {
          let el: HTMLElement = this.t3.nativeElement;
          el.click(); 
        }
       
        //console.log("jobcompletedlist",this.jobCompletedList)
        this.jobCompletedList.forEach((value: any, key: any) => {
          this.jobCompletedList[key].cat_image_path = environment.server_url+'uploads/job_category/' +value['cat_image_path'];              
        });      
      },
      error => {
        console.log(error);
      }
    );

  }

  ratingChange(count: any) {
    this.ratingDoneCount = count;
    this.ratingBlankCount = 5 - count; 
  }

  viewtip(jobId:any, jobtitle: any, currentjob: any) {
    this.popupJobName = jobtitle;
    this.ratingJobId = jobId;    
    this.currentJob = '';
    this.currentJob = currentjob;   
  }


  cancelrating() {
    this.ratingDoneCount = 0;
    this.ratingBlankCount = 5;  
    this.ratingcomment = ''; 
    this.ratingEmpty = false;
    this.commentEmpty = false;      
    jQuery('#exampleModalRating').modal('hide');   
  }

  rating(jobId:any, ratingjobber: any, jobtitle: any , currentjob: any) {
    this.popupJobName = jobtitle;
    this.ratingJobId = jobId;
    this.currentJob = '';
    this.currentJob = currentjob;
    this.jobberImg = environment.server_url+'' +this.currentJob.customers.customer.profile_img;
    this.jobberatorImg = environment.server_url+'' +this.currentJob.jobberator.profile_img;    
    this.ratingcommentjobber = ratingjobber;
    this.ratingBlankCountJobber = 5 - ratingjobber.rating;    
    
    let data = {
      "customer_id" : localStorage.getItem('user_id'),
      "job_id" : this.ratingJobId,
      "usertype" : 'jobber'
    }

    this.jobpostcalls.getRating(data).subscribe((res) => {
      if (res.data != null) {
        this.ratingDoneCount = res.data.rating;
        this.ratingBlankCount = 5 - this.ratingDoneCount;
        this.ratingcomment = res.data.comment;
      } else {
        this.ratingDoneCount = 0;
        this.ratingBlankCount = 5;  
        this.ratingcomment = '';       
      }
      // this.ratingDoneCount = 0;
      // this.ratingBlankCount = 5;  
      // this.ratingcomment = ''; 
      // this.ratingEmpty = false;
      // this.commentEmpty = false;          

    }, Error => {
    }
    );    
  }

  submitrating() {
    let flag: boolean;
    flag = true;
    this.ratingEmpty = false;
    this.commentEmpty = false;    
    if (this.ratingDoneCount == 0) {
      flag = false;
      this.ratingEmpty = true;
    }

    if (this.ratingcomment.trim() == '') {
      flag = false;
      this.commentEmpty = true;
    } 
    
    if (flag) {
      let data = {
        "customer_id" : localStorage.getItem('user_id'),
        "job_id" : this.ratingJobId,
        "rating" :  this.ratingDoneCount,
        "comment" : this.ratingcomment,
        "usertype" : 'jobber'
      }

      this.jobpostcalls.ratingSave(data).subscribe((res) => {
        this.ratingDoneCount = 0;
        this.ratingBlankCount = 5;  
        this.ratingcomment = ''; 
        this.ratingEmpty = false;
        this.commentEmpty = false;          
        jQuery('#exampleModalRating').modal('hide');
        this.myJobsCompletedList();
      }, Error => {
      }
      );
    }

  }

  cancelrequest() {
    jQuery('#exampleModalRequest').modal('hide');   
  }  


  requestrating() {

      let data = {
        "customer_id" : localStorage.getItem('user_id'),
        "job_id" : this.ratingJobId,
        "usertype" : 'jobber'
      }

      this.jobpostcalls.requestRating(data).subscribe((res) => {
        jQuery('#exampleModalRequest').modal('hide');
        this.myJobsCompletedList();
      }, Error => {
      }
      );

  }  

  confirmationPopup(jobId:any,customersId:any,status:any,count:any) {
    localStorage.removeItem('jobber_task_user_status');

    this.jobPendingId = jobId;
    //this.customersId = customersId;
    this.customersId = localStorage.getItem('user_id');
    this.status = status;
    this.count = count;

    if(this.status == 'jobber_rejected'){

      jQuery('#confirmationText').html("Are you not interested in this job?");

    }else if(this.status == 'started')
    {
      jQuery('#confirmationText').html("Do you want to start this job?");

    }else if(this.status == 'jobber_accepted'){
      jQuery('#confirmationText').html("Are you interested to do this job?");
    }


    jQuery('#confirmationPopup').modal('show')
  }

  jobDetails(value:any) {
    //this.redirectTo('/jobdetails?job_id='+value);
    this.router.navigate(['/jobdetails'],{ queryParams: { job_id: value }});
  }

  confirmationListener(value:any){
    jQuery('#confirmationPopup').modal('hide')
    switch(this.status)
    {
       case 'jobber_rejected': 
       this.closeJob(this.jobPendingId,this.customersId,this.status)
       break;
       case 'started':
         this.redirectTo('/tasklist/'+this.jobPendingId)
         break;
         case 'jobber_accepted' :
         this.acceptJob(this.jobPendingId,this.customersId,this.status,this.count);
         break;

    }
  }

  jobHistory(job_id: any) {
    var param = {
      "jobpost_id" : job_id,
      "jobber_id" : localStorage.getItem('user_id')      
    }      
    this.apiService.jobHistory(param)
    .subscribe(
      (data: any) => {
        this.noticount = data.data.count;
        this.notificationsList = data.data.notification_info;
        this.notificationsList.forEach((value: any, key: any) => {

          if (value['status'] == 'jobberator_accepted') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " accepted your job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }
          if (value['status'] == 'jobberator_rejected') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = " rejected your job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          } 
          if (value['status'] == 'jobberator_approved') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = " approved your job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }
          if (value['status'] == 'jobberator_disapproved') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = " disapproved your job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          } 
          if (value['status'] == 'jobberator_disputed') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = " disputed your job done";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }
          
          if (value['status'] == 'started') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = " started job";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }
          
          
          if (value['status'] == 'jobber_accepted') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];               
            this.notificationsList[key]['comment'] = " accepted "+value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']+"'s job acceptance";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }
          if (value['status'] == 'jobber_rejected') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = " rejected "+value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']+"'s job acceptance";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }  
          if (value['status'] == 'jobber_disputed') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = " disputed your job done";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }
                   
          
        });  
        console.log("notificationlist",this.notificationsList);         
      },
      error => {
        console.log(error);
      }
    );    
  }

  customerDetails() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobberator.jobberatorStepOneDetails(data).subscribe((res) => {
      this.userType = res.data.customerInfo.type;

    }, error => {
      //this.redirectTo('/');
    }
    );
  }  
  
  //Go to jog details page
  doGotoJobDetailsPage(jobid: any){
    this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobid }});
    return false;
  } 

  //Go to jog details page
  doGotoJobDetailsPagePopup(job: any,i: any){
    this.popUpJobDetails = job;
    this.popUpi = i;
  }
  
  myMoneyList(){
    let customerId = localStorage.getItem('user_id')
    console.log("cusid",customerId)
    var data = {
      "customers_id" : customerId,
      "search" : '',
      "pay_type" : 'tip'
    }
    this.apiService.myMoneyJobberList(data)
    .subscribe((response) => {
        let totalout = 0;          
        this.jobberMoneyList = response.data.joblist;
        this.jobberMoneyTempList = response.data.joblist;
        this.jobberMoneyList.forEach((value: any, key: any) => {
          totalout = totalout + parseFloat(value['amount']);
          totalout.toFixed(2);
          if (this.jobberMoneyList[key]['code'] ==  '000.100.110') {
            this.jobberMoneyList[key]['paystatus'] = 'success';
          } else {
            this.jobberMoneyList[key]['paystatus'] = 'failed';
          }
          let d = new Date(value['created_at']);
          this.jobberMoneyList[key]['paydate'] = d.toLocaleString('default', { month: 'long' })+' '+d.getDate()+', '+d.getFullYear();          
          if (value['amount'] == 0) {
            delete this.jobberMoneyList[key];
          }
		    });
        this.totalout = totalout;          
        //console.log("jobcompletedlist",this.jobCompletedList)
      
      },
      error => {
        console.log(error);
      }
    );

  } 
  
  onKeyUp(x: any) { 
    let totalout = 0;  
    this.jobberMoneyList = []; 
    // this.jobberMoneyList = this.jobberMoneyTempList;
    this.jobberMoneyTempList.forEach((value: any, key: any) => {
      if (value['job']['job_title'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['type'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['amount'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['payment_id'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['paystatus'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['paydate'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['job_id'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1) {
        totalout = totalout + parseFloat(value['amount']); 
        totalout.toFixed(2);
        this.jobberMoneyList[key] = value;
      }
    });
    this.totalout = totalout;    
  }   

}
