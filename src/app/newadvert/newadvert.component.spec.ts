import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewadvertComponent } from './newadvert.component';

describe('ScopeincreaseComponent', () => {
  let component: NewadvertComponent;
  let fixture: ComponentFixture<NewadvertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewadvertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewadvertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
