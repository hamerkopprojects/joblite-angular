import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaycallbacklicenseComponent } from './paycallbacklicense.component';

describe('PaycallbacklicenseComponent', () => {
  let component: PaycallbacklicenseComponent;
  let fixture: ComponentFixture<PaycallbacklicenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaycallbacklicenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaycallbacklicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
