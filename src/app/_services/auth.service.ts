import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpclientService } from './wrapper/httpclient.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  jwtHelper = new JwtHelperService();
  // constructor(private http: HttpClient) {}
  /**
     * Constructor for the home service class
     * @param http http
     */
  constructor(private http: HttpclientService) {
      this.http = http; // Setting http as overrided http service named as HttpclientService
  }

  // tslint:disable-next-line:typedef
  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }
}
