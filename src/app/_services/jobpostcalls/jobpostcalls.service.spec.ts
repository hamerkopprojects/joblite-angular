import { TestBed } from '@angular/core/testing';

import { JobpostcallsService } from './jobpostcalls.service';

describe('JobpostcallsService', () => {
  let service: JobpostcallsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JobpostcallsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
