import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpclientService } from './../wrapper/httpclient.service';

@Injectable({
  providedIn: 'root'
})
export class JobpostcallsService {

  /**
     * Constructor for the home service class
     * @param http http
     */
    constructor(private http: HttpclientService) {
      this.http = http; // Setting http as overrided http service named as HttpclientService
  }

  /**
  * To upload images
  */
 upload(formData: any,id: any){
  var url: any;
  if (id == '') {
    url = 'auth/jobpost/addphoto';
  } else {
    url = 'auth/jobpost/addphoto/'+id;
  }
  return this.http.postform(url,formData).pipe(
    map((response: any) => {
      console.log("response",response);
      return response;

    })
  );
  console.log("")

  }

  /**
  * To upload images and update
  */
 uploadupdate(formData: any,id: any){
  var url: any;
  url = 'auth/jobpost/editphoto/'+id;

  return this.http.postform(url,formData).pipe(
    map((response: any) => {
      console.log("response",response);
      return response;

    })
  );
  console.log("")

  }  
  
  /**
   * To get all jobpostphotoslist of jobs
   */
  jobpostphotoslist(data: any ) {
    return this.http.post('jobpostphotoslist',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

  /**
   * To save all job details
   */  
  jobDetailsSave(data: any ) {
    return this.http.post('auth/jobpost/addstepone',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

  /**
   * To get rating
   */  
   getRating(data: any ) {
    return this.http.post('getrating',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  

  /**
   * To request rating
   */  
   requestRating(data: any ) {
    return this.http.post('requestrating',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }   

  /**
   * To save rating
   */  
   ratingSave(data: any ) {
    return this.http.post('saverating',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  

  /**
   * To save all task details
   */  
  taskDetailsSave(data: any ) {
    return this.http.post('auth/jobpost/addsteptwo',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

  /**
   * To get matching criteria details
   */ 

  matchingCriteriaDetails(data: any ) {
    return this.http.post('jobpoststepthreedetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

  /**
   * To get my jobs lists
   */   
  myJobsLists(data: any ) {
    return this.http.post('myjobpostlist',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

  /**
   * To get my jobs lists
   */   
  myDraftsLists(data: any ) {
    return this.http.post('mydraftslist',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * My all jobs list
   */   
   myAllJobsLists(data: any ) {
    return this.http.post('myalljobpostlist',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }   

  /**
   * To get my jobs pending lists
   */   
   myJobsPendingLists(data: any ) {
    return this.http.post('jobberatorpendinglist',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 
  
  /**
   * To get my jobs active lists
   */   
   myJobsActiveLists(data: any ) {
    return this.http.post('jobberatoractivelist',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }    

  /**
   * To get my jobs pending lists of jobber
   */   
   myJobsJobberPendingLists(data: any ) {
    return this.http.post('jobberpendinglist',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }
  
  /**
   * To get my jobs active lists of jobber
   */   
   myJobsJobberActiveLists(data: any ) {
    return this.http.post('jobberactivelist',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  

  /**
   * To update status of
   */   
  statusChange(data: any ) {
    return this.http.post('jobstatuschange',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }   

  
  /**
   * To save all matching criteria
   */  
  matchingCriteriaSave(data: any ) {
    return this.http.post('auth/jobpost/addstepthree',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }
  
  /**
   * To save job task
   */  
   jobTaskSaveWithoutJobId(data: any ) {
    return this.http.post('auth/jobpost/addtaskwithoutjobid',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }


  /**
   * To save job type
   */  
   addJobType(data: any ) {
    return this.http.post('auth/autosuggest/addjobtypecust',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  

  /**
   * To save job type
   */  
   addJobTypeProfile(data: any ) {
    return this.http.post('auth/autosuggest/addjobtypebyprofile',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }   

  /**
   * To remove job type
   */  
    removeJobTypeProfile(data: any ) {
      return this.http.post('auth/autosuggest/removejobtypebyprofile',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    }
  /**
   * To save job task
   */  
  jobTaskSave(data: any ) {
    return this.http.post('auth/jobpost/addtask',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

  /**
   * To update job task
   */  
    jobTaskUpdateTemp(data: any ) {
      return this.http.post('auth/jobpost/edittasktemp',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    }
  
  /**
   * To update job task
   */  
  jobTaskUpdate(data: any ) {
    return this.http.post('auth/jobpost/edittask',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  


    /**
   * To remove job task
   */  
    jobTaskRemoveTemp(data: any ) {
      return this.http.post('auth/jobpost/deletetasktemp',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    } 
  
  /**
   * To remove job task
   */  
  jobTaskRemove(data: any ) {
    return this.http.post('auth/jobpost/deletetask',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }   

  /**
   * To update all job details
   */  
  jobDetailsUpdate(data: any ) {
    return this.http.post('auth/jobpost/editstepone',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

  /**
   * To check the payment
   */  
   paymentCheck(data: any ) {
    return this.http.post('paymentcheck',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  

  /**
   * To save plan after successful payment
   */  
   planSaveWithoutPayment(data: any ) {
    return this.http.post('plansavewithoutpayment',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

    /**
   * To save plan after successful payment
   */  
    withApplicantsPlanSaveWithoutPayment(data: any ) {
      return this.http.post('withapplicantsplansavewithoutpayment',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    } 
  
  /**
   * To save plan after successful payment
   */  
    newaddPlanSaveWithoutPayment(data: any ) {
      return this.http.post('newaddplansavewithoutpayment',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    } 

    /**
   * To save plan with 0 payment
   */  
    planZeroSave(data: any ) {
      return this.http.post('planzerosave',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    }

  /**
   * To save plan after successful payment
   */  
   planSave(data: any ) {
    return this.http.post('plansave',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }   
  
  /**
   * To save advanced matching save
   */  
  advancedMatchingSave(data: any ) {
    return this.http.post('auth/jobpost/addstepfour',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 
 
  /**
   * To save adverticement plan
   */   
  adverticementPlanSave(data: any ) {
    return this.http.post('auth/jobpost/addstepfive',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

  /**
   * To create checkout init
   */   
   startCheckoutInit(data: any ) {
    return this.http.post('startcheckoutinit',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }   

  /**
   * To get job details
   */  
  getJobDetails(data: any ) {
    return this.http.post('jobpoststeponedetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

  /**
   * To get task details
   */  
  getTaskDetails(data: any ) {
    return this.http.post('jobpoststeptwodetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }   
  
  /**
   * To delete post job image
   */
  deleteImage(data: any ) {
    return this.http.post('auth/jobpost/deletephoto',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

  /**
   * To get all education related lists
   */
  educationList() {
    return this.http.get('educationskilllist').pipe(
      map((response: any) => {
        return response;
      })
    );
  }  

  /**
   * To get all plan lists
   */
  planList() {
    return this.http.get('plandetailslist').pipe(
      map((response: any) => {
        return response;
      })
    );
  }    

  /**
   * To advanced matching details
   */  
  getAdvancedMatchingDetails(data: any ) {
    return this.http.post('jobpoststepfourdetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  

  /**
   * To advanced matching details
   */  
  getPaymentDetails(data: any ) {
    return this.http.post('jobpoststepfivedetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }   
  
}
