import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpclientService } from './../wrapper/httpclient.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  // constructor(private http: HttpClient) {}
  /**
     * Constructor for the home service class
     * @param http http
     */
  constructor(private http: HttpclientService) {
      this.http = http; // Setting http as overrided http service named as HttpclientService
  }

  // tslint:disable-next-line:typedef
  /**
   * To get all categories list to build a tree
   */
  getCategories() {
    return this.http.get('categorylist').pipe(
      map((response: any) => {
        return response;
      })
    );
  }

   /**
   * To get all other settings list
   */
    getOtherSettings() {
      return this.http.get('othersettings').pipe(
        map((response: any) => {
          return response;
        })
      );
    }

  /**
   * To get all home page categories list to build a tree
   */
     getHomeCategories() {
      return this.http.get('categoryhomelist').pipe(
        map((response: any) => {
          return response;
        })
      );
    }

  /**
   * To get an individual category data
   */
  getCategory(nodeId: number) {
    return this.http.get('Category?cateId='+nodeId).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To get all jobtypes based on category
   */
  getJobtype(data: any) {
    return this.http.post('jobtypesbycat',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To get all jobtypes based on category
   */
   getJobtypeWithCategoryDetails(data: any) {
    return this.http.post('jobtypeswithcat',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  
}
