import { TestBed } from '@angular/core/testing';

import { JobberprofileService } from './jobberprofile.service';

describe('JobberprofileService', () => {
  let service: JobberprofileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JobberprofileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
