import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpclientService } from './../wrapper/httpclient.service';
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class JobberprofileService {

  /**
     * Constructor for the home service class
     * @param http http
     */
    constructor(private http: HttpclientService) {
      this.http = http; // Setting http as overrided http service named as HttpclientService
  }

  /**
   * To get all jobberatorStepOneDetails
   */
  jobberatorStepOneDetails(data: any) {
    return this.http.post('jobberatorsteponedetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To get all jobberatorStepTwoDetails
   */
  jobberatorStepTwoDetails(data: any) {
    return this.http.post('jobberatorsteptwodetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To get all jobberatorStepThreeDetails
   */
  jobberatorStepThreeDetails(data: any) {
    return this.http.post('jobberstepsixdetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To get all stepsSaved
   */
   stepsSaved(data: any) {
      return this.http.post('getsteps',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    }

  /**
   * To get all jobberStepThreeDetails
   */
  jobberStepThreeDetails(data: any) {
    return this.http.post('jobberstepthreedetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  
  /**
   * To get all jobberStepFourDetails
   */
  jobberStepFourDetails(data: any) {
    return this.http.post('jobbersteptwodetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To get all skillCategoryDetails
   */
    skillCategoryDetails(data: any) {
    return this.http.post('jobbersteptwodetailscategory',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To get all skillCategoryJobTypeDetails
   */
   skillCategoryJobTypeDetails(data: any) {
    return this.http.post('jobbersteptwodetailsjobtype',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  

  /**
   * To get all skillCategoryJobTypeProficiencyDetails
   */
   skillCategoryJobTypeProficiencyDetails(data: any) {
    return this.http.post('jobbersteptwodetailsprof',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  

  /**
   * To get all language,prificiency and race list
   */
  langProficiencyRaceList() {
    return this.http.get('langproficiencylist').pipe(
      map((response: any) => {
        return response;
      })
    );
  }


  /**
   * To get all skillCategoryJobTypeDetails
   */
   educationQualiList(data: any) {
    return this.http.post('educationqualilist',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  
  
    /**
   * To get all lang prof list
   */
    langProfList(data: any) {
      return this.http.post('langproflist',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    }

  /**
   * To get all education related lists
   */
  educationList() {
    return this.http.get('educationlist').pipe(
      map((response: any) => {
        return response;
      })
    );
  }

   /**
  * To upload images
  */
 upload(formData: any,id: any){
  var url: any;
  url = 'auth/jobberator/profileimageupdate/'+id;
  return this.http.postform(url,formData).pipe(
    map((response: any) => {
      console.log("response",response);
      return response;

    })
  );
  console.log("")

  }

  /**
   * To save step1 details
   */
  step1Save(data: any) {
    return this.http.post('auth/jobberator/addstepone',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To save step2 details
   */
  step2Save(data: any) {
    return this.http.post('auth/jobberator/addsteptwo',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To save step2 details
   */
  step3Save(data: any) {
    return this.http.post('auth/jobber/addstepsix',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To save step3 jobber details
   */
  step3jobberSave(data: any) {
    return this.http.post('auth/jobber/addstepthree',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To save step4 details
   */
  step4Save(data: any) {
    return this.http.post('auth/jobber/addsteptwo',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To save step5 details
   */
  step5Save(data: any) {
    return this.http.post('auth/jobber/addstepfive',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To get all educationTabDetails
   */
  educationTabDetails(data: any) {
    return this.http.post('jobberstepfourdetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To remove education qualification
   */  
   educationJobberRemove(data: any ) {
    return this.http.post('removequalification',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 


  /**
   * To remove language and preference
   */  
     langJobberRemove(data: any ) {
      return this.http.post('removelanguage',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    }
  
  /**
   * To update education qualification
   */  
   educationJobberUpdate(data: any ) {
    return this.http.post('updatequalification',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  
  
  /**
   * To update language and proficiency
   */  
  langJobberUpdate(data: any ) {
      return this.http.post('updatelang',data).pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  /**
   * To save language and preference
   */
    langPrefAdd(data: any) {
      return this.http.post('addlangpref',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    }

  /**
   * To save educationJobberSave
   */
   educationJobberAdd(data: any) {
    return this.http.post('addqualification',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  

  /**
   * To save educationJobberSave
   */
  educationJobberSave(data: any) {
    return this.http.post('auth/jobber/addstepfour',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

 /**
  * To upload id card
  */
 idcardupload(formData: any,id: any){
  var url: any;
  url = 'auth/jobberator/idcardupdate/'+id;
  return this.http.postform(url,formData).pipe(
    map((response: any) => {
      console.log("response",response);
      return response;

    })
  );
  console.log("")

  }


 /**
  * To upload driving licens
  */
 dlcardupload(formData: any,id: any){
  var url: any;
  url = 'auth/jobberator/dlcardupdate/'+id;
  return this.http.postform(url,formData).pipe(
    map((response: any) => {
      console.log("response",response);
      return response;

    })
  );
  console.log("")

  }


 /**
  * To upload portfolio
  */
 portfolioupload(formData: any,id: any){
  var url: any;
  url = 'jobberportfolio/'+id;
  return this.http.postform(url,formData).pipe(
    map((response: any) => {
      console.log("response",response);
      return response;

    })
  );
  console.log("")

  }

  /**
   * Auth verify
   */
   authverify(data: any) {
    // return this.http.postverify('https://www.verifyid.co.za/webservice/authenticate',data).pipe(
    //   map((response: any) => {
    //     return response;
    //   })
    // );
    return this.http.postverify(environment.callback_url+'authenticate',data).pipe(
      map((response: any) => {
        return response;
      })
    );    
  } 

  /**
   * Auth verify
   */
      licenseverifylatest(data: any) {
        // return this.http.postverify('https://www.verifyid.co.za/webservice/authenticate',data).pipe(
        //   map((response: any) => {
        //     return response;
        //   })
        // );
        return this.http.postverifylatest(environment.callback_url+'verifyLicenseTest',data).pipe(
          map((response: any) => {
            return response;
          })
        );    
      }

  /**
   * Auth verify
   */
     idverifylatest(data: any) {
      // return this.http.postverify('https://www.verifyid.co.za/webservice/authenticate',data).pipe(
      //   map((response: any) => {
      //     return response;
      //   })
      // );
      // return this.http.postverifylatest(environment.callback_url+'verifyTest',data).pipe(
      //   map((response: any) => {
      //     return response;
      //   })
      // );  
      // BACKEND CALL
      return this.http.post('verifytest',data).pipe(
        map((response: any) => {
          return response;
        })
      );  
    }
  
  /**
   * Auth verify
   */
   idverify(data: any) {
    // return this.http.postverify('https://www.verifyid.co.za/webservice/authenticate',data).pipe(
    //   map((response: any) => {
    //     return response;
    //   })
    // );
    return this.http.postverify(environment.callback_url+'said_verification',data).pipe(
      map((response: any) => {
        return response;
      })
    );    
  } 
  
  /**
   * Auth verify
   */
   lsverify(data: any) {
    // return this.http.postverify('https://www.verifyid.co.za/webservice/authenticate',data).pipe(
    //   map((response: any) => {
    //     return response;
    //   })
    // );
    return this.http.postverify(environment.callback_url+'sadl_decode',data).pipe(
      map((response: any) => {
        return response;
      })
    );    
  }
  
  /**
   * linkedin verify
   */
   lnverify(data: any) {
    return this.http.post('linkedinvalidation',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 
  
  /**
   * facebook verify
   */
   facebookverify(data: any) {
    return this.http.post('facebookvalidation',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  } 

  /**
   * google verify
   */
    googleverify(data: any) {
      return this.http.post('googlevalidation',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    } 

  /**
   * idcard update
   */
   idcardupdate(data: any) {
    return this.http.post('idcardupdate',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }  
  
  /**
   * phone verify
   */
   phoneverify(data: any) {
    return this.http.post('phonevalidation',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }   
}
