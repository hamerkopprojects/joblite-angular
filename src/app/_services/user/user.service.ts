import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpclientService } from './../wrapper/httpclient.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  // constructor(private http: HttpClient) {}
  /**
     * Constructor for the home service class
     * @param http http
     */
  constructor(private http: HttpclientService) {
      this.http = http; // Setting http as overrided http service named as HttpclientService
  }

  // tslint:disable-next-line:typedef
  /**
   * To check email is already exists or not.
   */
  emailCheck(data: any){
    console.log(data);
    return this.http.post('auth/otp/signupfirst', data).pipe(
      map((response: any) => {
        return response;
      })
    );

 }

   /**
   * To check email is already exists or not.
   */
  emailCheckAuto(data: any){

    return this.http.post('auth/otp/signupauto', data).pipe(
      map((response: any) => {
        return response;
      })
    );

 }

    /**
   * To check email is already exists or not.
   */
    emailCheckDirect(data: any){

      return this.http.post('auth/otp/checkemail', data).pipe(
        map((response: any) => {
          return response;
        })
      );
  
   }

  /**
   * To check email is already exists or not.
   */
       changePassword(data: any){

        return this.http.post('auth/otp/changepassword', data).pipe(
          map((response: any) => {
            return response;
          })
        );
    
     }

   /**
   * To create a customer.
   */
  createCustomer(data: any){

    return this.http.post('auth/otp/signup', data).pipe(
      map((response: any) => {
        return response;
      })
    );

 }


   /**
   * To create a social media customer.
   */
    createCustomerSocial(data: any){

      return this.http.post('auth/otp/socialsignup', data).pipe(
        map((response: any) => {
          return response;
        })
      );
  
   } 

  /**
   * login.
   */
  login(data: any){

    return this.http.post('auth/otp/login', data).pipe(
      map((response: any) => {
        return response;
      })
    );

 }


  /**
   * login.
   */
   sociallogin(data: any){

    return this.http.post('auth/otp/sociallogin', data).pipe(
      map((response: any) => {
        return response;
      })
    );

 } 

  /**
   * login.
   */
  verify(data: any){

    return this.http.post('auth/otp/verify', data).pipe(
      map((response: any) => {
        return response;
      })
    );

 }

   /**
   * verify otp.
   */
    verifyotp(data: any){

      return this.http.post('auth/otp/verifyotp', data).pipe(
        map((response: any) => {
          return response;
        })
      );
  
   }
 
}
