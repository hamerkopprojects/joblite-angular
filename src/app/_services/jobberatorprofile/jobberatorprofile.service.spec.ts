import { TestBed } from '@angular/core/testing';

import { JobberatorprofileService } from './jobberatorprofile.service';

describe('JobberatorprofileService', () => {
  let service: JobberatorprofileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JobberatorprofileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
