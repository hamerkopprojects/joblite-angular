import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpclientService } from './../wrapper/httpclient.service';

@Injectable({
  providedIn: 'root'
})
export class JobberatorprofileService {

  /**
     * Constructor for the home service class
     * @param http http
     */
    constructor(private http: HttpclientService) {
      this.http = http; // Setting http as overrided http service named as HttpclientService
  }

/**
 * Notification view status update
 */
 notificationView(data: any) {
    return this.http.post('notificationviewed',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

/**
 * Notification view task status update
 */
 notificationViewTask(data: any) {
  return this.http.post('notificationtaskviewed',data).pipe(
    map((response: any) => {
      return response;
    })
  );
}

/**
 * Notification view status update
 */
 jobtypeNotificationView(data: any) {
  return this.http.post('jobtypenotificationviewed',data).pipe(
    map((response: any) => {
      return response;
    })
  );
}

/**
 * Notification view status update
 */
 jobtypeProfileNotificationView(data: any) {
  return this.http.post('jobtypeprofilenotificationviewed',data).pipe(
    map((response: any) => {
      return response;
    })
  );
}

  /**
   * To get all jobberatorStepOneDetails
   */
  jobberatorStepOneDetails(data: any) {
    return this.http.post('jobberatorsteponedetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To verify id
   */  
    idVerify(data: any ) {
      return this.http.post('idverify',data).pipe(
        map((response: any) => {
          return response;
        })
      );
    } 

  /**
   * To verify license
   */  
      licenseVerify(data: any ) {
        return this.http.post('licenseverify',data).pipe(
          map((response: any) => {
            return response;
          })
        );
      } 


  /**
   * To change the profile type
   */  
  profileChange(data: any) {
    return this.http.post('profiletypechange',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To get all jobberatorStepTwoDetails
   */
  jobberatorStepTwoDetails(data: any) {
    return this.http.post('jobberatorsteptwodetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To get all jobberatorStepThreeDetails
   */
  jobberatorStepThreeDetails(data: any) {
    return this.http.post('jobberatorstepthreedetails',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To get all language,prificiency and race list
   */
  langProficiencyRaceList() {
    return this.http.get('langproficiencylist').pipe(
      map((response: any) => {
        return response;
      })
    );
  }

   /**
  * To upload images
  */
 upload(formData: any,id: any){
  var url: any;
  url = 'auth/jobberator/profileimageupdate/'+id;
  return this.http.postform(url,formData).pipe(
    map((response: any) => {
      console.log("response",response);
      return response;

    })
  );
  console.log("")

  }

  /**
   * To save step1 details
   */
  step1Save(data: any) {
    return this.http.post('auth/jobberator/addstepone',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To save step2 details
   */
  step2Save(data: any) {
    return this.http.post('auth/jobberator/addsteptwo',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  /**
   * To save step2 details
   */
  step3Save(data: any) {
    return this.http.post('auth/jobberator/addstepthree',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

 /**
  * To upload id card
  */
 idcardupload(formData: any,id: any){
  var url: any;
  url = 'auth/jobberator/idcardupdate/'+id;
  return this.http.postform(url,formData).pipe(
    map((response: any) => {
      console.log("response",response);
      return response;

    })
  );
  console.log("")

  }


 /**
  * To upload driving licens
  */
 dlcardupload(formData: any,id: any){
  var url: any;
  url = 'auth/jobberator/dlcardupdate/'+id;
  return this.http.postform(url,formData).pipe(
    map((response: any) => {
      console.log("response",response);
      return response;

    })
  );
  console.log("")

  }

  /**
   * To get all jobberatorJobCategoryDetails
   */
   jobberatorJobCategoryDetails(data: any) {
  // console.log('input data');
    return this.http.post('jobtypesuserbycat',data).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

}
