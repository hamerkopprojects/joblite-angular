/**
 * Wrapper service module - Import dependencies here
 */
import { NgModule } from '@angular/core';
import { HttpclientService } from '../../_services/wrapper/index';

/**
 * Wrapper service module
 */
@NgModule({
    imports: [
    ],
    declarations: [],
    providers: [
        HttpclientService
    ]
})

/**
 * Exporting wrapper service module
 */
export class WrapperServiceModule { }
