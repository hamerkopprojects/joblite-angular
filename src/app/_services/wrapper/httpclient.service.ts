/**
 * Http client service - Import dependencies here
 */
import { Injectable } from '@angular/core';
//import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { NgxSpinnerService } from "ngx-spinner";

/**
 * Service used to override the http get and post
 */
@Injectable()

/**
 * Http client service class
 */
export class HttpclientService {
  /**
   * Constructor for the http client service class
   * @param http          http
   * @param SpinnerService spinner service
   */
  constructor(
    private http: HttpClient,
    private SpinnerService: NgxSpinnerService) { }

  /**
   * Overriding the http get method
   * @param url url
   */
  get(url: string) {
    this.showSpinner(); // To start the loader spinner
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Authorization': 'Bearer '+token });
    return this.http.get(environment.api_url + url, { headers: headers }).pipe(
      map((response: any) => {
        this.hideSpinner(); // To stop the loader spinner
        return response;
      })
    );
  }

  /**
   * Overriding the http get method to read header
   * @param url url
   */
  getfromheader(url: string) {
    this.showSpinner(); // To start the loader spinner
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Authorization': 'Bearer '+token });
    return this.http.get(environment.api_url + url, { headers: headers, observe: 'response' }).pipe(
      map((response: any) => {
        this.hideSpinner(); // To stop the loader spinner
        return response;
      })
    );
  }

    /**
   * Overriding the http get method to read header
   * @param url url
   */
    getlatest(url: string, token: string) {
      this.showSpinner(); // To start the loader spinner
      // const token = localStorage.getItem('token');
      const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Authorization': 'Bearer '+token });
      return this.http.get(url, { headers: headers, observe: 'response' }).pipe(
        map((response: any) => {
          this.hideSpinner(); // To stop the loader spinner
          return response;
        })
      );
    }

  /**
   * Overriding the http post method
   * @param url  url
   * @param data data
   */
   postverifylatest(url: string, data: any) {
    this.showSpinner(); // To start the loader spinner
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'fcAccId':'0', 'fcPass':'0','Accept': 'application/json','Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': '*','Access-Control-Allow-Methods': '*'});
    return this.http.post(url, data, { headers: headers }).pipe(
      map((response: any) => {
        this.hideSpinner(); // To stop the loader spinner
        return response;
      })
    );
  } 

  /**
   * Overriding the http post method
   * @param url  url
   * @param data data
   */
   postverify(url: string, data: any) {
    this.showSpinner(); // To start the loader spinner
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json','Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': '*','Access-Control-Allow-Methods': '*'});
    return this.http.post(url, data, { headers: headers }).pipe(
      map((response: any) => {
        this.hideSpinner(); // To stop the loader spinner
        return response;
      })
    );
  }  

  /**
   * Overriding the http post method
   * @param url  url
   * @param data data
   */
  post(url: string, data: any) {
    this.showSpinner(); // To start the loader spinner
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Authorization': 'Bearer '+token });
    return this.http.post(environment.api_url + url, data, { headers: headers }).pipe(
      map((response: any) => {
        this.hideSpinner(); // To stop the loader spinner
        return response;
      })
    );
  }

    /**
   * Overriding the http post method
   * @param url  url
   * @param data data
   */
  postfromheader(url: string, data: any) {
    this.showSpinner(); // To start the loader spinner
    const token = localStorage.getItem('token');
    // const headers = new HttpHeaders({ 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*','secure': 'false','changeOrigin': 'true', 'mode': 'no-cors' });
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Authorization': 'Bearer '+token});
    return this.http.post(environment.api_url + url, data, { headers: headers, withCredentials: true, observe: 'response' }).pipe(
      map((response: any) => {
        this.hideSpinner(); // To stop the loader spinner
        return response;
      })
    );
  }


   /**
   * Overriding the http post method
   * @param url  url
   * @param data data
   */
  postform(url: string, data: any) {
    this.showSpinner(); // To start the loader spinner
    const token = localStorage.getItem('token');
    // const headers = new HttpHeaders({ 'Accept':'application/json','Content-Type': 'multipart/form-data;boundary=----WebKitFormBoundaryyrV7KO0BoCBuDbTL', 'Authorization': 'Bearer '+token });
    const headers = new HttpHeaders({ 'Authorization': 'Bearer '+token });
    return this.http.post(environment.api_url + url, data, { headers: headers }).pipe(
      map((response: any) => {
        this.hideSpinner(); // To stop the loader spinner
        return response;
      })
    );
  }

     /**
   * Overriding the http post method
   * @param url  url
   * @param data data
   */
     postlinkedin(url: string, code: any) {
      this.showSpinner(); // To start the loader spinner
      // const token = localStorage.getItem('token');
      // const headers = new HttpHeaders({ 'Accept':'application/json','Content-Type': 'multipart/form-data;boundary=----WebKitFormBoundaryyrV7KO0BoCBuDbTL', 'Authorization': 'Bearer '+token });
      // const headers = new HttpHeaders({ 'Authorization': 'Bearer '+token });
      // const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
      // const headers = new HttpHeaders({});
      // return this.http.post(url, data, { headers: headers }).pipe(
      //   map((response: any) => {
      //     this.hideSpinner(); // To stop the loader spinner
      //     return response;
      //   })
      // );
      const token = localStorage.getItem('token');
      const headers = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded,application/jsonp',
        'Access-Control-Allow-Origin': '*', 
        'Access-Control-Allow-Methods' : 'GET, POST, PUT, DELETE',
        'Authorization': 'Bearer '+token 
      });
    
      const requestBody = new URLSearchParams();
      requestBody.set('grant_type', 'authorization_code');
      requestBody.set('code', code);
      // requestBody.set('redirect_uri', 'https://www.joblite.co.za/linkedinlogin');
      requestBody.set('redirect_uri', environment.callback_url +'linkedinlogin');
      requestBody.set('client_id', '77gg45kc43nbzy');
      requestBody.set('client_secret', 'UPRs45kxztCDzjfU');
    
      // return this.http.post(environment.callback_url + url, requestBody.toString(), { headers });
      return this.http.post(environment.api_url + url, requestBody.toString(), { headers: headers, withCredentials: true, observe: 'response' }).pipe(
        map((response: any) => {
          this.hideSpinner(); // To stop the loader spinner
          return response;
        })
      );
    }

  /**
   * Overriding the http delete method
   * @param url  url
   * @param data data
   */
  delete(url: string) {
    this.showSpinner(); // To start the loader spinner
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Authorization': 'Bearer '+token });
    return this.http.delete(environment.api_url + url, { headers: headers }).pipe(
      map((response: any) => {
        this.hideSpinner(); // To stop the loader spinner
        return response;
      })
    );
  }

  
   /**
   * Overriding the http delete method
   * @param url  url
   * @param data data
   */
  delete_string_responce(url: string) {
    this.showSpinner(); // To start the loader spinner
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Authorization': 'Bearer '+token });
    // this.http.delete(environment.api_url + url, {responseType: 'arraybuffer'});
    return this.http.delete(environment.api_url + url, { headers: headers , observe: 'response', responseType: 'text'}).pipe((response: any) => {
        this.hideSpinner(); // To stop the loader spinner
        return response;
      })
    ;
  }

  /**
   * Overriding the http put method
   * @param url  url
   * @param data data
   */
  put(url: string, data: any) {
    this.showSpinner(); // To start the loader spinner
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Authorization': 'Bearer '+token });
    return this.http.put(environment.api_url + url, data, { headers: headers }).pipe(
      map((response: any) => {
        this.hideSpinner(); // To stop the loader spinner
        return response;
      })
    );
  }


 /**
   * Overriding the http put method
   * @param url  url
   * @param data data
   */
  put_string_responce(url: string, data: any) {
    this.showSpinner(); // To start the loader spinner
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'Authorization': 'Bearer '+token });
    return this.http.put(environment.api_url + url, data, { headers: headers , observe: 'response', responseType: 'text'}).pipe(
      (response: any) => {
        this.hideSpinner(); // To stop the loader spinner
        return response;
      })
    ;
  }


  /**
   * Starting loader spinner
   */
  showSpinner() {
    this.SpinnerService.show();
    //this.loaderService.show();
  }

  /**
   * Stop loader spinner
   */
  hideSpinner() {
    this.SpinnerService.hide();
    //this.loaderService.hide();
  }

  /**
   * Stop loader spinner in case of error
   */
  private handleError(error: any) {
    this.SpinnerService.hide();
    //this.loaderService.hide();
  }
}
