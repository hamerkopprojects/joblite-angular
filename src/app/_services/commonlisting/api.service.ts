import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";
import { environment } from "../../../environments/environment";
import { HttpclientService } from './../wrapper/httpclient.service';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private httpOptions: any

  constructor(
    private http: HttpclientService, 
    private router: Router
  ) { }

    // get job details
    notificationList(parameter:any) {
      const params={
        customers_id : parameter.customers_id,
        user_type : parameter.user_type


      } 
         
        return this.http
          .post('jobberjobintimationsloglist', params)
          .pipe(
            map(data => {
              return data;
            })
          );
      }

    // get job details
    doGetJobList(type: string) {
      const params = {
        type: type
      };      
        return this.http
          .post('searchjoblist', params)
          .pipe(
            map(data => {
              return data;
            })
          );
      }


         // get job details
    doGetJobListByLoc(userId: any,loccoordinates: string, type: string) {
      const params = {
        customers_id:userId,
        loccoordinates: loccoordinates,
        type: type
      };      
        return this.http
          .post('searchjoblist', params)
          .pipe(
            map(data => {
              return data;
            })
          );
      } 

      doGetJobLoggedInList(userId: any,type: string) {
        const params={
          customers_id:userId,
          type: type
        }      
          return this.http
            .post('searchjoblist', params)
            .pipe(
              map(data => {
                return data;
              })
            );
        }

      // get education details
      eduDetailsCustomer(customer_id: any) {
        const params = {
          customerId: customer_id,
        };
        return this.http
          .post('usereduquali', params)
          .pipe(
            map(data => {
              return data;
            })
          );
      }
        
      // get language details
      languageDetailsCustomer(customer_id: any) {
        const params = {
          customerId: customer_id,
        };
        return this.http
          .post('userlangs', params)
          .pipe(
            map(data => {
              return data;
            })
          );
      }

        
    // get ratings
    ratingDetailsCustomer(customer_id: any) {
      const params = {
        customer_id: customer_id,
      };
      return this.http
        .post('ratingdetailscustomer', params)
        .pipe(
          map(data => {
            return data;
          })
        );
    } 
        
    // get ratings
    ratingDetailsJobs(jobpost_id: number) {
      const params = {
        job_id: jobpost_id,
      };
      return this.http
        .post('ratingdetailsjobs', params)
        .pipe(
          map(data => {
            return data;
          })
        );
    }        

    // get job details
    doGetJobDetails(jobpost_id: number) {
      let custId = localStorage.getItem('user_id');      
      const params = {
        jobpost_id: jobpost_id,
        cust_id: custId
      };
      return this.http
        .post('jobdetails', params)
        .pipe(
          map(data => {
            return data;
          })
        );
    }

    getPlanSettings(params: any) {
      return this.http
        .post('planprofilesettings', params)
        .pipe(
          map(data => {
            return data;
          })
        );
    }


    // get job details by category
    doGetJobListbyCategory() {
      const params = {};       
      return this.http
        .post('searchjobcategorylist', params)
        .pipe(
          map(data => {
            return data;
          })
        );
    }

    getAccessToken(params: any) {
    
      return this.http
        .post('lnauth', params)
        .pipe(
          map(data => {
            return data;
          })
        );
    }

    getLinkedInProfile(params: any) {
    
      return this.http
        .post('lnprofile', params)
        .pipe(
          map(data => {
            return data;
          })
        );
    }


  /**
   * Get linkedin access
   */
      linkedinaccess(data: any) {
        return this.http.postlinkedin(environment.callback_url+'linkedinAccess',data).pipe(
          map((response: any) => {
            return response;
          })
        ); 
        // return this.http.postverifylatest('https://www.linkedin.com/oauth/v2/accessToken',data).pipe(
        //   map((response: any) => {
        //     return response;
        //   })
        // );    
      }


    /**
     * Get linkedin data
     */
    linkedindetails(data: any) {
      return this.http.getlatest(environment.callback_url+'linkedinDetails',data).pipe(
        map((response: any) => {
          return response;
        })
      );
      // return this.http.postverify(environment.callback_url+'linkedinDetails',data).pipe(
      //   map((response: any) => {
      //     return response;
      //   })
      // ); 
      // return this.http.postverifylatest('https://www.linkedin.com/oauth/v2/accessToken',data).pipe(
      //   map((response: any) => {
      //     return response;
      //   })
      // );    
    }

    doGetJobListbyJobtypeFilter(job_type_array: any,search_jobtype:any,userId: any,search_page: any, type: string) {
      let parms: any; 

      if(typeof localStorage.loccoordinates !== 'undefined') { 
        let loccoordinates = localStorage.getItem('loccoordinates');
        parms = {
          search_jobtype:search_jobtype,
          job_type_array:job_type_array,
          customers_id:userId,
          search_limit:`${environment.search_limit}`,
          search_page:search_page,
          type: type,
          loccoordinates: loccoordinates         
        }
      } else {
        parms={
          search_jobtype:search_jobtype,
          job_type_array:job_type_array,
          customers_id:userId,
          search_limit:`${environment.search_limit}`,
          search_page:search_page,
          type: type         
        }
      }
      return this.http
        .post('searchjobfilterlist',parms)
        .pipe(
          map(data => {
            return data;
          })
        );
    }    

        // get job details by jobType
        doGetJobListbyJobtype(userId: any,search_page: any,search_jobtype:any,type: string,name: string) {
          let parms: any; 
          if(typeof localStorage.loccoordinates !== 'undefined') { 
            let loccoordinates = localStorage.getItem('loccoordinates');
            parms = {
              customers_id:userId,
              search_jobtype:search_jobtype,            
              search_limit:`${environment.search_limit}`,
              search_page:search_page,
              type: type,
              name: name,
              loccoordinates: loccoordinates 
            }
          } else {
            parms = {
              customers_id:userId,
              search_jobtype:search_jobtype,            
              search_limit:`${environment.search_limit}`,
              search_page:search_page,
              type: type,
              name: name 
            }  
          }      
          return this.http
            .post('searchjobfilterlist',parms)
            .pipe(
              map(data => {
                return data;
              })
            );
        }


      // To get favorite jobs
      favoriteJobs(userId: any) {
        const parms={
          customers_id:userId
        }
        return this.http
          .post('favouritelist',parms)
          .pipe(
            map(data => {
              return data;
            })
          );
      }


      // get job details by jobType
      doGetJobListbyCatFilter(search_jobcate:any,search_jobtype:any, userId: any,search_page: any, type: string) {
        let parms: any; 

        if(typeof localStorage.loccoordinates !== 'undefined') { 
          let loccoordinates = localStorage.getItem('loccoordinates');
          parms = {
            search_jobcate:search_jobcate,
            search_jobtype:search_jobtype,
            customers_id:userId,
            search_limit:`${environment.search_limit}`,
            search_page:search_page,
            type: type,
            loccoordinates: loccoordinates        
          }
        } else {
          parms = {
            search_jobcate:search_jobcate,
            search_jobtype:search_jobtype,
            customers_id:userId,
            search_limit:`${environment.search_limit}`,
            search_page:search_page,
            type: type        
          }
        }
        return this.http
          .post('searchjobfilterlist',parms)
          .pipe(
            map(data => {
              return data;
            })
          );
      }      

      // get job details by jobType
      doGetJobListbyJobtypeFilterWithCategory(search_jobcate: any,search_jobtype: any, userId: any,search_page: any, type:string) {
        let parms: any; 

        if(typeof localStorage.loccoordinates !== 'undefined') { 
          let loccoordinates = localStorage.getItem('loccoordinates');
          parms = {
            search_jobcate:search_jobcate,
            search_jobtype:search_jobtype,
            customers_id:userId,
            search_limit:`${environment.search_limit}`,
            search_page:search_page,
            type: type,
            loccoordinates: loccoordinates           
          }
        } else {
          parms = {
            search_jobcate:search_jobcate,
            search_jobtype:search_jobtype,
            customers_id:userId,
            search_limit:`${environment.search_limit}`,
            search_page:search_page,
            type: type           
          }
        }
        return this.http
          .post('searchjobfilterlist',parms)
          .pipe(
            map(data => {
              return data;
            })
          );
      }

      // get job details by jobType
      jobTypesList(search_jobcate:any) {
        const parms={
          search_jobcate:search_jobcate
        }
        return this.http
          .post('searchjobtypelist',parms)
          .pipe(
            map(data => {
              return data;
            })
          );
      }  
      
      // get job details by jobType
      jobTypesListWithoutCategory() {
        const params = {};         
        return this.http
          .post('searchjobtypelist', params)
          .pipe(
            map(data => {
              return data;
            })
          );
      } 
      
      closeJob(jobId:any,userId: any) {
        const parms={
          jobpost_id:jobId,
          customers_id:userId
        }
        return this.http
          .post('jobbercanceljob',parms)
          .pipe(
            map(data => {
              return data;
            })
          );
      }     
      
      acceptJob(jobId:any,userId: any) {
        const parms={
          jobpost_id:jobId,
          customers_id:userId
        }
        return this.http
          .post('jobberapplyjob',parms)
          .pipe(
            map(data => {
              return data;
            })
          );
      }   
      
      addToWishList(jobId:any,userId: any,isFavourite: any) {
        const parms={
          jobpost_id:jobId,
          customers_id:userId,
          is_favourite:isFavourite
        }
        return this.http
          .post('jobberfavouritejob',parms)
          .pipe(
            map(data => {
              return data;
            })
          );
      } 
      
      fetchJobApplicatsList(jobId:any,search_page: any){
        const parms={
          jobpost_id:jobId,
          search_limit:`${environment.search_limit}`,
          search_page:search_page 

        }
        return this.http.post('jobapplicantlist',parms).pipe( map(response => {
          
            return response;
          })
        );

      }

      cancellationReasons() {
       return this.http.get('cancellationreasons').pipe( map(response => {
           return response;
         })
       );

     }

      jobTaskListDetails(jobId:any) {
         const parms={
          jobpost_id:jobId
        }
        return this.http.post('taskdetails',parms).pipe( map(response => {
            return response;
          })
        );

      }
     
      jobberatorJobApplicantsStatusChange(jobintId:any,userId:any,status:string) {
        const parms={
          job_intimation_id : jobintId,
          customers_id : userId,
          status : status
        }
        return this.http.post('jobberatorjobapplicantstatuschange',parms).pipe( map(response => {
            return response;
          })
        );


      }

      jobberJobApplicantsStatusChange(jobintId:any,userId:any,status:string) {
        const parms={
          job_id : jobintId,
          customers_id : userId,
          status : status
        }
        return this.http.post('jobberjobapplicantstatuschange',parms).pipe( map(response => {
            return response;
          })
        );


      }

      jobberatorJobApplicantsStatusChangeStart(jobintId:any,userId:any,status:string,data:any) {
        const parms={
          job_id : jobintId,
          customers_id : userId,
          status : status,
          data : data
        }
        return this.http.post('jobberatorjobapplicantstartchange',parms).pipe( map(response => {
            return response;
          })
        );


      }
      
      tippay(data:any) {
        const parms={
          data : data
        }
        return this.http.post('tippay',parms).pipe( map(response => {
            return response;
          })
        );


      }  
      
      idpay(data:any) {
        const parms={
          data : data
        }
        return this.http.post('idpay',parms).pipe( map(response => {
            return response;
          })
        );


      } 

      licensepay(data:any) {
        const parms={
          data : data
        }
        return this.http.post('licensepay',parms).pipe( map(response => {
            return response;
          })
        );


      } 
      
      chatList(jobPostId) {
        const parms={
          jobPostId : jobPostId
        }        
        return this.http.post('chatlist',parms).pipe(
          map((response: any) => {
            return response;
          })
        );
      }      
      
      
      chat(jobPostId: any, custId: any, chattext: any) {
     
        const parms={
          jobPostId : jobPostId,
          custId: custId,
          chattext : chattext
        }
        return this.http.post('chatadd',parms).pipe( map(response => {
          return response;
        })
      );

      }    
      
      tempTaskAdd(jobId: any) {
        const parms={
          jobId : jobId
        }
        return this.http.post('jobtasktempadd',parms).pipe( map(response => {
          return response;
        })
      );

      }

      jobTaskStatusChange(taskId: any, status: any, message: any) {
        let custId = localStorage.getItem('user_id');      
        const parms={
          jobtask_id : taskId,
          cust_id: custId,
          status : status,
          message : message
        }
        return this.http.post('jobtaskstatuschange',parms).pipe( map(response => {
          return response;
        })
      );

      }

      jobExpiryDetails(jobPostId: any) {
        const parms={
          jobpost_id : jobPostId
        }
        return this.http.post('jobexpirydetails',parms).pipe( map(response => {
          return response;
        })
      );

      } 
      
      continueAdverticement(jobPostId: any) {
        const parms={
          jobpost_id : jobPostId
        }
        return this.http.post('continueadverticement',parms).pipe( map(response => {
          return response;
        })
      );

      } 

      updateJobberatorApplicantFavourite(jobintId:any,customerId:any,status:any) {
        const parms={
          job_intimation_id : jobintId,
          customers_id : customerId,
          is_favourite : status
        }
        return this.http.post('jobberatorapplicantfavourite',parms).pipe( map(response => {
          return response;
        })
      );

      }
      doGetJobListbySearch(userId: any,search_page: any,search_jobtype:any,searchname:any,type: string) {
        let parms: any;
        
        if(typeof localStorage.loccoordinates !== 'undefined') { 
          let loccoordinates = localStorage.getItem('loccoordinates');
          parms={
            type: type,
            // customers_id:userId,
            // search_jobtype:search_jobtype,            
            // search_limit:`${environment.search_limit}`,
            // search_page:search_page,
            name:searchname,
            loccoordinates: loccoordinates
          }
        } else {
          parms={
            type: type,
            // customers_id:userId,
            // search_jobtype:search_jobtype,            
            // search_limit:`${environment.search_limit}`,
            // search_page:search_page,
            name:searchname
          } 
        }
        return this.http
          .post('searchjobfilterlist',parms)
          .pipe(
            map(data => {
              return data;
            })
          );
      }

      myJobsCompletedList(customerId : any) {

        const parms={
          customers_id : customerId,
        }
        return this.http.post('jobberatorcompletedlist',parms).pipe( map(response => {
          return response;
        })
      );
      }

      myMoneyList(parms : any) {

        // const parms={
        //   customers_id : customerId,
        // }
        return this.http.post('jobberatormymoneylist',parms).pipe( map(response => {
          return response;
        })
      );
      }     
      

      myMoneyJobberList(parms : any) {

        // const parms={
        //   customers_id : customerId,
        // }
        return this.http.post('jobbermymoneylist',parms).pipe( map(response => {
          return response;
        })
      );
      }       

      myJobberJobsCompletedList(customerId : any) {

        const parms={
          customers_id : customerId,
        }
        return this.http.post('jobbercompletedlist',parms).pipe( map(response => {
          return response;
        })
      );
      }     
    jobNotifications(parameter:any){
      const params = {
        customers_id : parameter.customers_id,
        user_type : parameter.user_type
      };       
        return this.http
          .post('jobberjobintimationsloglist', params)
          .pipe(
            map(data => {
              return data;
            })
          );

    }

    MyjobsNotifications(parameter:any){
      const params = {
        customers_id : parameter.customers_id,
        user_type : parameter.user_type
      };       

        return this.http
          .post('jobberjobintimationsloglist', params)
          .pipe(
            map(data => {
              return data;
            })
          );

    }

    jobHistory(params:any){
 
        return this.http
          .post('jobhistory', params)
          .pipe(
            map(data => {
              return data;
            })
          );

    } 

    taskHistory(params:any){
 
        return this.http
          .post('taskhistory', params)
          .pipe(
            map(data => {
              return data;
            })
          );

    }
    
    taskDocs(params:any){
 
      return this.http
        .post('taskdocs', params)
        .pipe(
          map(data => {
            return data;
          })
        );

  }     
    
  /**
  * To upload images
  */
  taskdocupload(formData: any,id: any){
    var url: any;
    let custId = localStorage.getItem('user_id');    
    url = 'jobtaskdocumentupload/'+id+'/'+custId;
    return this.http.postform(url,formData).pipe(
      map((response: any) => {
        return response;
  
      })
    );
  
  }    

     
}
