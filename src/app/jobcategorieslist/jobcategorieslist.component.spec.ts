import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobcategorieslistComponent } from './jobcategorieslist.component';

describe('JobcategorieslistComponent', () => {
  let component: JobcategorieslistComponent;
  let fixture: ComponentFixture<JobcategorieslistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobcategorieslistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobcategorieslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
