import { Component, OnInit,  ViewChild, ElementRef } from '@angular/core';
import { ApiService } from '../_services/commonlisting/api.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
import { environment } from './../../environments/environment';
import { NotificationService } from './../_services/notification.service';
import { CategoriesService } from './../_services/categories/categories.service';
import { JobberprofileService } from './../_services/jobberprofile/jobberprofile.service';
declare var jQuery:any;

@Component({
  selector: 'app-jobcategorieslist',
  templateUrl: './jobcategorieslist.component.html',
  styleUrls: ['./jobcategorieslist.component.css']
})
export class JobcategorieslistComponent implements OnInit {
  @ViewChild('myDivPopUp') myDivPopUp: ElementRef<HTMLElement>;
  server_url: string;
  isFullListDisplayed: boolean = false;  
  settingsbody: any;
  titlemessage: string;
  filterType: string; 
  bestJobsText: string;
  popUpJobDetails: any;
  popUpi: any; 
  dynamicLoginMessage: string;
  status:any;
  jobId: any;
  count: any;
  jobCatagoryList = [];
  jobCatagory = [];
  jobCatagoryArray = [];
  jobCatagoryTypeArray = [];  
  jobNameArray = [];  
  jobTypeArray = [];
  jobTypesByCat = [];
  loggedIn: boolean; 
  jobCategoryName:any;
  jobType:string;
  workType = ['online','inperson'];
  allJobTypes = [];
  inperson: boolean;
  online: boolean;
  isCategorySearch: boolean;
  searchPage: number;
  totalResults: any;
  totalCount: any;
  previous: any;
  next: any;
  page_counts: any;
  start: any;  
  currentPrevious: any;
  currentNext: any;
  currentCount: any;
  checkCurrentCount: any;  
  jobIdReceived: any;
  countReceived: any;
  clickTypeStatusReceived: any;


  isChecked: any;
  checkedCategoryList: any;
  //checkedCategoryList:any;

  searchName: any;


  

  constructor(    private router: Router,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private category: CategoriesService,
    private jobber: JobberprofileService,
    private notifyService : NotificationService) { }

  ngOnInit(): void {
    this.server_url = environment.server_url;
    this.otherSettingsList();
    this.settingsbody = [];
    this.titlemessage = '';
    this.filterType = '';
    this.bestJobsText = 'Recent jobs';
    this.dynamicLoginMessage = ''; 
    this.isCategorySearch = true;
    this.searchPage = 1;
    this.previous = 0;
    this.next = 0;
    this.currentPrevious = 0;
    this.currentNext = 0;    
    this.page_counts = environment.page_counts;
    this.start = 1;
    this.currentCount = 0;
    this.checkCurrentCount = 0;
    this.inperson = true;
    this.online = true;
    if(typeof localStorage.token !== 'undefined') {
      this.loggedIn = true;
    } else {
      this.loggedIn = false;      
    }    

    this.route.queryParams.subscribe(params => {
      this.jobCategoryName = params['category'];
      this.searchName = params['searchname'];
      console.log("jobcategoryname",this.jobCategoryName )
      console.log("searchName",this.searchName )

      if (this.jobCategoryName === undefined) {
        if(this.searchName != undefined){

          // this.doGetJobListbySearch('');
          this.doGetJobListbyJobtype('');
        }else{
          this.searchName = '';
          this.doGetJobListbyJobtype('');
        setTimeout(() =>
        {
            this.initialJobStart();
            this.jobTypeAutoCheck(null,null);
        }, 2000);

        }
        
       
        //this.jobTypesListWithoutCategory();
      } else {
      this.searchName = '';
      this.doGetJobListbyJobtypeFiltersWithcategory('');
      this.jobCatagoryArray.push(parseInt(this.jobCategoryName));
      this.jobTypesList(this.jobCategoryName);
     
      setTimeout(() =>
        {
            this.initialJobStart();
            this.jobTypeAutoCheck("Single",this.jobCategoryName);
        }, 2000);
      }
      
          
    });
    
    this.doGetJobListbyCategory();
    // this.doGetJobListbyJobtypeFilters();
    
    //this.initialJobStart();
  }

  otherSettingsList() {
    //this.SpinnerService.show();
    this.category.getOtherSettings().subscribe((res) => {
      this.settingsbody = res.other;
     // this.redirectTo('/');
    }, error => {
      //this.redirectTo('/');
    }
    );
  }


  showToasterSuccess(){
    this.notifyService.showSuccess(this.titlemessage, "")
  }
  
  showToasterError(){
      this.notifyService.showError(this.titlemessage, "")
  }
  
  showToasterInfo(){
      this.notifyService.showInfo(this.titlemessage, "")
  }
  
  showToasterWarning(){
      this.notifyService.showWarning(this.titlemessage, "")
  }


  sortJob(type: string) {
    this.filterType = type;
    this.jobCatagoryList = [];
    if (type == '') {
      this.bestJobsText = 'Recent jobs';      
    }
    if (type == 'price') {
      this.bestJobsText = 'Price - high to low';
    }
    if (type == 'pricelow') {
      this.bestJobsText = 'Price - low to high';
    }
    if (type == 'lowtohigh') {
      this.bestJobsText = 'Rating low to high';
    }
    if (type == 'hightolow') {
      this.bestJobsText = 'Rating high to low';      
    }
    if (type == 'near') {
      this.bestJobsText = 'Jobs near me';      
    }
    this.route.queryParams.subscribe(params => {
      this.jobCategoryName = params['category'];
      this.searchName = params['searchname'];
      console.log("jobcategoryname",this.jobCategoryName )
      console.log("searchName",this.searchName )

      if (this.jobCategoryName === undefined) {
        if(this.searchName != undefined){

          this.doGetJobListbySearch(type);
        }else{
          this.searchName = '';
          this.doGetJobListbyJobtype(type);
        setTimeout(() =>
        {
            this.initialJobStart();
            this.jobTypeAutoCheck(null,null);
        }, 2000);

        }
        
       
        //this.jobTypesListWithoutCategory();
      }
      else{
      this.doGetJobListbyJobtypeFiltersWithcategory(type);
      this.jobCatagoryArray.push(parseInt(this.jobCategoryName));
      this.jobTypesList(this.jobCategoryName);
     
      setTimeout(() =>
        {
            this.initialJobStart();
            this.jobTypeAutoCheck("Single",this.jobCategoryName);
        }, 2000);
      }
      
          
    });
  }

  createRange(number: any){
    if (isNaN(number)) {
      number = 0;
    } 
    return new Array(number);
  }

  jobTypesListWithoutCategory() {
    this.apiService.jobTypesListWithoutCategory()
    .subscribe(
      (data: any) => {
        this.jobTypesByCat = data.data.jobtypes;
        // this.jobTypesByCat.forEach((value: any, key: any) => {
          // value.forEach((value: any, key: any) => {          
          //   this.allJobTypes[value.id] = value.id;
          // });            
        // });        
      },
      error => {
        console.log(error);
      }
    );
  }  

  jobTypesList(category: any) {
    
    this.apiService.jobTypesList(category)
    .subscribe(
      (data: any) => {
        this.jobTypesByCat = data.data.jobtypes;
        // let jobTypesByCat = JSON.stringify(this.jobTypesByCat);
        // this.jobTypesByCat = JSON.parse(jobTypesByCat);        
        console.log(this.jobTypesByCat);
       
        // this.jobTypesByCat.forEach((value: any, key: any) => {
        //   value.forEach((value: any, key: any) => {          
        //     this.allJobTypes[value.id] = value.id;
        //   });            
        // });
        // for (var i = 0; i < this.jobTypesByCat.length; i++) {
        //   this.jobCatagory[i].isSelected = true;
        //   this.jobCatagoryArray.push(this.jobCatagory[i].id);
        // }                   
      },
      error => {
        console.log(error);
      }
    );
  }

  pagination(value:any) {
    this.searchPage = value;  
    // this.jobType=value;  
    if (!this.isCategorySearch) {
      this.doGetJobListbyJobtypeFilters();
    } else {
      this.doGetJobListbyCatFilter();
    } 
  }  
  
  gonext(value:any) {
    this.searchPage = value;  
    this.start = value;
    // this.jobType=value;  
    if (!this.isCategorySearch) {
      this.doGetJobListbyJobtypeFilters();
    } else {
      this.doGetJobListbyCatFilter();
    } 
  } 

  goprev(value:any) {
    this.searchPage = value;  
    this.start = value - environment.page_counts + 1;
    // this.jobType=value;  
    if (!this.isCategorySearch) {
      this.doGetJobListbyJobtypeFilters();
    } else {
      this.doGetJobListbyCatFilter();
    } 
  }   

  range1(i: any, start: any){return i >= start?this.range1(i-1,start).concat(i):[]}  

   searchJob() {
    this.jobCatagoryList = [];
    this.doGetJobListbyJobtype('');
   }
    //Get jobdeatails
    doGetJobListbyJobtype(type: string) {
    
      this.apiService.doGetJobListbyJobtype(localStorage.user_id,this.searchPage,this.workType, this.filterType, this.searchName)
        .subscribe(
          (data: any) => {
            // data.data.resp.forEach((value: any, key: any) => {
            //   if (data.data.resp[key].job_intimation[0]?.status == 'donot_show') {
            //     data.data.resp.splice(key,1);
            //   }
            // });            
            // this.jobCatagoryList = data.data.resp;
            this.jobCatagoryList = this.jobCatagoryList.concat(data.data.resp);
            this.jobCatagoryList = this.uniqueArray2(this.jobCatagoryList);
            console.log("worktype",this.workType)
            console.log("cat",this.jobCatagoryList);
            // this.totalResults = data.data.count;
            this.totalCount = data.data.count;
           
            this.checkCurrentCount = data.data.count / environment.search_limit; 
            if (this.start == 1) {
              this.previous = 0;
            }           
            if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount > 1) {
              this.currentCount = environment.page_counts;
              if (this.checkCurrentCount > environment.page_counts) {
                this.next = this.currentCount + 1;
              }
            } else if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount <= 1){
              this.currentCount = this.checkCurrentCount;
            }

            if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount > 1) {
              this.currentCount = environment.page_counts;
              if (this.checkCurrentCount > environment.page_counts) {
                this.next = this.currentCount + 1;
              }             
            } else if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount <= 1){
              this.currentCount = this.checkCurrentCount;
            }         

            if (this.start > 1 && this.start == this.searchPage) {
              this.previous = this.start - 1;
              this.currentCount = this.start + (environment.page_counts - 1);
              if (this.currentCount >= data.data.count) {
                this.currentCount = data.data.count;
                this.next = 0;
              }

            }

            if (this.start > 1 && this.start !== this.searchPage) {
              this.currentCount = this.start + (environment.page_counts - 1);
              if (this.currentCount >= data.data.count) {
                this.currentCount = data.data.count;
              }
            }
            if (this.currentCount < 1) {
              this.currentCount = 1;
            }
            this.currentNext = this.currentCount + 1;
            this.currentPrevious = this.start - 1;
           
            this.totalResults = this.range1(this.currentCount, this.start); 

            this.jobCatagoryList.forEach((value: any, key: any) => {
              let result = value['cat_image_path'].includes("uploads/job_category/");
              if (!result) {
                value['cat_image_path'] = environment.server_url+'uploads/job_category/' +value['cat_image_path'];
              }
    
              // if ((this.jobCatagoryList[key]['jobpost_photos'].length == 0)) {
              //   this.jobCatagoryList[key]['jobpost_photos'][0] = [];
              //   this.jobCatagoryList[key]['jobpost_photos'][0].image_path = 'assets/images/Default_Image_Thumbnail.jpg';
              // } else {
              //   value['jobpost_photos'][0]['image_path'] = environment.server_url+'' +value['jobpost_photos'][0]['image_path'];                
              // }
            });  
            
          
            // this.previous = 0;
            // this.next = 0;
            // this.page_counts = environment.page_counts;
            // this.start = 1;                       
          },
          error => {
            console.log(error);
          }
        );
        }

            //Get jobdeatails
    doGetJobListbyJobtypeFiltersWithcategory(type: string) {
      console.log("type1",this.jobCatagoryTypeArray)
      this.apiService.doGetJobListbyJobtypeFilterWithCategory(this.jobCategoryName,this.workType,localStorage.user_id,this.searchPage,type)
        .subscribe(
          (data: any) => {
            // data.data.resp.forEach((value: any, key: any) => {
            //   if (data.data.resp[key].job_intimation[0]?.status == 'donot_show') {
            //     data.data.resp.splice(key,1);
            //   }
            // });           
            this.jobCatagoryList = this.jobCatagoryList.concat(data.data.resp);
            this.jobCatagoryList = this.uniqueArray2(this.jobCatagoryList);
            console.log("catlist",this.jobCatagoryList)

            // this.totalResults = data.data.count;
            this.totalCount = data.data.count;
           
            this.checkCurrentCount = data.data.count / environment.search_limit; 
            if (this.start == 1) {
              this.previous = 0;
            }           
            if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount > 1) {
              this.currentCount = environment.page_counts;
              if (this.checkCurrentCount > environment.page_counts) {
                this.next = this.currentCount + 1;
              }
            } else if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount <= 1){
              this.currentCount = this.checkCurrentCount;
            }

            if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount > 1) {
              this.currentCount = environment.page_counts;
              if (this.checkCurrentCount > environment.page_counts) {
                this.next = this.currentCount + 1;
              }  
            } else if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount <= 1){
              this.currentCount = this.checkCurrentCount;
            }         

            if (this.start > 1 && this.start == this.searchPage) {
              this.previous = this.start - 1;
              this.currentCount = this.start + (environment.page_counts - 1);
              if (this.currentCount >= data.data.count) {
                this.currentCount = data.data.count;
                this.next = 0;
              }

            }

            if (this.start > 1 && this.start !== this.searchPage) {
              this.currentCount = this.start + (environment.page_counts - 1);
              if (this.currentCount >= data.data.count) {
                this.currentCount = data.data.count;
              }
            }

            if (this.currentCount < 1) {
              this.currentCount = 1;
            }
            this.currentNext = this.currentCount + 1;
            this.currentPrevious = this.start - 1;
 
            this.totalResults = this.range1(this.currentCount, this.start); 
            this.jobCatagoryList.forEach((value: any, key: any) => {
            let result = value['cat_image_path'].includes("uploads/job_category/");
            if (!result) {
              value['cat_image_path'] = environment.server_url+'uploads/job_category/' +value['cat_image_path'];
            }
    
            // if ((this.jobCatagoryList[key]['jobpost_photos'].length == 0)) {
            //     this.jobCatagoryList[key]['jobpost_photos'][0] = [];
            //     this.jobCatagoryList[key]['jobpost_photos'][0].image_path = 'assets/images/Default_Image_Thumbnail.jpg';
            //   } else {
            //     value['jobpost_photos'][0]['image_path'] = environment.server_url+'' +value['jobpost_photos'][0]['image_path'];                
            //   }
            });              
          },
          error => {
            console.log(error);
          }
        );
        }

    clearCatFilter() {
      this.jobCatagoryArray = [];
      this.jobCategoryName = '';
      this.jobTypesByCat = [];

      this.jobCatagoryTypeArray = [];
      this.jobNameArray = [];   
      this.doGetJobListbyCatFilter();
      jQuery(".catcheckbox").prop('checked',false);               
    }    
    
    doGetJobListbyCatFilterApply() {
      this.jobCatagoryList = [];
      this.doGetJobListbyCatFilter();
    }

    //Get jobdeatails
    doGetJobListbyCatFilter() {
      this.isCategorySearch = true;
      this.apiService.doGetJobListbyCatFilter(this.jobCatagoryArray,this.workType,localStorage.user_id,this.searchPage, this.filterType)
        .subscribe(
          (data: any) => {
            // data.data.resp.forEach((value: any, key: any) => {
            //   if (data.data.resp[key].job_intimation[0]?.status == 'donot_show') {
            //     data.data.resp.splice(key,1);
            //   }
            // });             
            //this.jobCatagoryList = data.data.resp;
            this.jobCatagoryList = this.jobCatagoryList.concat(data.data.resp);
            this.jobCatagoryList = this.uniqueArray2(this.jobCatagoryList);
            //this.totalResults = data.data.count;
            this.totalCount = data.data.count;
           
            this.checkCurrentCount = data.data.count / environment.search_limit; 
            if (this.start == 1) {
              this.previous = 0;
            }           
            if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount > 1) {
              this.currentCount = environment.page_counts; 
              if (this.checkCurrentCount > environment.page_counts) {
                this.next = this.currentCount + 1;
              }
            } else if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount <= 1){
              this.currentCount = this.checkCurrentCount;
            }

            if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount > 1) {
              this.currentCount = environment.page_counts;
              if (this.checkCurrentCount > environment.page_counts) {
                this.next = this.currentCount + 1;
              }  
            } else if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount <= 1){
              this.currentCount = this.checkCurrentCount;
            }         

            if (this.start > 1 && this.start == this.searchPage) {
              this.previous = this.start - 1;
              this.currentCount = this.start + (environment.page_counts - 1);
              if (this.currentCount >= data.data.count) {
                this.currentCount = data.data.count;
                this.next = 0;
              }

            }

            if (this.start > 1 && this.start !== this.searchPage) {
              this.currentCount = this.start + (environment.page_counts - 1);
              if (this.currentCount >= data.data.count) {
                this.currentCount = data.data.count;
              }
            }

            if (this.currentCount < 1) {
              this.currentCount = 1;
            }            
            this.currentNext = this.currentCount + 1;
            this.currentPrevious = this.start - 1;
 
            this.totalResults = this.range1(this.currentCount, this.start);   
            this.jobCatagoryList.forEach((value: any, key: any) => {
            let result = value['cat_image_path'].includes("uploads/job_category/");
            if (!result) {
              value['cat_image_path'] = environment.server_url+'uploads/job_category/' +value['cat_image_path'];
            }
    
              // if ((this.jobCatagoryList[key]['jobpost_photos'].length == 0)) {
              //   this.jobCatagoryList[key]['jobpost_photos'][0] = [];
              //   this.jobCatagoryList[key]['jobpost_photos'][0].image_path = 'assets/images/Default_Image_Thumbnail.jpg';
              // } else {
              //   value['jobpost_photos'][0]['image_path'] = environment.server_url+'' +value['jobpost_photos'][0]['image_path'];                
              // }
            });                       
          },
          error => {
            console.log(error);
          }
        );
        }

    clearJobFilter() {
      this.jobCatagoryTypeArray = [];
      this.jobNameArray = [];
      this.doGetJobListbyCatFilter();   
      jQuery(".jobcheckbox").prop('checked',false);         
    }

    singleClearJobFilter(index:any){
        // let index =this.jobCatagoryTypeArray.indexOf(value);
        this.jobCatagoryTypeArray.splice(index,1);
        this.jobNameArray.splice(index,1);
        this.jobCatagoryTypeArray = this.array_unique(this.jobCatagoryTypeArray);
        this.jobNameArray = this.array_unique(this.jobNameArray);                 
        if (this.jobNameArray.length == 0) {
          this.doGetJobListbyCatFilter();          
        } else {
          this.doGetJobListbyJobtypeFilters();
        }
    }    
    
    uniqueArray2(arr:any) {
      var a = [];
      var keyarray = [];
      for (var i=0, l=arr.length; i<l; i++) {
        console.log(keyarray);
        if (!keyarray.includes(arr[i].id) && arr[i] !== '') {
          a.push(arr[i]);
          keyarray.push(arr[i].id);
        }
      }
      return a;
    }
    //Get jobdeatails
    doGetJobListbyJobtypeFilters() {
      this.isCategorySearch = false;
      console.log("type",this.jobCatagoryList);
      this.apiService.doGetJobListbyJobtypeFilter(this.jobCatagoryTypeArray,this.workType,localStorage.user_id,this.searchPage, this.filterType)
        .subscribe(
          (data: any) => {
            // data.data.resp.forEach((value: any, key: any) => {
            //   if (data.data.resp[key].job_intimation[0]?.status == 'donot_show') {
            //     data.data.resp.splice(key,1);
            //   }
            // });             
            //this.jobCatagoryList = data.data.resp;
            this.jobCatagoryList = this.jobCatagoryList.concat(data.data.resp);
            this.jobCatagoryList = this.uniqueArray2(this.jobCatagoryList);
            
            //this.totalResults = data.data.count; 
            this.totalCount = data.data.count;           
           
            this.checkCurrentCount = data.data.count / environment.search_limit; 
            if (this.start == 1) {
              this.previous = 0;
            }           
            if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount > 1) {
              this.currentCount = environment.page_counts;
              if (this.checkCurrentCount > environment.page_counts) {
                this.next = this.currentCount + 1;
              }
            } else if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount <= 1){
              this.currentCount = this.checkCurrentCount;
            }

            if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount > 1) {
              this.currentCount = environment.page_counts;
              if (this.checkCurrentCount > environment.page_counts) {
                this.next = this.currentCount + 1;
              } 
            } else if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount <= 1){
              this.currentCount = this.checkCurrentCount;
            }         

            if (this.start > 1 && this.start == this.searchPage) {
              this.previous = this.start - 1;
              this.currentCount = this.start + (environment.page_counts - 1);
              if (this.currentCount >= data.data.count) {
                this.currentCount = data.data.count;
                this.next = 0;
              }

            }

            if (this.start > 1 && this.start !== this.searchPage) {
              this.currentCount = this.start + (environment.page_counts - 1);
              if (this.currentCount >= data.data.count) {
                this.currentCount = data.data.count;
              }
            }

            if (this.currentCount < 1) {
              this.currentCount = 1;
            }            
            this.currentNext = this.currentCount + 1;
            this.currentPrevious = this.start - 1;
 
            this.totalResults = this.range1(this.currentCount, this.start); 
            this.jobCatagoryList.forEach((value: any, key: any) => {
            let result = value['cat_image_path'].includes("uploads/job_category/");
            if (!result) {
              value['cat_image_path'] = environment.server_url+'uploads/job_category/' +value['cat_image_path'];
            }   
              // if ((this.jobCatagoryList[key]['jobpost_photos'].length == 0)) {
              //   this.jobCatagoryList[key]['jobpost_photos'][0] = [];
              //   this.jobCatagoryList[key]['jobpost_photos'][0].image_path = 'assets/images/Default_Image_Thumbnail.jpg';
              // } else {
              //   value['jobpost_photos'][0]['image_path'] = environment.server_url+'' +value['jobpost_photos'][0]['image_path'];                
              // }
            });              
          },
          error => {
            console.log(error);
          }
        );
        }        



                 //Get jobdeatails
    doGetJobListbyCategory() {
      this.apiService.doGetJobListbyCategory()
        .subscribe( 
          (data: any) => {
            this.jobCatagory = data.data.category;
          },
          error => {
            console.log(error);
          }
        );
        }

    addCatgeoryArray(e: any,value:any){
      if(e.target.checked){
      this.jobCatagoryArray.push(value);
      }
      else if(!e.target.checked){
        let index =this.jobCatagoryArray.indexOf(value);
        this.jobCatagoryArray.splice(index,1);
      }
      
      if(this.jobCatagoryArray.length == 0)
      {
        this.jobTypesByCat = [];
        this.jobNameArray=[];
        this.jobCatagoryTypeArray=[];
      }
      else{
        this.jobTypeAutoCheck("single",this.jobCatagoryArray);
        this.jobTypesList(this.jobCatagoryArray); 
      }   
      this.isChecked = this.jobCatagory.every(function(item:any) {
        return item.isSelected == true;
      })
      
      
                  
    }

    addCatgeoryJobTypeArray(e: any,value:any,name:any){
      console.log(value);
      if(e.target.checked){
        this.jobCatagoryTypeArray.push(value);
        this.jobNameArray.push(name);
        console.log(this.jobCatagoryTypeArray);
      }
      else if(!e.target.checked){
        let index =this.jobCatagoryTypeArray.indexOf(value);
        this.jobCatagoryTypeArray.splice(index,1);
        this.jobNameArray.splice(index,1);
        console.log(this.jobCatagoryTypeArray);
      }

    }    

    workTypeFilterApply(value:any){
      if (value == 'inperson') {
        if (this.inperson) {
          this.inperson = false;
        } else {
          this.inperson = true;
        }
      }
      if (value == 'online') {
        if (this.online) {
          this.online = false;
        } else {
          this.online = true;
        }
      }  
      if (this.inperson && this.online) {
        this.workType = ['inperson','online'];
      } else if(this.inperson) {
        this.workType = ['inperson'];
      } 
      else if(this.online) {
        this.workType = ['online'];
      } else {
        this.workType = []; 
      }           
      // this.jobType=value;  
      this.jobCatagoryList = [];
      if (!this.isCategorySearch) {
        this.doGetJobListbyJobtypeFilters();
      } else {
        this.doGetJobListbyCatFilter();
      } 
    }

 

    addTypeArray(value:any){
      this.jobType=value;
    }

    //Go to jog details page
    // doGotoJobDetailsPage(jobid){
    // this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobid }});
    // } 

    doGotoJobDetailsPage(job: any,i: any){
      this.popUpJobDetails = job;
      this.popUpi = i;
    } 
    
    closeJob(jobId: any, i: any) {
      if (!this.loggedIn) {
        jQuery("[data-target='#myModal_login']").click();
      } else {
        this.jobCatagoryList.splice(i,1);
        this.apiService.closeJob(jobId,localStorage.user_id)
        .subscribe(
          (data: any) => {
            let el: HTMLElement = this.myDivPopUp.nativeElement;
            el.click();
            // this.jobCatagory = data.data.category;
            // console.log(this.jobCatagory);
            this.titlemessage = 'Closed job successfully';
            this.showToasterSuccess(); 
          },
          error => {
            console.log(error);
          }
        );        
      }
    }
    
    acceptJob(jobId: any, i: any) {
      if (!this.loggedIn) {
        
        jQuery("[data-target='#myModal_login']").click();
      } else {
        if (this.jobCatagoryList[i].job_intimation[0]?.status == null || typeof this.jobCatagoryList[i].job_intimation[0] === 'undefined') {
          if(typeof this.jobCatagoryList[i].job_intimation[0] === 'undefined')    {
            this.jobCatagoryList[i].job_intimation[0] = [];
          }           
           this.jobCatagoryList[i].job_intimation[0].status = 'initiated';
          this.apiService.acceptJob(jobId,localStorage.user_id)
          .subscribe(
            (data: any) => {
              this.jobCatagory = data.data.category;
               // console.log("job",this.jobCatagory);
               this.titlemessage = 'Job applied successfully';
               this.showToasterSuccess(); 
               let el: HTMLElement = this.myDivPopUp.nativeElement;
               el.click();
            },
            error => {
              console.log(error);
            }
          );          
        }
      }
    }    

    addToWishList(jobId: any, i: any) {
      let is_favourite: string;
      if (!this.loggedIn) {
        jQuery("[data-target='#myModal_login']").click();
      } else {
        if (this.jobCatagoryList[i].job_intimation[0]?.is_jobbers_favourite == 'favourite') {
          if(typeof this.jobCatagoryList[i].job_intimation[0] === 'undefined')    {
            this.jobCatagoryList[i].job_intimation[0] = [];
          }           
          this.jobCatagoryList[i].job_intimation[0].is_jobbers_favourite = 'unfavourite';
          is_favourite = 'unfavourite';
        } else {
          if(typeof this.jobCatagoryList[i].job_intimation[0] === 'undefined')    {
            this.jobCatagoryList[i].job_intimation[0] = [];
          }          
          this.jobCatagoryList[i].job_intimation[0].is_jobbers_favourite = 'favourite';          
          is_favourite = 'favourite';
        }
        this.popUpJobDetails = this.jobCatagoryList[i];
        let el: HTMLElement = this.myDivPopUp.nativeElement;
        el.click();
        this.apiService.addToWishList(jobId,localStorage.user_id,is_favourite)
        .subscribe(
          (data: any) => {
            if (is_favourite == 'unfavourite') {
              this.titlemessage = 'Removed from favourites successfully';
              this.showToasterSuccess(); 
            }
            if (is_favourite == 'favourite') {
              this.titlemessage = 'Added to favourites successfully';
              this.showToasterSuccess(); 
            }
          },
          error => {
            console.log(error);
          }
        ); 
      }
    }

    categoryListConfirmation(jobId: any, count: any,clickStausType: any,currentStatus:any) {


      if ( clickStausType == 'close' || clickStausType == 'like' || currentStatus != 'initiated' ) {

        localStorage.setItem('jobId', jobId);
        localStorage.setItem('count', count);
        localStorage.setItem('clickStausType', clickStausType);
        if (this.loggedIn) { 
          clickStausType == 'close' ? jQuery('#confirmationText').html("Do you want to close?") : jQuery('#confirmationText').html("Do you want to accept?");
          if(clickStausType == 'like'){
            if(this.jobCatagoryList[count].job_intimation[0]?.is_jobbers_favourite == 'favourite'){
              jQuery('#confirmationText').html("Do you want to remove this item from favourits?")
            }
            else{
              jQuery('#confirmationText').html("Do you want to add this item to favourits?")
    
            }
            
          }
        } else {
          if (clickStausType == 'accept') {
            this.dynamicLoginMessage = 'In order to apply this lite job you need to Log In / Sign Up';
          }
          if (clickStausType == 'close') {
            this.dynamicLoginMessage = 'In order to close this lite job you need to Log In / Sign Up';          
          }
     
        //jQuery('#confirmationPopup').modal('show');
        jQuery("#mymodalsignupmain").click(); 
        }

      }
    }

    categoryConfirmationListener(value:any) {
      this.jobIdReceived = localStorage.getItem('jobId');
      this.countReceived = localStorage.getItem('count');
      this.clickTypeStatusReceived = localStorage.getItem('clickStausType');
      jQuery('#confirmationPopup').modal('hide');

      if (this.clickTypeStatusReceived == 'accept') {
        this.acceptJob(this.jobIdReceived,this.countReceived)
      }
      else if (this.clickTypeStatusReceived == 'close') {
        this.closeJob(this.jobIdReceived,this.countReceived);
      }
      else if(this.clickTypeStatusReceived == 'like') {
        this.addToWishList(this.jobIdReceived,this.countReceived);
      }
      localStorage.removeItem('jobId');
      localStorage.removeItem('count');
      localStorage.removeItem('clickStausType');
        
    }
    

    checkUncheckAll(e:any){
      
      for (var i = 0; i < this.jobCatagory.length; i++) {
        this.jobCatagory[i].isSelected = this.isChecked;
      }
       if(e.target.checked){
        this.jobCatagoryArray=[];
        for (var i = 0; i < this.jobCatagory.length; i++) {
          this.jobCatagoryArray.push(this.jobCatagory[i].id);
        } 
        this.jobTypesList(this.jobCatagoryArray); 
        this.jobTypeAutoCheck(null,null);
      }
      else if(!e.target.checked){
        this.jobTypesByCat = [];
        this.jobCatagoryArray = [];

        this.jobNameArray=[];
      this.jobCatagoryTypeArray=[];

      }
    
    }

    initialJobStart()
    {
      if(this.jobCategoryName == undefined){
        this.isChecked = true;
        this.jobCatagoryArray = [];
        for (var i = 0; i < this.jobCatagory.length; i++) {
          this.jobCatagory[i].isSelected = true;
          this.jobCatagoryArray.push(this.jobCatagory[i].id);
        }    

      }else{
        this.isChecked = false;
        this.jobCatagoryArray = [];

        for (var i = 0; i < this.jobCatagory.length; i++) {
          if(this.jobCategoryName == this.jobCatagory[i].details[0].job_cate_id){
            this.jobCatagory[i].isSelected = true;
            this.jobCatagoryArray.push(this.jobCatagory[i].details[0].job_cate_id);
          }
        }    
        
        // this.jobCatagory[i].isSelected = true;
        // this.jobCatagoryArray.push(this.jobCategoryName);

      }
      
      this.jobTypesList(this.jobCatagoryArray); 
    }

  array_unique($a: any) {
      return [...new Set($a)];
  }
  

    jobTypeAutoCheck(clickType: any, clickData: any)
    {
      let getJobCategoryKey : any;
      let getEachJobCategoryKey : any;
      let readData : any;

      

      this.apiService.jobTypesList(clickType == "single" ? clickData : this.jobCategoryName)
      .subscribe(
        (data: any) => {
          readData = data.data.jobtypes; 
         
          
      this.jobNameArray=[];
      this.jobCatagoryTypeArray=[];

      

      getJobCategoryKey = Object.keys(readData);

        for(let i=0;i<getJobCategoryKey.length;i++){

          getEachJobCategoryKey=readData[getJobCategoryKey[i]];

          for(let j=0;j<getEachJobCategoryKey.length;j++)
          {
            this.jobNameArray.push(readData[getJobCategoryKey[i]][j].lang[0].name); 
            this.jobCatagoryTypeArray.push(readData[getJobCategoryKey[i]][j].lang[0].id);
            this.allJobTypes.push(readData[getJobCategoryKey[i]][j].lang[0].name);
            this.jobNameArray = this.array_unique(this.jobNameArray);
            this.jobCatagoryTypeArray = this.array_unique(this.jobCatagoryTypeArray);
            this.allJobTypes = this.array_unique(this.allJobTypes);                        
          }
          
        }      
        },
        error => {
          console.log(error);
        }
      );

    }
    doGetJobListbySearch(type: string) {

      this.apiService.doGetJobListbySearch(localStorage.user_id,this.searchPage,this.workType,this.searchName,type)
      .subscribe(
        (data: any) => {
                   
          // this.jobCatagoryList = data.data.resp;
          this.jobCatagoryList = this.jobCatagoryList.concat(data.data.resp);
          this.jobCatagoryList = this.uniqueArray2(this.jobCatagoryList);
          console.log("searchcat",this.jobCatagoryList);
          this.totalCount = data.data.count;    
         
          this.checkCurrentCount = data.data.count / environment.search_limit; 
          if (this.start == 1) {
            this.previous = 0;
          }           
          if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount > 1) {
            this.currentCount = environment.page_counts;
            if (this.checkCurrentCount > environment.page_counts) {
              this.next = this.currentCount + 1;
            }
          } else if (this.start == 1 && this.start == this.searchPage && this.checkCurrentCount <= 1){
            this.currentCount = this.checkCurrentCount;
          }

          if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount > 1) {
            this.currentCount = environment.page_counts;
            if (this.checkCurrentCount > environment.page_counts) {
              this.next = this.currentCount + 1;
            }             
          } else if (this.start == 1 && this.start !== this.searchPage && this.checkCurrentCount <= 1){
            this.currentCount = this.checkCurrentCount;
          }         

          if (this.start > 1 && this.start == this.searchPage) {
            this.previous = this.start - 1;
            this.currentCount = this.start + (environment.page_counts - 1);
            if (this.currentCount >= data.data.count) {
              this.currentCount = data.data.count;
              this.next = 0;
            }

          }

          if (this.start > 1 && this.start !== this.searchPage) {
            this.currentCount = this.start + (environment.page_counts - 1);
            if (this.currentCount >= data.data.count) {
              this.currentCount = data.data.count;
            }
          }
          if (this.currentCount < 1) {
            this.currentCount = 1;
          }
          this.currentNext = this.currentCount + 1;
          this.currentPrevious = this.start - 1;
         
          this.totalResults = this.range1(this.currentCount, this.start);  
          this.jobCatagoryList.forEach((value: any, key: any) => {
    
          let result = value['cat_image_path'].includes("uploads/job_category/");
          if (!result) {
            value['cat_image_path'] = environment.server_url+'uploads/job_category/' +value['cat_image_path'];
          }
            // if ((this.jobCatagoryList[key]['jobpost_photos'].length == 0)) {
            //   this.jobCatagoryList[key]['jobpost_photos'][0] = [];
            //   this.jobCatagoryList[key]['jobpost_photos'][0].image_path = 'assets/images/Default_Image_Thumbnail.jpg';
            // } else {
            //   value['jobpost_photos'][0]['image_path'] = environment.server_url+'' +value['jobpost_photos'][0]['image_path'];                
            // }
          });                    
        },
        error => {
          console.log(error);
        }
      );
    }


    homeConfirmation(jobId: any, count: any,clickStausType:any,currentStatus:any, currentLocation:any) {
      let dataStorage: any = [jobId,count,clickStausType,currentStatus];
  
      if (currentStatus != 'initiated' || clickStausType == 'close' || clickStausType == 'like' || clickStausType == 'accept') {
  
        this.jobId = jobId;
        this.count = count;
        this.status = clickStausType;
        clickStausType == 'close' ? jQuery('#confirmationHomeText').html("Do you want to close?") : jQuery('#confirmationHomeText').html("Do you want to accept?");
        if(clickStausType == 'like'){
          if(this.jobCatagoryList[count].job_intimation[0]?.is_jobbers_favourite == 'favourite'){
            jQuery('#confirmationHomeText').html("Do you want to remove this item from favourits?")
          }
          else{
            jQuery('#confirmationHomeText').html("Do you want to add this item to favourits?")
  
          }  
        } 
        let el: HTMLElement = this.myDivPopUp.nativeElement;
        el.click();
        if (this.loggedIn) {      
          if (clickStausType == 'like')   {
            this.addToWishList(this.jobId,this.count);
          } else {
            if (currentStatus == null) {
              var data = {
                "customers_id" : localStorage.getItem('user_id')
              }
              this.jobber.jobberStepThreeDetails(data).subscribe((res) => {
                if (res.data.workinfo.length == 0) {
                  jQuery('#confirmationProfileJobberatorHomePopup').modal('show');
                } else {
                  if (res.data.workinfo[0].tab2 == '1') {
                    jQuery('#confirmationHomePopup').modal('show');  
                  } else {
                    jQuery('#confirmationProfileHomePopup').modal('show');
                  }                
                }
  
              }, error => {
                //this.redirectTo('/');
              }
              );
           
            } else {
              jQuery('#confirmationHomePopup').modal('show');
            }
            // jQuery('#confirmationHomePopup').modal('show');
          }
        } else {
          localStorage.setItem("jobDetailsjobId",jobId);
          if (clickStausType == 'accept') {
            this.dynamicLoginMessage = 'In order to apply this lite job you need to Log In / Sign Up';
          }
          if (clickStausType == 'close') {
            this.dynamicLoginMessage = 'In order to close this lite job you need to Log In / Sign Up';          
          }
          if (clickStausType == 'like') {
            this.dynamicLoginMessage = 'In order to shortlist/taken off from shortlist this lite job you need to Log In / Sign Up';      
          }    
         
          jQuery("#mymodalsignupmain").click();     
        }
      }
    }

    homeConfirmationClick() {
      // if (!this.loggedIn) {
      //   jQuery("#mymodalsignup").click();
      // } else {
  
        jQuery('#confirmationHomePopup').modal('hide');
  
        if (this.status == 'accept') {
          this.acceptJob(this.jobId,this.count)
        }
        else if (this.status == 'close') {
          this.closeJob(this.jobId,this.count);
        }
        else if(this.status  == 'like') {
          this.addToWishList(this.jobId,this.count);
        }
  //    }    
    }

    homeProfileConfirmationClick() {
      // jQuery('.modal-backdrop').remove(); 
      jQuery('#confirmationProfileHomePopup').modal('hide');
      this.redirectTo('/jobberprofile');   
    }

    redirectTo(uri:string){
      //this.childModal.hide();
      this.router.navigate([uri]);
      return false;    
    }
    
}
