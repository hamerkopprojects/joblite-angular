import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkedinloginComponent } from './linkedinlogin.component';

describe('LinkedinloginComponent', () => {
  let component: LinkedinloginComponent;
  let fixture: ComponentFixture<LinkedinloginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LinkedinloginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkedinloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
