import { Component, OnInit } from '@angular/core';
import { ApiService } from '../_services/commonlisting/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from './../../environments/environment';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { UserService} from './../_services/user/user.service';
import { JobberprofileService } from './../_services/jobberprofile/jobberprofile.service';
import { NotificationService } from './../_services/notification.service';
import { NgxSpinnerService } from "ngx-spinner"; 

@Component({
  selector: 'app-linkedinlogin',
  templateUrl: './linkedinlogin.component.html',
  styleUrls: ['./linkedinlogin.component.css']
})
export class LinkedinloginComponent implements OnInit {
  linkedInToken = "";
  
  signup3form = new FormGroup({
    optradio: new FormControl('', [Validators.required]),
    signup_checkform: new FormControl('', [Validators.required])
  });

  titlemessage: string;
  signupStep2: boolean;
  firstName: any;
  lastName: any;
  name: any;
  email: string;
  user_type: any;
  profile_img: any;
  errorMessage: string;
  backedPage: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private user: UserService,
    private jobber: JobberprofileService,
    private notifyService : NotificationService, 
    private SpinnerService: NgxSpinnerService,
    ) {}
  
  ngOnInit() {
    this.titlemessage = '';
    this.backedPage = localStorage.getItem('linked_verify_back_page');
    this.signupStep2 = false;
    this.linkedInToken = this.route.snapshot.queryParams["code"];
    //alert(this.linkedInToken);
    let dataver = {
      "grant_type" : "authorization_code",
      "code" : this.linkedInToken,
      "redirect_uri" : environment.callback_url +'linkedinlogin',
      "client_id" : "77gg45kc43nbzy",
      "client_secret" : "UPRs45kxztCDzjfU", 
    } 

    this.apiService.getAccessToken(dataver)
    .subscribe(
      (data: any) => {
        console.log(data);
        let dataprofile = {
          "token" : data.data.access_token,
        } 
        this.apiService.getLinkedInProfile(dataprofile)
        .subscribe(
          (data: any) => {
            console.log(data);
            this.firstName = data.data.localizedFirstName;
            this.lastName = data.data.localizedLastName;
            this.name = this.firstName +''+ this.lastName+''+data.data.id;
            // this.email = data.data.id +'@linkedin.com';
            this.email = data.emaildata.elements[0]['handle~'].emailAddress;
            this.user_type = 'linkedin';
            this.profile_img = data.photodata.profilePicture['displayImage~'].elements[0].identifiers[0].identifier;
            if (this.backedPage == 'jobberprofile' || this.backedPage == 'profile') {
              if (localStorage.getItem('user_email') == this.email) {
                let dataver = {
                    "customerId" : localStorage.getItem('user_id'),
                    "username"  : this.profile_img            
                  }         
                  this.jobber.lnverify(dataver).subscribe((resver) => {
                    console.log(resver);  
                    localStorage.removeItem('linked_verify_back_page');
                    this.titlemessage = 'Verified linkedin account successfully';
                    this.showToasterSuccess(); 
                    this.redirectTo('/'+this.backedPage);  
                    //this.jobberatorStepTwoDetails();             
          
                  }, error => {
                    alert(error.Error);
                    this.SpinnerService.hide();
                  }
                  ); 
              } else {
                this.titlemessage = 'Your email is not matching';
                localStorage.removeItem('linked_verify_back_page');
                this.showToasterError(); 
                this.redirectTo('/'+this.backedPage);        
              }

            } else {
              var datapass = {
                "email" : this.email,
                "location" : localStorage.getItem('maplocation'),
                "loc_coordinates" : localStorage.getItem('loccoordinates')
              }
                // this.orderdata
                this.user.emailCheckAuto(datapass).subscribe((res)=>{
                  this.errorMessage = '';
                  //this.signupStep2 = true;
                  localStorage.setItem('linkedInfirstName',this.firstName);
                  localStorage.setItem('linkedInlastName',this.lastName);
                  localStorage.setItem('linkedInName',this.name);
                  localStorage.setItem('linkedInEmail',this.email);
                  localStorage.setItem('linkedInUserType',this.user_type);
                  localStorage.setItem('linkedInProfileImg',this.profile_img);
                  localStorage.setItem('user_type_click',this.user_type)
                  this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
                  this.SpinnerService.hide();  
                  //localStorage.removeItem('loccoordinates');        
                },Error =>{
                  this.errorMessage = '';          
                  this.social_login_step();
                })
            }
          },
          error => {
            this.signupStep2 = false;
            console.log(error.message);
          }
        );
      },
      error => {
        console.log(error.message);
      }
    );
  }

  signup_step3() {

    if(this.signup3form.valid){
      var data = {
        "first_name" : this.firstName,
        "last_name" : this.lastName,
        "username" : this.name,
        "email" : this.email,
        "user_type" : this.signup3form.value.optradio,
        "customer_type" : this.user_type,
        "profile_img" : this.profile_img,
        "location" : localStorage.getItem('maplocation'),
        "loc_coordinates" : localStorage.getItem('loccoordinates')
      }
      // this.orderdata
      this.user.createCustomerSocial(data).subscribe((res)=>{
        //this.redirectTo('/usersuccess');
        this.social_login_step();
      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
    }
  } 

  social_login_step() {
    this.signupStep2 = false;

      var data = {
        "email" : this.email,
        "user_type" : this.user_type
      }
      // this.orderdata
      this.user.sociallogin(data).subscribe((res)=>{
        localStorage.setItem('token', res.data.access_token);
        localStorage.setItem('user_id', res.data.user.id);
        localStorage.setItem('user_email', res.data.user.email);
        localStorage.setItem('user_type', res.data.user.type);

              if (res.data.user.type == 'jobber') {
                //this.redirectTo('/myjobberjobs');
                this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
              } else {
                //this.redirectTo('/myjobs');
                this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
              }  

      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
  }

  showToasterSuccess(){
    this.notifyService.showSuccess(this.titlemessage, "")
  }
  
  showToasterError(){
      this.notifyService.showError(this.titlemessage, "")
  }
  
  showToasterInfo(){
      this.notifyService.showInfo(this.titlemessage, "")
  }
  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;
  } 

}
