import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService} from './../_services/user/user.service'; 
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.css']
})
export class VerifyComponent implements OnInit {
  code: string;
  errorMessage: string;
  form = new FormGroup({
    otp: new FormControl('', [Validators.required])
  });
  constructor(private router: Router,
    private route: ActivatedRoute,
    private user: UserService,
    private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.code = this.route.snapshot.params.code;
    this.errorMessage = '';    
  }

  verify() {
    if(this.form.valid){
      var data = {
        "encrypt_otp" : this.code,
        "otp" : this.form.value.otp
      }
      // this.orderdata
      this.user.verify(data).subscribe((res)=>{
        //console.log(res.data.access_token);
        localStorage.setItem('token', res.data.access_token);
        localStorage.setItem('user_id', res.data.user.id);
        localStorage.setItem('user_type', res.data.user.type);
        if (res.data.user.type == "both" || res.data.user.type == "jobber") {
          this.redirectTo('/jobberprofile');
        } else {
          this.redirectTo('/profile');        
        }
      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
    }
  }

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
  }

}
