import { Component, OnInit } from '@angular/core';
import { ApiService } from '../_services/commonlisting/api.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-jobcategories',
  templateUrl: './jobcategories.component.html',
  styleUrls: ['./jobcategories.component.css']
})
export class JobcategoriesComponent implements OnInit {
  jobCatagoryList=[];
  loggedIn: boolean;
  constructor(
    private router: Router,
    private apiService: ApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    if(typeof localStorage.token !== 'undefined') {
      this.loggedIn = true;
    } else {
      this.loggedIn = false;      
    }    
    this.doGetJobListbyCategory();
  }

     //Go to jog details page
     doGotoJobDetailsPage(catName:string){
       console.log("catname",catName)
      this.router.navigate(['/jobcategory-listing'],{ queryParams: { category: catName }});
     }   

         //Get jobdeatails
    doGetJobListbyCategory() {
      this.apiService.doGetJobListbyCategory()
        .subscribe(
          (data: any) => {
            this.jobCatagoryList = data.data.category;
            console.log("jobcat",this.jobCatagoryList);
          },
          error => {
            console.log(error);
          }
        );
        }
}
