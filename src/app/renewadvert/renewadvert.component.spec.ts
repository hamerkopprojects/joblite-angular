import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewadvertComponent } from './renewadvert.component';

describe('ScopeincreaseComponent', () => {
  let component: RenewadvertComponent;
  let fixture: ComponentFixture<RenewadvertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenewadvertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewadvertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
