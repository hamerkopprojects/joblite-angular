import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaycallbackjobstartComponent } from './paycallbackjobstart.component';

describe('PaycallbackjobstartComponent', () => {
  let component: PaycallbackjobstartComponent;
  let fixture: ComponentFixture<PaycallbackjobstartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaycallbackjobstartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaycallbackjobstartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
