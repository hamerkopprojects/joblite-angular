/**
 * Interface for login
 */
export interface ILogin {
    username: string;
    password: string;
}
