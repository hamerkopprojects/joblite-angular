/**
 * Interface for Loader
 */
export interface ILoaderState {
    show: boolean;
}
