import { Component, OnInit } from '@angular/core';
import { ApiService } from './../_services/commonlisting/api.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from "@angular/router";
declare var jQuery:any;
@Component({
  selector: 'app-jobdetails',
  templateUrl: './jobdetails.component.html',
  styleUrls: ['./jobdetails.component.css']
})
export class JobdetailsComponent implements OnInit {
  loggedIn: boolean;
  jobpost_id:any;
  dynamicLoginMessage = '';  
  status:any;   
  data :any;
  ratingdata :any;
  jobCatagory =[];
  jobTask =[];
  constructor(
    private router: Router,
    private apiService: ApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    jQuery('.modal').modal('hide');
    if(typeof localStorage.token !== 'undefined') {
      this.loggedIn = true;
    } else {
      this.loggedIn = false;      
    }     
    this.route.queryParams.subscribe(params => {
      this.jobpost_id = params['job_id'];
      console.log("id",this.jobpost_id)

      if (this.jobpost_id !== undefined) {
        // this.router.navigate(['/pending-orders']);
      }
      else{
      this.router.navigate(['/Home']);
      }
    });
    this.dynamicLoginMessage = '';     

    this.doGetJobDetails();
    this.ratingDetails();

  }

  

      //Get jobdeatails
      ratingDetails() {
        this.apiService.ratingDetailsJobs(this.jobpost_id)
        .subscribe(
          (data: any) => {
            this.ratingdata= data.rating;
            this.ratingdata.forEach((value: any, key: any) => {
              value.customer.profile_img = environment.server_url + value.customer.profile_img;
            });             
          },
          error => {
            console.log(error);
          }
        );
      }

      //Get jobdeatails
      doGetJobDetails() {
        this.apiService.doGetJobDetails(this.jobpost_id)
        .subscribe(
          (data: any) => {
            this.data=data.jobpostInfo;
            console.log("data",this.data)
            this.jobCatagory = this.data.jobpost_job_category[0].job_category.details;
            console.log("jobcat",this.jobCatagory)
            this.jobTask = this.data.jobpost_task
            console.log("jobtask",this.jobTask);
          },
          error => {
            console.log(error);
          }
        );
      }

      homeConfirmation(clickStausType:any,currentStatus:any) {
    
        if (currentStatus != 'initiated' || clickStausType == 'close' || clickStausType == 'like') {
          // this.count = count;
          this.status = clickStausType;
    
          clickStausType == 'close' ? jQuery('#confirmationHomeText').html("Do you want to close?") : jQuery('#confirmationHomeText').html("Do you want to accept?");
          if(clickStausType == 'like'){
            if(this.data.job_intimation[0]?.is_jobbers_favourite == 'favourite'){
              jQuery('#confirmationHomeText').html("Do you want to remove this item from favourits?")
            }
            else{
              jQuery('#confirmationHomeText').html("Do you want to add this item to favourits?")
    
            }  
          } 
          if (this.loggedIn) {      
            jQuery('#confirmationHomePopup').modal('show');
          } else {
            localStorage.setItem("jobDetailsjobId",this.jobpost_id);
            if (clickStausType == 'accept') {
              this.dynamicLoginMessage = 'In order to apply this lite job you need to Log In / Sign Up';
            }
            if (clickStausType == 'close') {
              this.dynamicLoginMessage = 'In order to close this lite job you need to Log In / Sign Up';          
            }
            if (clickStausType == 'like') {
              this.dynamicLoginMessage = 'In order to shortlist/taken off from shortlist this lite job you need to Log In / Sign Up';      
            }                
            jQuery("#mymodalsignup").click();     
          }
        }
      } 
      
      homeConfirmationClick() {
        // if (!this.loggedIn) {
        //   jQuery("#mymodalsignup").click();
        // } else {
          jQuery('#confirmationHomePopup').modal('hide');
    
          if (this.status == 'accept') {
            this.acceptJob()
          }
          else if (this.status == 'close') {
            this.closeJob();
          }
          else if(this.status  == 'like') {
            this.addToWishList();
          }
    //    }    
      }   

      redirectTo(uri:string){
        // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
        // this.router.navigate([uri]));
        //this.childModal.hide();
        this.router.navigate([uri]);
        return false;
      }       
      
      closeJob() {
          this.apiService.closeJob(this.jobpost_id,localStorage.user_id)
          .subscribe(
            (data: any) => {
              // this.jobCatagory = data.data.category;
              // console.log(this.jobCatagory);
              //jobcategory-listing
              this.redirectTo('/jobcategory-listing');
            },
            error => {
              console.log(error);
            }
          );        
       // }
      }
      
      acceptJob() {
        // if (!this.loggedIn) {
        //   jQuery("#mymodalsignup").click();
        // } else {
          if (this.data.job_intimation[0]?.status == null || typeof this.data.job_intimation[0] === 'undefined') {
            if(typeof this.data.job_intimation[0] === 'undefined')    {
              this.data.job_intimation[0] = [];
            }           
            this.data.job_intimation[0].status = 'initiated';
            this.apiService.acceptJob(this.jobpost_id,localStorage.user_id)
            .subscribe(
              (data: any) => {
                //this.jobCatagory = data.data.category;
                 //console.log(this.jobCatagory);
              },
              error => {
                console.log(error);
              }
            );          
          }
    //    }
      }    
    
      addToWishList() {
        let is_favourite: string;
        // if (!this.loggedIn) {
        //   jQuery("#mymodalsignup").click();
        // } else {
          if (this.data.job_intimation[0]?.is_jobbers_favourite == 'favourite') {
            if(typeof this.data.job_intimation[0] === 'undefined')    {
              this.data.job_intimation[0] = [];
            }           
            this.data.job_intimation[0].is_jobbers_favourite = 'unfavourite';
            is_favourite = 'unfavourite';
          } else {
            if(typeof this.data.job_intimation[0] === 'undefined')    {
              this.data.job_intimation[0] = [];
            }          
            this.data.job_intimation[0].is_jobbers_favourite = 'favourite';          
            is_favourite = 'favourite';
          }
          this.apiService.addToWishList(this.jobpost_id,localStorage.user_id,is_favourite)
          .subscribe(
            (data: any) => {
            },
            error => {
              console.log(error);
            }
          ); 
      //  }
      }   
      
      changeDynamicLoginMessage() {
        this.dynamicLoginMessage = '';
      }      
}
