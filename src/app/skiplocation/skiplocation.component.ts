import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
declare var jQuery:any;

@Component({
  selector: 'app-skiplocation',
  templateUrl: './skiplocation.component.html',
  styleUrls: ['./skiplocation.component.css']
})
export class SkiplocationComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
   
    this.redirectTo('/')
    jQuery('.modal').modal('hide');
  }

  redirectTo(uri: string) {
    this.router.navigate([uri]);
    return false;
  }

}
