import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkiplocationComponent } from './skiplocation.component';

describe('SkiplocationComponent', () => {
  let component: SkiplocationComponent;
  let fixture: ComponentFixture<SkiplocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkiplocationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkiplocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
