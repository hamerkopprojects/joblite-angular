import { Component, OnInit } from '@angular/core';
import { JobberatorprofileService } from './../_services/jobberatorprofile/jobberatorprofile.service';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../_services/commonlisting/api.service';



@Component({
  selector: 'app-personelinfo',
  templateUrl: './personelinfo.component.html',
  styleUrls: ['./personelinfo.component.css']
})
export class PersonelinfoComponent implements OnInit {

  respbody: any;
  username: any;
  personelinfo: any;
  gender: any;
  location: any;
  jobcategory: any;
  image: any;
  jobinterest: any = [];
  jobinterest1: any;
  namearray: any = [];
  proficiency: any = [];
  userType: string;
  userName: string;
  viewType: any;
  isJobber: any;
  personelProfile: any;
  age: any; 
  ratingdata :any;
  langdata: any;
  edudata: any;
  avgrating : any;
  roundedavg: any;
  

  constructor(private jobberator: JobberatorprofileService,
    private router: Router,
    private apiService: ApiService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.langdata = [];
    this.edudata = [];
    this.jobberatorStepOneDetails();
    this.jobberatorJobCategoryDetails();
    this.ratingDetails();
    this.languageDetails();
    this.userLangs();
    this.userEdus();
  }


    //Get user education qualifications
    userEdus() {
      this.apiService.eduDetailsCustomer(localStorage.getItem('user_id'))
      .subscribe(
        (data: any) => {
          this.edudata = data.edus;
        },
        error => {
          console.log(error);
        }
      );
    }


    //Get user languages
    userLangs() {
      this.apiService.languageDetailsCustomer(localStorage.getItem('user_id'))
      .subscribe(
        (data: any) => {
          this.langdata = data.langs;
          this.langdata.forEach((value: any, key: any) => {
            if (value.prefs[0].name == 'Basic') {
              this.langdata[key].prefs[0].weight = 2;
            } 
            if (value.prefs[0].name == 'Intermediate') {
              this.langdata[key].prefs[0].weight = 4;
            } 
            if (value.prefs[0].name == 'Expert') {
              this.langdata[key].prefs[0].weight = 6;
            } 
            if (value.prefs[0].name == 'Strong') {
              this.langdata[key].prefs[0].weight = 8;
            } 
            if (value.prefs[0].name == 'Advanced') {
              this.langdata[key].prefs[0].weight = 10;
            } 
          }); 
        },
        error => {
          console.log(error);
        }
      );
    }

  createRange(number: any){
    if (isNaN(number)) {
      number = 0;
    } 
    return new Array(number);
  } 

    //Get language details
    languageDetails() {
      this.avgrating = 0;
      let count = 0;
      this.apiService.ratingDetailsCustomer(localStorage.getItem('user_id'))
      .subscribe(
        (data: any) => {
          this.ratingdata = data.rating;
          this.ratingdata.forEach((value: any, key: any) => {
            value.customer.profile_img = environment.server_url + value.customer.profile_img;
            count = count + value.rating;
          }); 
          if (this.ratingdata.length == 0) {
            this.avgrating = 0; 
            this.roundedavg  = 0; 
          } else {
            this.avgrating = count / this.ratingdata.length; 
            this.roundedavg  = Math.round(this.avgrating); 
          }
        },
        error => {
          console.log(error);
        }
      );
    }

    //Get jobdeatails
    ratingDetails() {
      this.avgrating = 0;
      let count = 0;
      this.apiService.ratingDetailsCustomer(localStorage.getItem('user_id'))
      .subscribe(
        (data: any) => {
          this.ratingdata = data.rating;
          this.ratingdata.forEach((value: any, key: any) => {
            value.customer.profile_img = environment.server_url + value.customer.profile_img;
            count = count + value.rating;
          }); 
          if (this.ratingdata.length == 0) {
            this.avgrating = 0; 
            this.roundedavg  = 0; 
          } else {
            this.avgrating = count / this.ratingdata.length; 
            this.roundedavg  = Math.round(this.avgrating); 
          }
        },
        error => {
          console.log(error);
        }
      );
    }

  changeUserRole(type: any) {
    this.viewType = type;
  }

  jobberatorStepOneDetails() {
    var data = {
      "customers_id": localStorage.getItem('user_id')
    }
    this.jobberator.jobberatorStepOneDetails(data).subscribe((res) => {
      this.personelProfile =  res.data.customerInfo

      this.username =this.personelProfile.username;
      this.personelinfo =this.personelProfile.personal_info;
      this.gender = this.personelProfile.gender;
      this.location = this.personelProfile.location;
      if(this.personelProfile.profile_img != null && this.personelProfile.profile_img != '' && this.personelProfile.profile_img != 'null')
      {
        // this.image = environment.server_url + this.personelProfile.profile_img;
        if (this.personelProfile.user_type == 'normal' || this.personelProfile.profile_img.includes('uploads/user_profile/')) {
          this.image = environment.server_url + this.personelProfile.profile_img;  
        } else {
          let x = Math.floor((Math.random() * 10000) + 1);
          // Remove backslashes from the image URL
          if (this.personelProfile.user_type == 'linkedin' || this.personelProfile.user_type == 'facebook') {
            this.image = this.personelProfile.profile_img.replace(/\//g, '/');
          } else {
            this.image = this.personelProfile.profile_img+'?rand='+x;
          }
        }
      } else {
        this.image = 'assets/images/image_upload.png';
      }
      this.userType = this.personelProfile.type;
      if (this.userType != 'both') {
        this.viewType = this.userType; 
      } else {
        this.viewType = 'jobber';         
      }
      this.age = res.data.customerInfo.age;



    }, error => {
      //this.redirectTo('/');
    }
    );

  }

  jobberatorJobCategoryDetails() {
    var data = {
      "customers_id": localStorage.getItem('user_id')
    }
    this.jobberator.jobberatorJobCategoryDetails(data).subscribe((res) => {
      this.respbody = res;
      console.log(res.data.jobtypes.skills);

      this.proficiency = res.data.jobtypes?.skills[0]?.job_types;

      this.jobcategory = res.data.jobtypes?.skills;
      if (this.jobcategory.length > 0) {
        this.jobcategory.forEach((value: any, key: any) => {
          if (this.jobcategory[key]['job_cats']['image_path'] == null) {
            this.jobcategory[key]['job_cats']['image_path'] = 'assets/images/catagerios_06.png';
          } else {
            this.jobcategory[key]['job_cats']['image_path'] = environment.server_url+'uploads/job_category/' +this.jobcategory[key]['job_cats']['image_path'];
          }
        }); 
      }
     
      this.jobinterest = res.data.jobtypes.job_interests

      var arr = Object.entries(this.jobinterest).map(([type, value]) => ({ type, value }));
      this.jobinterest = arr;
      for (let i = 0; i < this.jobinterest.length; i++) {
        this.namearray.push(this.jobinterest[i].value[0].name)
      }
      console.log(this.namearray);
    }, error => {
      //this.redirectTo('/');
    }
    );



  }
  redirectTo(uri: string) {
    this.router.navigate([uri]);
    return false;
  }
  profileChange() {
    if (this.userType == 'both' || this.userType == 'jobber') {
      this.router.navigate(['/jobberprofile']);
      return false;
    } else {
      this.router.navigate(['/profile']);
      return false;
    }

  }


}
