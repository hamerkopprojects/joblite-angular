import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { NgxSpinnerService } from "ngx-spinner";
import { environment } from 'src/environments/environment';
import { CategoriesService} from './../_services/categories/categories.service'; 
import { UserService} from './../_services/user/user.service'; 
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../_services/commonlisting/api.service';
import { JobberatorprofileService } from './../_services/jobberatorprofile/jobberatorprofile.service';
import { JobberprofileService } from './../_services/jobberprofile/jobberprofile.service';
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { NotificationService } from './../_services/notification.service';
import { Subscription } from 'rxjs';
//import { MapsAPILoader } from '@agm/core';

declare var jQuery:any;

//import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // linkedInCredentials = {
  //   clientId: "77it7ifm3ktngm",
  //   redirectUrl: "http://localhost:4200/linkedinlogin",
  //   scope: "r_liteprofile%20r_emailaddress%20w_member_social" // To read basic user profile data and email
  // };
  linkedInCredentials = {
    clientId: "77gg45kc43nbzy",
    // redirectUrl: "https://www.joblite.co.za/linkedinlogin",
    redirectUrl: environment.callback_url +'linkedinlogin',
    // scope: "r_liteprofile%20r_emailaddress%20w_member_social" // To read basic user profile data and email
    scope: "r_liteprofile%20r_emailaddress" // To read basic user profile data and email
  };
  //@ViewChild('childModal') public childModal:ModalDirective;
  @ViewChild('myDiv') myDiv: ElementRef<HTMLElement>;
  @ViewChild('myDivPopUp') myDivPopUp: ElementRef<HTMLElement>;
  server_url: string;
  titlemessage: string;
  filterType: string; 
  bestJobsText: string;
  popUpJobDetails: any;
  popUpi: any;
  userType : string; 
  userName: string;  
  userEmail: string;
  image:any;  
  ImageUrl :any;
  getImageUrl :any;  
  dynamicLoginMessage: string;
  title = 'jcjoblite';
  respbody: any;
  categoryFirst: any;
  categoryNextFourth: any;
  categoryRemains: any;
  signupStep1: boolean;
  signupStep2: boolean;
  forgotStep1: boolean;
  forgotStep2: boolean;
  loginStep: boolean;
  verificationStep: boolean;
  errorMessage: string;
  email: string;
  jobList =[];
  jobCatagoryList=[];
  loggedIn: boolean; 
  notificationsList: any;
  jobTypenotificationsList: any;
  jobTypeFilternotificationsList: any;  
  noticount: any;
  settingsbody: any;

  jobId: any;
  count: any;
  status:any;
  postaJobStatus : any;
  location: any;
  userData: any;
  firstName: any;
  lastName: any;
  name: any;
  user_type: any;
  profile_img: any;

  formmap = new FormGroup({
    location: new FormControl('', [Validators.required])
  });

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  });
  signup2form = new FormGroup({
    first_name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    last_name: new FormControl(''),
    username: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)]),
    optradio: new FormControl('', [Validators.required]),
    signup_checkform: new FormControl('', [Validators.required])
  });
  signup3form = new FormGroup({
    optradio: new FormControl('', [Validators.required]),
    signup_checkform: new FormControl('', [Validators.required])
  });
  loginform = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  forgotform = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  });

  forgotpassform = new FormGroup({
    otp: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  otpform = new FormGroup({
    otp: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  searchName: any;
  viewJoblistStatus: any;

  maplocation: any;
  loccoordinates: string;
  locErrorMessage: string;
  center: any;
  defaultBounds: any;
  input: any;
  mapoptions: any;
  autocomplete: any;
  isPost: string;
  private authSubscription: Subscription;

  constructor(private SpinnerService: NgxSpinnerService,
    private jobberator: JobberatorprofileService,     
    private category: CategoriesService,
    private user: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private authService: SocialAuthService,
    //private mapsAPILoader: MapsAPILoader,
    private notifyService : NotificationService,
    private jobber: JobberprofileService,
    private http: HttpClient) { }

  ngOnDestroy(): void {
    // Unsubscribe from the subscription to prevent memory leaks
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }
  ngOnInit() { 
    this.forgotStep1 = false;
    this.forgotStep2 = false;
    // if (typeof localStorage.user_type_click !== 'undefined') {
    //   this.user_type = localStorage.getItem('user_type_click');
    // } else {
      this.user_type = '';
    // }
    // console.log(this.user_type);

    this.server_url = environment.server_url;
    this.maplocation = '';
    if(typeof localStorage.maplocation !== 'undefined') {
      this.maplocation = localStorage.getItem('maplocation');
    }

    this.isPost = '';
    this.locErrorMessage = '';
    this.center = { lat: -26.195246, lng: 28.034088 };
    // Create a bounding box with sides ~10km away from the center point
    this.defaultBounds = {
      north: this.center.lat + 0.1,
      south: this.center.lat - 0.1,
      east: this.center.lng + 0.1,
      west: this.center.lng - 0.1,
    };
    this.input = document.getElementById("pac-input") as HTMLInputElement;
    this.mapoptions = {
      bounds: this.defaultBounds,
      componentRestrictions: { country: "za" },
      fields: ["address_components", "geometry", "icon", "name"],
      strictBounds: false,
      types: ["establishment"],
    };
    
    this.autocomplete = new google.maps.places.Autocomplete(this.input, this.mapoptions);  
    
    this.autocomplete.addListener("place_changed", () => {
  
      const place = this.autocomplete.getPlace();
      this.loccoordinates = place.geometry.location.lat()+
      ','+place.geometry.location.lng();
      localStorage.setItem('loccoordinates', this.loccoordinates); 
            
      this.maplocation = place.name+', '+place.address_components[1].long_name
      +', '+place.address_components[5].long_name
      +', '+place.address_components[5].short_name
      +', '+place.address_components[6].short_name; 
      localStorage.setItem('maplocation', this.maplocation);
      location.reload();
    });     
    
    if(typeof localStorage.loccoordinates == 'undefined' && typeof localStorage.token == 'undefined') {
      setTimeout(() => {
        jQuery('#mapPopup').modal('show');
      }, 1000);
    }

    this.titlemessage = '';
    this.filterType = '';
    this.bestJobsText = 'Recent jobs';
    this.userData = '';
    this.authSubscription = this.authService.authState.subscribe((user) => {
      this.userData = user;
      this.email = user.email; 
      this.firstName = user.firstName;
      this.lastName = user.lastName;
      let x = Math.floor((Math.random() * 10000) + 1);
      this.name = user.name+''+x;
      this.profile_img = user.photoUrl;
      this.user_type = localStorage.getItem('user_type_click');
      // if (this.user_type == 'facebook') {
      //   this.email = user.id+'@facebook.com';
      // }
      this.signupStep2 = false;
      // localStorage.removeItem('user_type_click');

      var data = {
        "email" : this.email,
        "location" : localStorage.getItem('maplocation'),
        "loc_coordinates" : localStorage.getItem('loccoordinates')
      }
        // this.orderdata
        this.user.emailCheckAuto(data).subscribe((res)=>{
          this.errorMessage = '';
          this.signupStep1 = false;
          this.signupStep2 = true;
          this.loginStep = false;  
          this.verificationStep = false;        
          this.SpinnerService.hide();  
          //localStorage.removeItem('loccoordinates');        
        },Error =>{
          this.errorMessage = '';          
          this.social_login_step();
        })      
    });    
    // if(typeof localStorage.token !== 'undefined') {
    //   if (localStorage.getItem('user_type') == "both" || localStorage.getItem('user_type') == "jobber") {
    //     this.redirectToWhenLogin('/jobberprofile');
    //   } else {
    //     this.redirectToWhenLogin('/profile');        
    //   }
    // }

    window.onscroll = function() {myFunction()};
    
    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;
    
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }    

    var owl = jQuery('.owl-carousel');
    owl.owlCarousel({
      margin: 10,
      autoplay:true,
  autoplayTimeout:2000,
  autoplayHoverPause:true,
      loop: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 1
        }
      }
    })

    if(typeof localStorage.token !== 'undefined') {
      this.loggedIn = true;
      var data = {
        "customers_id": localStorage.getItem('user_id')
      }
      this.jobberator.jobberatorStepOneDetails(data).subscribe((res) => {
        this.isPost =  res?.data?.jobberator?.tab1;
      }, error => {
        //this.redirectTo('/');
      }
      );
    } else {
      this.loggedIn = false;      
    }   
    this.ImageUrl='assets/images/image_upload.png'; 
    this.dynamicLoginMessage = '';      
    this.signupStep1 = true;
    this.signupStep2 = false;
    this.loginStep = false;
    this.verificationStep = false;
    this.email = '';
    this.errorMessage = '';
    this.noticount = '';
    this.location = '';  
    if(typeof localStorage.token !== 'undefined') {   
      this.customerDetails();
    }
    this.categoryList(); 
    this.otherSettingsList(); 
    this.doGetJobList(); 
    this.doGetJobListbyCategory();
    if(localStorage.getItem('user_type_click') == 'linkedin') {
      localStorage.removeItem('user_type_click')
      jQuery('#myModal_linkedin').modal('show');
    } 
  }

  linkedinlogin() {
    // window.location.href = `https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=${
    //   this.linkedInCredentials.clientId
    // }&redirect_uri=${this.linkedInCredentials.redirectUrl}&scope=${this.linkedInCredentials.scope}`;
    window.location.href = `https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=${
      this.linkedInCredentials.clientId
    }&redirect_uri=${this.linkedInCredentials.redirectUrl}&scope=${this.linkedInCredentials.scope}`;
  }


  locationChange(){
    jQuery('#mapPopup').modal('show');
  }

  showToasterSuccess(){
    this.notifyService.showSuccess(this.titlemessage, "")
  }
  
  showToasterError(){
      this.notifyService.showError(this.titlemessage, "")
  }
  
  showToasterInfo(){
      this.notifyService.showInfo(this.titlemessage, "")
  }
  
  showToasterWarning(){
      this.notifyService.showWarning(this.titlemessage, "")
  }


  sortJob(type: string) {
    this.filterType = type;
    this.jobCatagoryList = [];
    if (type == '') {
      this.bestJobsText = 'Recent jobs';      
    }
    if (type == 'price') {
      this.bestJobsText = 'Price - high to low';
    }
    if (type == 'pricelow') {
      this.bestJobsText = 'Price - low to high';
    }
    if (type == 'rating') {
      this.bestJobsText = 'Rating - high to low';
    }
    if (type == 'ratinglow') {
      this.bestJobsText = 'Rating - low to high';      
    }
    if (type == 'near') {
      this.bestJobsText = 'Jobs near me';      
    }
    this.doGetJobList();
  }


    notificationList() {
      var param = {
        "customers_id" : localStorage.getItem('user_id'),
        "user_type" : this.userType
      }      
      this.apiService.notificationList(param)
      .subscribe(
        (data: any) => {
          this.noticount = data.data.count;
          this.jobTypenotificationsList = data.data.jobtypedata;
          this.jobTypenotificationsList.forEach((value: any, key: any) => {
            this.jobTypenotificationsList[key]['username'] = value['cust_details']['first_name']+" "+value['cust_details']['last_name']; 
            this.jobTypenotificationsList[key]['comment'] = value['status']+" job type "+value['other'];          
            // if (value['cust_details']['profile_img'] == null) {
              this.jobTypenotificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            // } else {
            //   this.jobTypenotificationsList[key]['profile_img'] = environment.server_url+'' +value['cust_details']['profile_img'];
            // }           
          });  
          
          this.jobTypeFilternotificationsList = data.data.jobtypeprofiledata;
          this.jobTypeFilternotificationsList.forEach((value: any, key: any) => {
            this.jobTypeFilternotificationsList[key]['username'] = value['cust_details']['first_name']+" "+value['cust_details']['last_name']; 
            this.jobTypeFilternotificationsList[key]['comment'] = value['status']+" job type "+value['other'];          
         
          });

          this.notificationsList = data.data.notification_info;
          this.notificationsList.forEach((value: any, key: any) => {
            //this.respbody[key]['details'][0]['image_path'] = environment.server_url+'uploads/job_category/' +this.respbody[key]['details'][0]['image_path']; 

            if (value['status'] == 'jobberator_accepted') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " accepted your job application";
              // this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }
            if (value['status'] == 'started') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " started job";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }            
            if (value['status'] == 'jobberator_rejected') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
              this.notificationsList[key]['comment'] = " rejected your job application";
            } 
            if (value['status'] == 'jobberator_approved') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
              this.notificationsList[key]['comment'] = " approved your job application";
            }
            if (value['status'] == 'jobberator_disapproved') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
              this.notificationsList[key]['comment'] = " disapproved your job application";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            } 
            if (value['status'] == 'jobberator_disputed') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
              this.notificationsList[key]['comment'] = " disputed your job done";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }                         
            if (value['status'] == 'jobber_accepted') {
              this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];               
              this.notificationsList[key]['comment'] = " accepted your job acceptance";
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              if (value['jobber_details']['user_type'] == 'normal' || value['jobber_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobber_details']['user_type'] == 'linkedin' || value['jobber_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img']+'?rand='+x;
                }
              }
            }
            if (value['status'] == 'jobber_rejected') {
              this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
              this.notificationsList[key]['comment'] = " rejected your job acceptance";
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              if (value['jobber_details']['user_type'] == 'normal' || value['jobber_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobber_details']['user_type'] == 'linkedin' || value['jobber_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img']+'?rand='+x;
                }
              }
            }  
            if (value['status'] == 'jobber_disputed') {
              this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
              this.notificationsList[key]['comment'] = " disputed your job done";
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              if (value['jobber_details']['user_type'] == 'normal' || value['jobber_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobber_details']['user_type'] == 'linkedin' || value['jobber_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'initiated') {
              this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
              this.notificationsList[key]['comment'] = " applied";
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              if (value['jobber_details']['user_type'] == 'normal' || value['jobber_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobber_details']['user_type'] == 'linkedin' || value['jobber_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img']+'?rand='+x;
                }
              }
            }   
            
            if (value['status'] == 'rated' && value['user_type'] == 'jobber') {
              this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
              this.notificationsList[key]['comment'] = " rated";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
              if (value['jobber_details']['user_type'] == 'normal' || value['jobber_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobber_details']['user_type'] == 'linkedin' || value['jobber_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobber_details']['profile_img']+'?rand='+x;
                }
              }
            }  
            
            if (value['status'] == 'rated' && value['user_type'] == 'jobberator') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];              
              this.notificationsList[key]['comment'] = " rated";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }
            
            if (value['status'] == 'started' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobber started task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobberator_disputed' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobberator disputed task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobber_disputed' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobber disputed task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobberator_rejected' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobberator rejected task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobberator_accepted' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobberator accepted task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobber_accepted' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobber accepted task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobber_rejected' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobber rejected task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'completed' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobberator completed task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobberator_canceled' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobberator canceled task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobber_accept_cancellation' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobber accepted cancellation task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobber_cant_reach_agreement' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobber coudn't reach agreement for the task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobberator_accepted_cant_reach' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobberator accepted can't reach task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobberator_job_canceled' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobberator cancelled job";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }

            if (value['status'] == 'jobber_accepted_job_cancellation' && value['task_id'] !== '') {
              this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
              this.notificationsList[key]['comment'] = " jobber accepted job cancellation task";
              //this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
              if (value['jobberator_details']['user_type'] == 'normal' || value['jobberator_details']['profile_img'].includes('uploads/user_profile/')) {
                this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];  
              } else {
                let x = Math.floor((Math.random() * 10000) + 1);
                // Remove backslashes from the image URL
                if (value['jobberator_details']['user_type'] == 'linkedin' || value['jobberator_details']['user_type'] == 'facebook') {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img'].replace(/\//g, '/');
                } else {
                  this.notificationsList[key]['profile_img'] = value['jobberator_details']['profile_img']+'?rand='+x;
                }
              }
            }
                     
            
          });  
          this.notificationsList = this.notificationsList.splice(0,10);
        },
        error => {
          console.log(error);
        }
      );    
    }

    createRange(number: any){
      if (isNaN(number)) {
        number = 0;
      } 
      return new Array(number);
    }

    //Get jobdeatailed List
    doGetJobList() {
      if (!this.loggedIn) {
      if(typeof localStorage.loccoordinates == 'undefined') {      
        this.apiService.doGetJobList(this.filterType)
          .subscribe(
            (data: any) => {
              this.jobList = data.data.resp;
              this.jobList.forEach((value: any, key: any) => {

                // if (value['cat_image_path'] == null) {
                //   value['cat_image_path'] = 'assets/images/Default_Image_Thumbnail.jpg';
                // } else {
                  value['cat_image_path']= environment.server_url+'uploads/job_category/' +value['cat_image_path'];
                // }
  
                // if ((value['jobpost_photos'].length == 0)) {
                //   value['jobpost_photos'][0] = [];
                //   value['jobpost_photos'][0]['image_path'] = 'assets/images/Default_Image_Thumbnail.jpg';
                // } else {
                 // value['jobpost_photos'][0]['image_path'] = environment.server_url+'' +value['jobpost_photos'][0]['image_path'];                
                // }
              });            
            },
            error => {
              console.log(error);
            }
          );
      } else {
        this.apiService.doGetJobListByLoc(localStorage.user_id,localStorage.getItem('loccoordinates'),this.filterType)
          .subscribe(
            (data: any) => {
              this.jobList = data.data.resp;
              this.jobList.forEach((value: any, key: any) => {
                // if (value['cat_image_path'] == null) {
                //   value['cat_image_path'] = 'assets/images/Default_Image_Thumbnail.jpg';
                // } else {
                  value['cat_image_path']= environment.server_url+'uploads/job_category/' +value['cat_image_path'];
                // }
  
                // if ((value['jobpost_photos'].length == 0)) {
                //   value['jobpost_photos'][0] = [];
                //   value['jobpost_photos'][0]['image_path'] = 'assets/images/Default_Image_Thumbnail.jpg';
                // } else {
                 // value['jobpost_photos'][0]['image_path'] = environment.server_url+'' +value['jobpost_photos'][0]['image_path'];                
                // }
              });            
            },
            error => {
              console.log(error);
            }
          );        
      }

      } else {
        if(typeof localStorage.loccoordinates == 'undefined') { 
          this.apiService.doGetJobLoggedInList(localStorage.user_id,this.filterType)
          .subscribe(
            (data: any) => {
              this.jobList = data.data.resp;
              this.jobList.forEach((value: any, key: any) => {
                // if (value['cat_image_path'] == null) {
                //   value['cat_image_path'] = 'assets/images/Default_Image_Thumbnail.jpg';
                // } else {
                  value['cat_image_path']= environment.server_url+'uploads/job_category/' +value['cat_image_path'];
                // }
                //this.respbody[key]['details'][0]['image_path'] = environment.server_url+'uploads/job_category/' +this.respbody[key]['details'][0]['image_path']; 
                // if (value['jobpost_photos'][0]['image_path'] == null) {
                //   value['jobpost_photos'][0]['image_path'] = 'assets/images/slider_04.png';
                // } else {
                //   value['jobpost_photos'][0]['image_path'] = environment.server_url+'' +value['jobpost_photos'][0]['image_path'];
                // }
              });            
            },
            error => {
              console.log(error);
            }
          );        
        }  else {
          this.apiService.doGetJobListByLoc(localStorage.user_id,localStorage.getItem('loccoordinates'),this.filterType)
            .subscribe(
              (data: any) => {
                this.jobList = data.data.resp;
                this.jobList.forEach((value: any, key: any) => {
                  // if (value['cat_image_path'] == null) {
                  //   value['cat_image_path'] = 'assets/images/Default_Image_Thumbnail.jpg';
                  // } else {
                    value['cat_image_path']= environment.server_url+'uploads/job_category/' +value['cat_image_path'];
                  // }
    
                  // if ((value['jobpost_photos'].length == 0)) {
                  //   value['jobpost_photos'][0] = [];
                  //   value['jobpost_photos'][0]['image_path'] = 'assets/images/Default_Image_Thumbnail.jpg';
                  // } else {
                   // value['jobpost_photos'][0]['image_path'] = environment.server_url+'' +value['jobpost_photos'][0]['image_path'];                
                  // }
                });            
              },
              error => {
                console.log(error);
              }
            );        
        }
      }
    }

    profileChange(type: string) {
      var data = {
        "customers_id" : localStorage.getItem('user_id')
      }    
      this.jobberator.profileChange(data).subscribe((res) => {
        localStorage.setItem('user_type','both');
        this.redirectTo(type);
      }, error => {
        //this.redirectTo('/');
      }
      );  
    }

    customerDetails() {
      var data = {
        "customers_id" : localStorage.getItem('user_id')
      }
      this.jobberator.jobberatorStepOneDetails(data).subscribe((res) => {
        this.location = res.data.customerInfo.location;        
        this.userType = res.data.customerInfo.type;
        this.userName = res.data.customerInfo.name;
        this.userEmail = res.data.customerInfo.email;
        this.image = environment.server_url + res.data.customerInfo.profile_img;
        this.getImageUrl = res.data.customerInfo.profile_img;

        if(this.getImageUrl != null && this.getImageUrl != '' && this.getImageUrl != 'null')
        {
          if (res.data.customerInfo.user_type == 'normal' || res.data.customerInfo.profile_img.includes('uploads/user_profile/')) {
            this.ImageUrl = environment.server_url + res.data.customerInfo.profile_img;  
          } else {
            let x = Math.floor((Math.random() * 10000) + 1);
            // Remove backslashes from the image URL
            if (res.data.customerInfo.user_type == 'linkedin' || res.data.customerInfo.user_type == 'facebook') {
              this.ImageUrl = res.data.customerInfo.profile_img.replace(/\//g, '/');
            } else {
              this.ImageUrl = res.data.customerInfo.profile_img+'?rand='+x;
            }
          }
        }
        else{
          this.ImageUrl = 'assets/images/image_upload.png';   
        }

        if (this.loggedIn) {
          this.notificationList(); 
        }
  
      }, error => {
        //this.redirectTo('/');
      }
      );
    }   

     //Go to jog details page
    //  doGotoJobDetailsPage(jobid: any){
    //   this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobid }});
    //   return false;
    //  } 

    doGotoJobDetailsPage(job: any,i: any){
      this.popUpJobDetails = job;
      this.popUpi = i;
    } 
     
    //notification view
    notificationView(jobid: any, notificationLogId: any, link: string){
      var data = {
        "job_intimation_log_id" : notificationLogId
      }
      this.jobberator.notificationView(data).subscribe((res) => {
        // this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobid }});
        this.router.navigate([''+link+'']);
        return false;
      }, error => {
        //this.redirectTo('/');
      }
      );    
    }
    
    //notification view task
    notificationViewTask(jobid: any, notificationLogId: any, link: string){
      var data = {
        "job_intimation_log_id" : notificationLogId
      }
      this.jobberator.notificationViewTask(data).subscribe((res) => {
        // this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobid }});
        this.router.navigate([''+link+'']);
        return false;
      }, error => {
        //this.redirectTo('/');
      }
      );    
    }

    jobtypeNotificationView(jobid: any, notificationLogId: any){
      var data = {
        "job_intimation_log_id" : notificationLogId
      }
      this.jobberator.jobtypeNotificationView(data).subscribe((res) => {
        this.router.navigate(['/jobpost/'+jobid]);
        return false;
      }, error => {
        //this.redirectTo('/');
      }
      );    
    }
    
    jobtypeProfileNotificationView(notificationLogId: any){
      var data = {
        "job_intimation_log_id" : notificationLogId
      }
      this.jobberator.jobtypeProfileNotificationView(data).subscribe((res) => {
        this.router.navigate(['/jobberprofile']);
        return false;
      }, error => {
        //this.redirectTo('/');
      }
      );    
    }    

    //Get jobdeatails
    doGetJobListbyCategory() {
      this.apiService.doGetJobListbyCategory()
        .subscribe(
          (data: any) => {
            this.jobCatagoryList = data.data.category;
          },
          error => {
            console.log(error);
          }
        );
        }
     
      //Go to jog details page
      doGotoJobListingPage(){
        this.router.navigate(['/jobcategory-listing']);
        }   
         //Go to jog details page
         doGotoJobDetailsCategoryPage(catName:string){
          this.router.navigate(['/jobcategory-listing'],{ queryParams: { category: catName }});
          }   

  categoryList() {
    //this.SpinnerService.show();
    this.category.getHomeCategories().subscribe((res) => {
      this.respbody = res.data.category;
      this.respbody.forEach((value: any, key: any) => {
        //this.respbody[key]['details'][0]['image_path'] = environment.server_url+'uploads/job_category/' +this.respbody[key]['details'][0]['image_path']; 
        if (this.respbody[key]['details'][0]['image_path'] == null) {
          this.respbody[key]['details'][0]['image_path'] = 'assets/images/catagerios_06.png';
        } else {
          this.respbody[key]['details'][0]['image_path'] = environment.server_url+'uploads/job_category/' +this.respbody[key]['details'][0]['image_path'];
        }
      });
      //this.categoryFirst = this.respbody.slice(0, 1);
      this.categoryFirst = this.respbody.slice(0, 4);      
      this.categoryNextFourth = this.respbody.slice(1, 5);
      this.categoryRemains = this.respbody.slice(5, 10);
     // this.redirectTo('/');
    }, error => {
      //this.redirectTo('/');
    }
    );
  }


  otherSettingsList() {
    //this.SpinnerService.show();
    this.category.getOtherSettings().subscribe((res) => {
      this.settingsbody = res.other;
     // this.redirectTo('/');
    }, error => {
      //this.redirectTo('/');
    }
    );
  }

  signup_step1() {

    if(this.form.valid){
      var data = {
        "email" : this.form.value.email,
        "location" : localStorage.getItem('maplocation'),
        "loc_coordinates" : localStorage.getItem('loccoordinates')
      }
        // this.orderdata
        this.user.emailCheck(data).subscribe((res)=>{
          this.email = this.form.value.email;
          if (res.status == 'otp') {
            this.signupStep1 = false;
            this.signupStep2 = false;
            this.loginStep = false;  
            this.verificationStep = true;
            this.errorMessage = ''; 
            this.titlemessage = 'An email with OTP sent to '+this.email;
            this.showToasterSuccess(); 
          }

          if (res.status == 'signup2') {
            this.errorMessage = '';          
            this.email = this.form.value.email; 
            this.signupStep1 = false;
            this.signupStep2 = true;
            this.loginStep = false;
            this.verificationStep = false;            
          }

        },Error =>{
          this.errorMessage = Error.error.error.msg;
          this.SpinnerService.hide();
        })
    }
  }

  otp_verify() {

    if(this.otpform.valid){
      var data = {
        "email" : this.email,
        "otp" : this.otpform.value.otp
      }
      // this.orderdata
      this.user.verifyotp(data).subscribe((res)=>{
        this.userData = '';
        this.signupStep1 = false;
        this.signupStep2 = true;
        this.loginStep = false;  
        this.verificationStep = false;
        this.errorMessage = '';  
        this.titlemessage = 'Your email verified successfully';
        this.showToasterSuccess(); 
        // jQuery("#mymodalsignup").click();  
        // jQuery("#mainclose").click();  
        //this.redirectTo('/usersuccess');
      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
    }
  }

  signup_step2() {

    if(this.signup2form.valid){
      var data = {
        "first_name" : this.signup2form.value.first_name,
        "last_name" : this.signup2form.value.last_name,
        "username" : this.signup2form.value.username,
        "email" : this.email,
        "password" : this.signup2form.value.password,
        "user_type" : this.signup2form.value.optradio,
        "location" : localStorage.getItem('maplocation'),
        "loc_coordinates" : localStorage.getItem('loccoordinates')
      }
      // this.orderdata
      this.user.createCustomer(data).subscribe((res)=>{
        /*this.signupStep1 = false;
        this.signupStep2 = false;
        this.loginStep = false;  
        this.verificationStep = true;
        this.errorMessage = ''; */ 
        // jQuery("#mymodalsignup").click();  
        // jQuery("#mainclose").click();  
        // this.redirectTo('/usersuccess');
        this.titlemessage = 'Signup completed successfully';
        this.showToasterSuccess();
        this.signupStep1 = false;
        this.signupStep2 = false;
        this.loginStep = true;  
        this.verificationStep = false;
        this.errorMessage = ''; 
        //this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
    }
  }


  signup_step3() {

    if(this.signup3form.valid){
      var data = {
        "first_name" : this.firstName,
        "last_name" : this.lastName,
        "username" : this.name,
        "email" : this.email,
        "user_type" : this.signup3form.value.optradio,
        "customer_type" : this.user_type,
        "profile_img" : this.profile_img,
        "location" : localStorage.getItem('maplocation'),
        "loc_coordinates" : localStorage.getItem('loccoordinates')
      }
      // this.orderdata
      this.user.createCustomerSocial(data).subscribe((res)=>{
        //this.redirectTo('/usersuccess');
        this.social_login_step();
      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
    }
  }  

  forgotPasswordStep1(){
    this.forgotStep1 = true;
    this.forgotStep2 = false;
    this.signupStep1 = false;
    this.signupStep2 = false;
    this.loginStep = false;  
    this.verificationStep = false;
    this.errorMessage = '';  
  } 

  forgotPasswordStep2(){
    var data = {
      "email" : this.forgotform.value.email
    }
      // this.orderdata
      this.user.emailCheckDirect(data).subscribe((res)=>{
        this.email = this.forgotform.value.email;
        this.forgotStep1 = false;
        this.forgotStep2 = true;
        this.signupStep1 = false;
        this.signupStep2 = false;
        this.loginStep = false;  
        this.verificationStep = false;
        this.errorMessage = ''; 
        this.titlemessage = 'An email with OTP sent to '+this.email;
        this.showToasterSuccess();

      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
  } 

  change_password(){
    var data = {
      "email" : this.email,
      "otp" : this.forgotpassform.value.otp,
      "password" : this.forgotpassform.value.password
    }
      // this.orderdata
      this.user.changePassword(data).subscribe((res)=>{
        this.email = this.forgotform.value.email;
        this.forgotStep1 = false;
        this.forgotStep2 = false;
        this.signupStep1 = false;
        this.signupStep2 = false;
        this.loginStep = true;  
        this.verificationStep = false;
        this.errorMessage = ''; 
        this.titlemessage = 'Password changed successfully';
        this.showToasterSuccess();

      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })    
  }

  gotoLoginNew(){
    this.forgotStep1 = false;
    this.forgotStep2 = false;
    this.signupStep1 = false;
    this.signupStep2 = false;
    this.loginStep = true;
    this.verificationStep = false;  
    this.errorMessage = '';  
  }

  gotoSignupNew(){
    this.forgotStep1 = false;
    this.forgotStep2 = false;
    this.signupStep1 = true;
    this.signupStep2 = false;
    this.loginStep = false;  
    this.verificationStep = false;
    this.errorMessage = '';  
  } 

  gotoLogin(){
    this.forgotStep1 = false;
    this.forgotStep2 = false;
    this.signupStep1 = false;
    this.signupStep2 = false;
    this.loginStep = true;
    this.verificationStep = false;  
    this.errorMessage = '';  
    jQuery("#mymodalsignup").click();
    jQuery("#mainclose").click();    
  }

  gotoSignup(){
    this.forgotStep1 = false;
    this.forgotStep2 = false;
    this.signupStep1 = true;
    this.signupStep2 = false;
    this.loginStep = false;  
    this.verificationStep = false;
    this.errorMessage = '';  
    jQuery("#mymodalsignup").click();  
    jQuery("#mainclose").click();       
  }  

  login_step() {
    let jobDetailsjobId:any;

    if(this.loginform.valid){
      var data = {
        "username" : this.loginform.value.name,
        "password" : this.loginform.value.password
      }
      // this.orderdata
      this.user.login(data).subscribe((res)=>{
        localStorage.setItem('token', res.data.access_token);
        localStorage.setItem('user_id', res.data.user.id);
        localStorage.setItem('user_email', res.data.user.email);
        localStorage.setItem('user_type', res.data.user.type);
        localStorage.removeItem('loccoordinates');

        jobDetailsjobId = localStorage.getItem('jobDetailsjobId');
        this.titlemessage = 'Logged in successfully';
        if(jobDetailsjobId != null){
          //this.redirectTo('/jobdetails');
          localStorage.removeItem("jobDetailsjobId");
          // this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobDetailsjobId }});
          let el: HTMLElement = this.myDiv.nativeElement;
          el.click();
          if (res.data.user.type == 'jobber') {
            //this.redirectTo('/myjobberjobs');
            this.showToasterSuccess();
            this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
          } else {
            //this.redirectTo('/myjobs');
            this.showToasterSuccess();
            this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
          } 
          return false;          
          //this.redirectTo('/jobdetails?job_id='+jobDetailsjobId);
        }else {
          if(this.postaJobStatus){
            this.postaJobStatus = false;
            // res.data.user.type != 'jobber' ? this.redirectTo('/jobpost') : "";
            this.showToasterSuccess();
            this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
          }else {
            if(this.viewJoblistStatus) {
              // this.redirectTo('/jobcategory-listing')
              this.showToasterSuccess();
              this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
            }else {
              if (res.data.user.type == 'jobber') {
                //this.redirectTo('/myjobberjobs');
                this.showToasterSuccess();
                this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
              } else {
                //this.redirectTo('/myjobs');
                this.showToasterSuccess();
                this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
              }  

            }
           

          }
          

        }
        
              



        //this.redirectTo('/myjobs');
        // if (res.data.user.type == "both" || res.data.user.type == "jobber") {
        //   this.redirectTo('/jobberprofile');
        // } else {
        //   this.redirectTo('/profile');        
        // }

      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
    }
  }


  social_login_step() {
    this.signupStep2 = false;
    let jobDetailsjobId:any;

      var data = {
        "email" : this.email,
        "user_type" : this.user_type
      }
      // this.orderdata
      this.user.sociallogin(data).subscribe((res)=>{
        localStorage.setItem('token', res.data.access_token);
        localStorage.setItem('user_id', res.data.user.id);
        localStorage.setItem('user_email', res.data.user.email);
        localStorage.setItem('user_type', res.data.user.type);
        localStorage.removeItem('user_type_click');

        jobDetailsjobId = localStorage.getItem('jobDetailsjobId');
        if(jobDetailsjobId != null){
          //this.redirectTo('/jobdetails');
          localStorage.removeItem('loccoordinates');
          localStorage.removeItem("jobDetailsjobId");
          // this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobDetailsjobId }});
          this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
          let el: HTMLElement = this.myDiv.nativeElement;
          el.click();
          return false;          
          //this.redirectTo('/jobdetails?job_id='+jobDetailsjobId);
        }else {
          if(this.postaJobStatus){
            this.postaJobStatus = false;
            res.data.user.type != 'jobber' ? this.redirectTo('/jobpost') : "";
          }else {
            if(this.viewJoblistStatus) {
              this.redirectTo('/jobcategory-listing')

            }else {
              if (res.data.user.type == 'jobber') {
                //this.redirectTo('/myjobberjobs');
                this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
              } else {
                //this.redirectTo('/myjobs');
                this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
              }  

            }
           

          }
          

        }
        

      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
  }

  notificationViewMyPage(notificationLogId: any,url: any){
    var data = {
      "job_intimation_log_id" : notificationLogId
    }
    this.jobberator.notificationView(data).subscribe((res) => {
      this.redirectTo(url);
      return false;
    }, error => {
      //this.redirectTo('/');
    }
    );    
  }  

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));

    this.signupStep1 = false;
    this.signupStep2 = false;
    this.loginStep = false;  
    let el: HTMLElement = this.myDiv.nativeElement;
    el.click();
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;    
  }

  redirectToWhenLogin(uri:string){
    this.router.navigate([uri]);
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user_id');
    localStorage.removeItem('user_type');
    localStorage.removeItem('localJobPostId');
    localStorage.removeItem('localplanchange');
    localStorage.removeItem('maplocation');
    localStorage.removeItem('user_type_click');
    localStorage.removeItem('user_email');
    localStorage.removeItem('linked_verify_back_page');
    this.loggedIn = false;
    this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
    // this.redirectTo('/');
    location.href = '/';
    return false;
  } 
  
  closeJob(jobId: any, i: any) {
    // if (!this.loggedIn) {
    //   jQuery("#mymodalsignup").click();
    // } else {
      this.jobList.splice(i,1);
      this.apiService.closeJob(jobId,localStorage.user_id)
      .subscribe(
        (data: any) => {
          let el: HTMLElement = this.myDivPopUp.nativeElement;
          el.click();
          // this.jobCatagory = data.data.category;
          this.titlemessage = 'Closed job successfully';
          this.showToasterSuccess(); 
        },
        error => {
          console.log(error);
        }
      );        
   // }
  }
  
  acceptJob(jobId: any, i: any) {
    // if (!this.loggedIn) {
    //   jQuery("#mymodalsignup").click();
    // } else {
      if (this.jobList[i].job_intimation[0]?.status == null || typeof this.jobList[i].job_intimation[0] === 'undefined') {
        if(typeof this.jobList[i].job_intimation[0] === 'undefined')    {
          this.jobList[i].job_intimation[0] = [];
        }           
        this.jobList[i].job_intimation[0].status = 'initiated';
        //this.jobList.splice(i,1);        
        this.apiService.acceptJob(jobId,localStorage.user_id)
        .subscribe(
          (data: any) => {
            //this.jobCatagory = data.data.category;
             this.titlemessage = 'Job applied successfully';
             this.showToasterSuccess(); 
             let el: HTMLElement = this.myDivPopUp.nativeElement;
             el.click();
             //this.router.navigate(['/skiplocation'],{ skipLocationChange : true});
          },
          error => {
            console.log(error);
          }
        );          
      }
//    }
  }    

  addToWishList(jobId: any, i: any) {
    let is_favourite: string;
    // if (!this.loggedIn) {
    //   jQuery("#mymodalsignup").click();
    // } else {
      if (this.jobList[i].job_intimation[0]?.is_jobbers_favourite == 'favourite') {
        if(typeof this.jobList[i].job_intimation[0] === 'undefined')    {
          this.jobList[i].job_intimation[0] = [];
        }           
        this.jobList[i].job_intimation[0].is_jobbers_favourite = 'unfavourite';
        is_favourite = 'unfavourite';
      } else {
        if(typeof this.jobList[i].job_intimation[0] === 'undefined')    {
          this.jobList[i].job_intimation[0] = [];
        }          
        this.jobList[i].job_intimation[0].is_jobbers_favourite = 'favourite';          
        is_favourite = 'favourite';
      }
      this.popUpJobDetails = this.jobList[i];
      let el: HTMLElement = this.myDivPopUp.nativeElement;
      el.click();
      this.apiService.addToWishList(jobId,localStorage.user_id,is_favourite)
      .subscribe(
        (data: any) => {
          if (is_favourite == 'unfavourite') {
            this.titlemessage = 'Removed from favourites successfully';
            this.showToasterSuccess(); 
          }
          if (is_favourite == 'favourite') {
            this.titlemessage = 'Added to favourites successfully';
            this.showToasterSuccess(); 
          }
        },
        error => {
          console.log(error);
        }
      ); 
  //  }
  }  
  jobSearchPage(searchname:any){
    //console.log(searchname)
   
    this.searchName = searchname
    // this.loggedIn ? this.router.navigate(['/jobcategory-listing'],{ queryParams: { searchname: this.searchName }}): '' ; 
    this.router.navigate(['/jobcategory-listing'],{ queryParams: { searchname: this.searchName }});    
  }

  homeConfirmation(jobId: any, count: any,clickStausType:any,currentStatus:any, currentLocation:any) {
    let dataStorage: any = [jobId,count,clickStausType,currentStatus];
    if (currentStatus != 'initiated' || clickStausType == 'close' || clickStausType == 'like') {

      this.jobId = jobId;
      this.count = count;
      this.status = clickStausType;

      clickStausType == 'close' ? jQuery('#confirmationHomeText').html("Do you want to close?") : jQuery('#confirmationHomeText').html("Do you want to accept?");
      if(clickStausType == 'like'){
        if(this.jobList[count].job_intimation[0]?.is_jobbers_favourite == 'favourite'){
          jQuery('#confirmationHomeText').html("Do you want to remove this item from favourits?")
        }
        else{
          jQuery('#confirmationHomeText').html("Do you want to add this item to favourits?")

        }  
      } 
      let el: HTMLElement = this.myDivPopUp.nativeElement;
      el.click();
      if (this.loggedIn) {   
        if (clickStausType == 'like')   {
          this.addToWishList(this.jobId,this.count);
        } else {
          if (currentStatus == null) {
            var data = {
              "customers_id" : localStorage.getItem('user_id')
            }
            this.jobber.jobberStepThreeDetails(data).subscribe((res) => {
              if (res.data.workinfo.length == 0) {
                jQuery('#confirmationProfileJobberatorHomePopup').modal('show');
              } else {
                if (res.data.workinfo[0].tab2 == '1') {
                  jQuery('#confirmationHomePopup').modal('show');  
                } else {
                  jQuery('#confirmationProfileHomePopup').modal('show');
                }                
              }

            }, error => {
              //this.redirectTo('/');
            }
            );
         
          } else {
            jQuery('#confirmationHomePopup').modal('show');
          }
        }
      } else {
        localStorage.setItem("jobDetailsjobId",jobId);
        if (clickStausType == 'accept') {
          this.dynamicLoginMessage = 'In order to apply this lite job you need to Log In / Sign Up';
        }
        if (clickStausType == 'close') {
          this.dynamicLoginMessage = 'In order to close this lite job you need to Log In / Sign Up';          
        }
        if (clickStausType == 'like') {
          this.dynamicLoginMessage = 'In order to shortlist/taken off from shortlist this lite job you need to Log In / Sign Up';      
        }                
        jQuery("#mymodalsignupmain").click();     
      }
    }
  }

  changeDynamicLoginMessage() {
    this.dynamicLoginMessage = '';
  }

  homeProfileConfirmationClick() {
    // jQuery('.modal-backdrop').remove(); 
    jQuery('#confirmationProfileHomePopup').modal('hide');
    this.redirectTo('/jobberprofile');   
  }

  homeConfirmationClick() {
    // if (!this.loggedIn) {
    //   jQuery("#mymodalsignup").click();
    // } else {

      jQuery('#confirmationHomePopup').modal('hide');

      if (this.status == 'accept') {
        this.acceptJob(this.jobId,this.count)
      }
      else if (this.status == 'close') {
        this.closeJob(this.jobId,this.count);
      }
      else if(this.status  == 'like') {
        this.addToWishList(this.jobId,this.count);
      }
//    }    
  }
  postaJob() {
    this.postaJobStatus = true
    jQuery('#myModal_login').modal('show');

    

  }
  viewAllJobListing() {
    // this.viewJoblistStatus = true
    // jQuery('#myModal_login').modal('show');
    this.redirectTo('/jobcategory-listing');
  }


  signInWithGoogle(): void {
    this.user_type = 'google';
    localStorage.setItem('user_type_click', this.user_type);
    // this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    // .then(() => {
    //   //success case
    //   })
    //   .catch(error => {
    //     console.log(error); //the behaviour of error popup can be handled here
    // });
  }

  signInWithFB(): void {
    this.user_type = 'facebook';
    localStorage.setItem('user_type_click', this.user_type);
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authService.signOut();
  }

  notvalid() {
    this.errorMessage = 'Please enter a valid email';
    return false;
  }

  clearLinkedin() {
    localStorage.removeItem('linkedInfirstName');
    localStorage.removeItem('linkedInlastName');
    localStorage.removeItem('linkedInName');
    localStorage.removeItem('linkedInEmail');
    localStorage.removeItem('linkedInUserType');
    localStorage.removeItem('linkedInProfileImg');   
  } 

  linkedin_signup_step3() {
    this.firstName = localStorage.getItem('linkedInfirstName');
    this.lastName = localStorage.getItem('linkedInlastName');
    this.name = localStorage.getItem('linkedInName');
    this.email = localStorage.getItem('linkedInEmail');
    this.user_type = localStorage.getItem('linkedInUserType');
    this.profile_img = localStorage.getItem('linkedInProfileImg');
    localStorage.removeItem('linkedInfirstName');
    localStorage.removeItem('linkedInlastName');
    localStorage.removeItem('linkedInName');
    localStorage.removeItem('linkedInEmail');
    localStorage.removeItem('linkedInUserType');
    localStorage.removeItem('linkedInProfileImg');
    if(this.signup3form.valid){
      var data = {
        "first_name" : this.firstName,
        "last_name" : this.lastName,
        "username" : this.name,
        "email" : this.email,
        "user_type" : this.signup3form.value.optradio,
        "customer_type" : this.user_type,
        "profile_img" : this.profile_img,
        "location" : localStorage.getItem('maplocation'),
        "loc_coordinates" : localStorage.getItem('loccoordinates')
      }
      // this.orderdata
      this.user.createCustomerSocial(data).subscribe((res)=>{
        //this.redirectTo('/usersuccess');
        this.social_login_step();
      },Error =>{
        this.errorMessage = Error.error.error.msg;
        this.SpinnerService.hide();
      })
    }
  }

}
