import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobbermytaskComponent } from './jobbermytask.component';

describe('JobbermytaskComponent', () => {
  let component: JobbermytaskComponent;
  let fixture: ComponentFixture<JobbermytaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobbermytaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobbermytaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
