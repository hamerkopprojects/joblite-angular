import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JobpostcallsService} from './../_services/jobpostcalls/jobpostcalls.service'; 
import { ApiService } from '../_services/commonlisting/api.service';
import { environment } from 'src/environments/environment';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { JobberatorprofileService } from './../_services/jobberatorprofile/jobberatorprofile.service';

declare var jQuery: any;

@Component({
  selector: 'app-myjobs',
  templateUrl: './myjobs.component.html',
  styleUrls: ['./myjobs.component.css']
})
export class MyjobsComponent implements OnInit {
  @ViewChild('t1') t1: ElementRef<HTMLElement>; 
  @ViewChild('t2') t2: ElementRef<HTMLElement>; 
  @ViewChild('t3') t3: ElementRef<HTMLElement>; 
  @ViewChild('t5') t5: ElementRef<HTMLElement>; 
  @ViewChild('t6') t6: ElementRef<HTMLElement>;    

  loadAPI: Promise<any>; 

  server_url: string;
  url = 'https://test.oppwa.com/v1/paymentWidgets.js?checkoutId='; 
  bankFee: any;
  peachFee: any;
  serviceCharge: any;
  serviceFee: any;
  totalamount: any; 
  totalout: any;
  totalin: any;
  searchpay: any;
  jobList: any; 
  draftsList: any;
  allJobList: any;  
  jobsPendingLists: any;
  jobsActiveLists: any;  
  currentDate: any; 
  profileImgUrl: any;
  userType: any;
  jobCatagoryList: any;
  jobCompletedList: any;
  ratingJobId: any;
  currentJob: any;
  jobberImg: any;
  jobberatorImg: any;
  jobberMoneyList: any;
  jobberMoneyTempList: any;  
  searchpaymentid: any;
  paytype: any;
  status: any;
  applicantsId: any;
  jobKey: any;
  customersID: any;
  noticount: any;
  notificationsList: any;
  ratingDoneCount: any;
  ratingBlankCount: any;
  ratingBlankCountJobber: any;
  ratingcomment: any;
  ratingcommentjobber: any;
  ratingEmpty: boolean;
  commentEmpty: boolean;  
  shopperResultUrl: any;
  checkoutId: any;
  showPayment: boolean;
  showPaymentTip: boolean;
  tipamount: any;
  amountEmpty: boolean;
  popupJobId: any;
  budget: any;
  popupJobName: any;
  tab: any; 
  constructor( 
    private jobpostcalls: JobpostcallsService,     
    private router: Router,
    private apiService: ApiService,    
    private route: ActivatedRoute,
    private jobberator: JobberatorprofileService) { }

  ngOnInit(): void {
    this.notificationsList = [];
    this.server_url = environment.server_url;
    this.serviceCharge = '';
    this.bankFee = 0;
    this.peachFee = 0;
    this.serviceFee = 0;
    this.totalamount = '';
    this.totalout = 0;
    this.totalin = 0;
    this.searchpay = '';
    this.ratingDoneCount = 0;
    this.ratingBlankCount = 5;
    this.ratingBlankCountJobber = 5;
    this.ratingJobId = ''; 
    this.currentJob = '';
    this.jobberImg = '';
    this.jobberatorImg = '';
    this.ratingcomment = '';
    this.tipamount = '';
    this.popupJobId = '';

    this.ratingcommentjobber = '';
    this.ratingEmpty = false;
    this.commentEmpty = false; 
    this.amountEmpty = false;   
    this.showPayment = false;
    this.showPaymentTip = false;    
    this.searchpaymentid = '';
    this.paytype = '';
    this.budget = '';
    this.shopperResultUrl = environment.callback_url+'paycallbackjobstart/'+this.applicantsId+'/'+this.customersID+'/'+this.status;    
    this.jobList = [];
    this.draftsList = [];
    this.allJobList = [];
    this.jobsPendingLists = [];
    this.profileImgUrl = environment.server_url;
    this.popupJobName = '';
    //this.tab = "";
    this.tab = this.route.snapshot.params.tab;
    if (this.tab == 't1' ) {
      setTimeout(() => {
        let el: HTMLElement = this.t1.nativeElement;
        el.click();
      }, 1000) 
    }
    if (this.tab == 't2' ) {
      setTimeout(() => {
        let el: HTMLElement = this.t2.nativeElement;
        el.click();
      }, 1000) 
    }
    if (this.tab == 't3' ) {
      setTimeout(() => {
        let el: HTMLElement = this.t3.nativeElement;
        el.click();
      }, 1000) 
    }
    if (this.tab == 't5' ) {
      setTimeout(() => {
        let el: HTMLElement = this.t5.nativeElement;
        el.click();
      }, 1000) 
    }
    if (this.tab == 't6' ) {
      setTimeout(() => {
        let el: HTMLElement = this.t6.nativeElement;
        el.click();
      }, 1000) 
    }

    this.userType = localStorage.getItem('user_type');
    
    this.myJobsLists();
    this.draftLists();
    this.myJobsPendingLists();
    this.myJobsActiveLists();    
    this.favoriteJobs();   
    this.myJobsCompletedList();
    this.myMoneyList();
    this.myAllJobsLists();    
  }

  createRange(number: any){
    if (isNaN(number)) {
      number = 0;
    } 
    return new Array(number);
  }

  loadScript() {
    console.log(this.checkoutId)
    let node = document.createElement('script');
    node.src = this.url+''+this.checkoutId;
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }  
  
  
  draftLists() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobpostcalls.myDraftsLists(data).subscribe((res) => {
      this.draftsList = res.data.joblist;
      this.draftsList.forEach((value: any, key: any) => {
        this.draftsList[key].cat_image_path = environment.server_url+'uploads/job_category/' +value['cat_image_path'];              
      });    
      // this.jobList.forEach((value: any, key: any) => {
               
      // });       
    }, error => {
      //this.redirectTo('/');
    }
    );
  }

  myJobsLists() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobpostcalls.myJobsLists(data).subscribe((res) => {
      this.jobList = res.data.joblist;
      this.jobList.forEach((value: any, key: any) => {
        if (value.work_type == 'inperson') {
          this.jobList[key].work_type = 'In-person';
        }
        if (value.work_type == 'online') {
          this.jobList[key].work_type = 'Online';
        } 
        var date = new Date();
        this.currentDate = date.toISOString().slice(0, 19).replace('T', ' ');        
        if (value.status == 'active') {
          this.jobList[key].stage = 'Active';
        } else if (value.status == 'deactive') {
          this.jobList[key].stage = 'Canceled';
        } else  {
          this.jobList[key].stage = 'Not Completed';
        }  
        this.jobList[key].cat_image_path = environment.server_url+'uploads/job_category/' +value['cat_image_path'];               
      });       
    }, error => {
      //this.redirectTo('/');
    }
    );
  }
  
  myAllJobsLists() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobpostcalls.myAllJobsLists(data).subscribe((res) => {
      this.allJobList = res.data.joblist;
      this.allJobList.forEach((value: any, key: any) => {
        if (value.work_type == 'inperson') {
          this.allJobList[key].work_type = 'In-person';
        }
        if (value.work_type == 'online') {
          this.allJobList[key].work_type = 'Online';
        } 
        var date = new Date();
        this.currentDate = date.toISOString().slice(0, 19).replace('T', ' ');        
        if (value.status == 'active') {
          this.allJobList[key].stage = 'Active';
        } else if (value.status == 'deactive') {
          this.allJobList[key].stage = 'Canceled';
        } else  {
          this.allJobList[key].stage = 'Not Completed';
        }  
        this.allJobList[key].cat_image_path = environment.server_url+'uploads/job_category/' +value['cat_image_path'];               
      });       
    }, error => {
      //this.redirectTo('/');
    }
    );
  }  

  myJobsPendingLists() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobpostcalls.myJobsPendingLists(data).subscribe((res) => {
      this.jobsPendingLists = res.data.joblist;
      this.jobsPendingLists.forEach((value: any, key: any) => {
        if (value.work_type == 'inperson') {
          this.jobsPendingLists[key].work_type = 'In-person';
        }
        if (value.work_type == 'online') {
          this.jobsPendingLists[key].work_type = 'Online';
        } 
        var date = new Date();
        this.currentDate = date.toISOString().slice(0, 19).replace('T', ' ');        
        if (value.status == 'active') {
          this.jobsPendingLists[key].stage = 'Active';
        } else if (value.status == 'deactive') {
          this.jobsPendingLists[key].stage = 'Canceled';
        } else  {
          this.jobsPendingLists[key].stage = 'Not Completed';
        } 
        this.jobsPendingLists[key].cat_image_path = environment.server_url+'uploads/job_category/' +value['cat_image_path'];                
      });       
    }, error => {
      //this.redirectTo('/');
    }
    );
  }   

  myJobsActiveLists() {
    var data = {
      "customers_id" : localStorage.getItem('user_id')
    }
    this.jobpostcalls.myJobsActiveLists(data).subscribe((res) => {
      this.jobsActiveLists = res.data.joblist;
      this.jobsActiveLists.forEach((value: any, key: any) => {
        if (value.work_type == 'inperson') {
          this.jobsActiveLists[key].work_type = 'In-person';
        }
        if (value.work_type == 'online') {
          this.jobsActiveLists[key].work_type = 'Online';
        } 
        var date = new Date();
        this.currentDate = date.toISOString().slice(0, 19).replace('T', ' ');        
        if (value.status == 'active') {
          this.jobsActiveLists[key].stage = 'Active';
        } else if (value.status == 'deactive') {
          this.jobsActiveLists[key].stage = 'Canceled';
        } else  {
          this.jobsActiveLists[key].stage = 'Not Completed';
        } 
        this.jobsActiveLists[key].cat_image_path = environment.server_url+'uploads/job_category/' +value['cat_image_path'];                
      });       
    }, error => {
      //this.redirectTo('/');
    }
    );
  }  
  
  statusChange(status :any, jobid: any, key: any) {
    var data = {
      "customers_id" : localStorage.getItem('user_id'),
      "jobpost_id" : jobid,
      "status" : status
    }    
    this.jobpostcalls.statusChange(data).subscribe((res) => {
        this.jobList[key].status = status;
        if (status == 'deactive') {
          this.jobList[key].stage = 'Canceled';
        }
    }, error => {
      //this.redirectTo('/');
    }
    );
    return false;
  }


  jobberatorJobApplicantsStatusChange(jobintIdReceived:any,userIdReceived:any,statusReceived:any){
    //let status = 'jobberator_accepted';
   // var data = {
   //   "job_intimation_id": this.jobintId,
   //   "customers_id" : this.userId,
   //   "status" : status
    
     
   // }
   this.apiService.jobberJobApplicantsStatusChange(jobintIdReceived, userIdReceived, statusReceived).subscribe((response: any) => {
    this.myJobsLists();
    this.myJobsPendingLists();      
    this.myJobsActiveLists();
    
   },

     error => {
       //this.redirectTo('/');
     }
   );

  }  

  closeJob(jobintIdReceived:any,userIdReceived:any,statusReceived:any) {

    this.jobberatorJobApplicantsStatusChange(jobintIdReceived,userIdReceived,statusReceived);   
     
    }
  
  acceptJob(jobintIdReceived:any,userIdReceived:any,statusReceived:any) {

    this.jobberatorJobApplicantsStatusChange(jobintIdReceived,userIdReceived,statusReceived);   
    //this.jobList[count].customers.status = 'jobber_accepted';
  }   

  statusActiveChange(status :any, key: any, jobid: any) {
    var data = {
      "customers_id" : localStorage.getItem('user_id'),
      "jobpost_id" : jobid,
      "status" : status
    }    
    this.jobpostcalls.statusChange(data).subscribe((res) => {
      this.myJobsLists();
      this.myJobsPendingLists();      
      this.myJobsActiveLists();
    }, error => {
      //this.redirectTo('/');
    }
    );
    return false;
  }  

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false; 
  }  

  redirectToBySkip(uri:string){
    this.router.navigateByUrl('/skiplocation', {skipLocationChange: false}).then(()=>
    this.router.navigate([uri]));
    return false; 
  }  

  favoriteJobs() {
    this.apiService.favoriteJobs(localStorage.user_id)
    .subscribe(
      (data: any) => {
        data.data.resp.forEach((value: any, key: any) => {
          if (data.data.resp[key].job_intimation[0]?.status == 'donot_show') {
            data.data.resp.splice(key,1);
          }
        });            
        this.jobCatagoryList = data.data.resp;
        //console.log("jobcatlist",this.jobCatagoryList)
      
      },
      error => {
        console.log(error);
      }
    );

  }
  myJobsCompletedList(){
    let customerId = localStorage.getItem('user_id')
    console.log("cusid",customerId)
    var data = {
      "customers_id" : customerId
    }
    this.apiService.myJobsCompletedList(data)
    .subscribe((response) => {
                  
        this.jobCompletedList = response.data.joblist;
        this.tab = this.route.snapshot.params.tab;
        if (this.tab == 't3' ) {
          let el: HTMLElement = this.t3.nativeElement;
          el.click(); 
        } 
        this.jobCompletedList.forEach((value: any, key: any) => {
          this.jobCompletedList[key].cat_image_path = environment.server_url+'uploads/job_category/' +value['cat_image_path'];              
        });       
        //console.log("jobcompletedlist",this.jobCompletedList)
      
      },
      error => {
        console.log(error);
      }
    );

  }


  ratingChange(count: any) {
    this.ratingDoneCount = count;
    this.ratingBlankCount = 5 - count; 
  }


  cancelrating() {
    this.ratingDoneCount = 0;
    this.ratingBlankCount = 5;  
    this.ratingcomment = ''; 
    this.ratingcommentjobber = '';
    this.ratingEmpty = false;
    this.commentEmpty = false;      
    jQuery('#exampleModalRating').modal('hide');   
  }

  rating(jobId:any, ratingjobber: any, jobtitle: any, currentjob: any) {
    this.popupJobName = jobtitle;
    this.ratingJobId = jobId;
    this.currentJob = '';
    this.currentJob = currentjob;
    this.jobberImg = environment.server_url+'' +this.currentJob.customers.customer.profile_img;
    this.jobberatorImg = environment.server_url+'' +this.currentJob.jobberator.profile_img;
    this.ratingcommentjobber = ratingjobber;
    this.ratingBlankCountJobber = 5 - ratingjobber.rating;
    
    let data = {
      "customer_id" : localStorage.getItem('user_id'),
      "job_id" : this.ratingJobId,
      "usertype" : 'jobberator'
    }

    this.jobpostcalls.getRating(data).subscribe((res) => {
      if (res.data != null) {
        this.ratingDoneCount = res.data.rating;
        this.ratingBlankCount = 5 - this.ratingDoneCount;
        this.ratingcomment = res.data.comment;
      } else {
        this.ratingDoneCount = 0;
        this.ratingBlankCount = 5;  
        this.ratingcomment = '';       
      }
      // this.ratingDoneCount = 0;
      // this.ratingBlankCount = 5;  
      // this.ratingcomment = ''; 
      // this.ratingEmpty = false;
      // this.commentEmpty = false;          

    }, Error => {
    }
    );    
  }


  paytip(jobId:any, ratingjobber: any, jobtitle: any) {
    this.popupJobName = jobtitle;  
    this.popupJobId = jobId;
    this.amountEmpty = false;      
  } 
  
  paytipformcreate() {
    let flag: boolean;
    flag = true;
    this.amountEmpty = false;    

    if (this.tipamount == '' || this.tipamount == null) {
      flag = false;
      this.amountEmpty = true;
    } 
    if (flag == true) {
      var data = {
        "jobpost_id" : this.popupJobId,
        "amount" : this.tipamount        
      }
      this.jobpostcalls.startCheckoutInit(data).subscribe((res) => {
        this.checkoutId = res.data.id;      
        this.loadAPI = new Promise((resolve) => {
          console.log('resolving promise...');
          this.loadScript();
        });   
        this.shopperResultUrl = environment.callback_url+'paycallbacktip/'+this.popupJobId; 
        this.showPaymentTip = true;                 

      }, error => {
        console.log('error');
        console.log(error);
      }
      ); 
    }
    
    
  }  

  submitrating() {
    let flag: boolean;
    flag = true;
    this.ratingEmpty = false;
    this.commentEmpty = false;    
    if (this.ratingDoneCount == 0) {
      flag = false;
      this.ratingEmpty = true;
    }

    if (this.ratingcomment.trim() == '') {
      flag = false;
      this.commentEmpty = true;
    } 
    
    if (flag) {
      let data = {
        "customer_id" : localStorage.getItem('user_id'),
        "job_id" : this.ratingJobId,
        "rating" :  this.ratingDoneCount,
        "comment" : this.ratingcomment,
        "usertype" : 'jobberator'
      }

      this.jobpostcalls.ratingSave(data).subscribe((res) => {
        this.ratingDoneCount = 0;
        this.ratingBlankCount = 5;  
        this.ratingcomment = ''; 
        this.ratingEmpty = false;
        this.commentEmpty = false;          
        jQuery('#exampleModalRating').modal('hide');
        this.myJobsCompletedList();        

      }, Error => {
      }
      );
    }

  } 

  viewtip(jobId:any, jobtitle: any, currentjob: any) {
    this.popupJobName = jobtitle;
    this.ratingJobId = jobId;    
    this.currentJob = '';
    this.currentJob = currentjob;   
  }  
  
  cancelrequest() {
    jQuery('#exampleModalRequest').modal('hide');   
  }  


  requestrating() {

      let data = {
        "customer_id" : localStorage.getItem('user_id'),
        "job_id" : this.ratingJobId,
        "usertype" : 'jobberator'
      }

      this.jobpostcalls.requestRating(data).subscribe((res) => {
        jQuery('#exampleModalRequest').modal('hide');
        this.myJobsCompletedList();
      }, Error => {
      }
      );

  }

  myMoneyList(){
    let customerId = localStorage.getItem('user_id')
    console.log("cusid",customerId)
    var data = {
      "customers_id" : customerId,
      "search" : this.searchpaymentid,
      "pay_type" : this.paytype
    }
    this.apiService.myMoneyList(data)
    .subscribe((response) => {
        let totalout = 0;
        let totalin = 0;          
        this.jobberMoneyList = response.data.joblist;
        this.jobberMoneyTempList = response.data.joblist;
        this.jobberMoneyList.forEach((value: any, key: any) => {
          if (value['type'] == 'job amount returned') {
            totalin = totalin + parseFloat(value['amount']);
            totalin.toFixed(2);
          } else {
            totalout = totalout + parseFloat(value['amount']);
            totalout.toFixed(2);
          }

          if (this.jobberMoneyList[key]['code'] ==  '000.100.110') {
            this.jobberMoneyList[key]['paystatus'] = 'success';
          } else {
            this.jobberMoneyList[key]['paystatus'] = 'failed';
          }
          let d = new Date(value['created_at']);
          this.jobberMoneyList[key]['paydate'] = d.toLocaleString('default', { month: 'long' })+' '+d.getDate()+', '+d.getFullYear();
          //finalDate = d.toISOString();
          if (value['amount'] == 0) {
            delete this.jobberMoneyList[key];
          }
		    });
        this.totalout = totalout;
        this.totalin = totalin;          
        //console.log("jobcompletedlist",this.jobCompletedList)
      
      },
      error => {
        console.log(error);
      }
    );

  } 
  
  myMoneyListUsingStatus(type: any){
    if (this.paytype == type) {
      this.paytype = '';
    } else {
      this.paytype = type;
    }
    let customerId = localStorage.getItem('user_id')
    console.log("cusid",customerId)
    var data = {
      "customers_id" : customerId,
      "search" : this.searchpaymentid,
      "pay_type" : this.paytype
    }
    this.apiService.myMoneyList(data)
    .subscribe((response) => {
                  
        this.jobberMoneyList = response.data.joblist;
        //console.log("jobcompletedlist",this.jobCompletedList)
      
      },
      error => {
        console.log(error);
      }
    );

  }   

  confirmationPopup(status:any,id:any,key:any,customerId:any) {
   
    this.status = status
    this.applicantsId = id
    this.jobKey = key
    this.customersID = customerId
    if(status == 'views'){

      jQuery('#confirmationText').html("Do you want to fetch the viewlist?");

    }else if(status == 'applicantlist') {

      jQuery('#confirmationText').html("Do you want to view the applicants list?");
    
    } else if(status == 'review') {

      jQuery('#confirmationText').html("Do you want to edit?");
    } else if(status == 'tasklist') {
      jQuery('#confirmationText').html("Do you want to redirect to tasklist page?");
    } else if(status == 'started') {

      jQuery('#confirmationText').html("Are you ready to deposite your budget amount before going to start the job?");

    } else if(status == 'jobberator_rejected') {
      jQuery('#confirmationText').html("Do you want to reject?");
    } else if(status == 'renew') {

      jQuery('#confirmationText').html("Do you want to renew?");
    } else if(status == 'edit') {

      jQuery('#confirmationText').html("Do you want to edit?");
    } else if(status == 'scopeincrease'){
  
      jQuery('#confirmationText').html("Do you want to increase scope?");
    } else if(status == 'publish'){
  
      jQuery('#confirmationText').html("Do you want to publish?");
    }  else if (status == 'continueadvt') {

      jQuery('#confirmationText').html("Do you want to continue your adverticement?");
    }

    jQuery('#confirmationPopup').modal('show');
  }

  continueAdverticement(jobPostId : any) {
    this.apiService.continueAdverticement(jobPostId).subscribe((response: any) => { 
        let usertype = localStorage.getItem('user_type'); 
        if (usertype == "jobberator" || usertype == "both") {
          this.redirectToBySkip('/myjobs');
        } else {
          this.redirectToBySkip('/myjobberjobs');          
        }           
      },
      error => {
      }
    );    
  }  

  confirmationListener(value: any) {
    jQuery('#confirmationPopup').modal('hide');
    jQuery('#confirmationPopupNotStart').modal('hide');
    if (value == 'activereload') {
      this.myJobsActiveLists(); 
      return;    
    }
   switch(this.status)
   {
      case 'views': 
      //this.redirectTo('/');
      break;

      case 'applicantlist':
        this.redirectTo('/jobapplicants/'+ this.applicantsId)
        break;

      case 'review' :
        this.redirectTo('/jobpost/'+this.applicantsId+'/edit')
        //this.redirectTo('/jobpost/'+this.applicantsId)
      break;

      case 'renew' :
        this.redirectTo('/jobpost/'+this.applicantsId+'/renew')
      break;

      case 'edit' :
        this.redirectTo('/jobpost/'+this.applicantsId+'/edit')
      break;

      case 'publish' :
        this.redirectTo('/jobpost/'+this.applicantsId+'/renew')
      break;      

      case 'scopeincrease' :
        this.redirectTo('/scopeincrease/'+this.applicantsId)
      break;

      case 'continueadvt' :
        this.redirectTo('/newadvert/'+this.applicantsId)
      break;

      case 'tasklist' :
        this.redirectTo('/jobberatortasklist/'+this.applicantsId)
        break;
      case 'jobberator_rejected' :
        this.closeJob( this.applicantsId,this.customersID, this.status)
        break;

      case 'started' :
      // this.acceptJob(this.applicantsId,this.customersID, this.status)
      var data = {
        "jobpost_id" : this.applicantsId,
        "customer_id" : this.customersID        
      }      
      this.jobpostcalls.startCheckoutInit(data).subscribe((res) => {
          if (res.status == 'failed') {
            jQuery('#confirmationPopupNotStart').modal('show');
          } else {
            this.budget = res.budget;        
            this.checkoutId = res.data.id;
            let budget: any;
            budget = parseFloat(this.budget);  
            if (budget > -1 && budget < 101) {
              this.serviceFee = res.other.jobberator_commission1;
            }
            if (budget > 100 && budget < 501) {
              this.serviceFee = res.other.jobberator_commission2;          
            }
            if (budget > 500 && budget < 1001) {
              this.serviceFee = res.other.jobberator_commission3;        
            }
            if (budget > 1000 && budget < 5001) {
              this.serviceFee = res.other.jobberator_commission4;       
            }
            if (budget > 5000 && budget < 10001) {
              this.serviceFee = res.other.jobberator_commission5;          
            }
            if (budget > 10000) {
              this.serviceFee = res.other.jobberator_commission6;          
            }
            this.bankFee = res.other.jobberator_bank_fee;
            this.peachFee = res.other.jobberator_peachpay_fee;
            let serviceCharge: any;
            serviceCharge = this.budget * (this.serviceFee / 100);
            this.serviceCharge = serviceCharge.toFixed(2);
            // this.serviceCharge = serviceCharge;
            this.totalamount = parseFloat(this.budget) + parseFloat(this.bankFee) + parseFloat(this.peachFee) + parseFloat(serviceCharge);
            this.loadAPI = new Promise((resolve) => {
              console.log('resolving promise...');
              this.loadScript();
            });   
            this.shopperResultUrl = environment.callback_url+'paycallbackjobstart/'+this.applicantsId+'/'+this.customersID+'/'+this.status;                 
            this.showPayment = true;
          }
  
      }, error => {
        console.log('error');
        console.log(error);
      }
      );      
      break;

      case 'continueadvt' :
      this.continueAdverticement(this.applicantsId)
      break;      

        
   }
   
  }

  jobHistory(job_id: any) {
    var param = {
      "jobpost_id" : job_id,
      "jobberator_id" : localStorage.getItem('user_id')       
    }      
    this.apiService.jobHistory(param)
    .subscribe(
      (data: any) => {
        this.noticount = data.data.count;
        this.notificationsList = data.data.notification_info;
        this.notificationsList.forEach((value: any, key: any) => {

          if (value['status'] == 'jobberator_accepted') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = " accepted "+value['jobber_details']['first_name']+" "+value['jobber_details']['last_name']+"'s job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }
          if (value['status'] == 'jobberator_rejected') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = " rejected "+value['jobber_details']['first_name']+" "+value['jobber_details']['last_name']+"'s job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          } 
          if (value['status'] == 'jobberator_approved') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = " approved your job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }
          if (value['status'] == 'jobberator_disapproved') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = " disapproved your job application";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          } 
          if (value['status'] == 'jobberator_disputed') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = " disputed your job done";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          } 
          
          if (value['status'] == 'started') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = " started job";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }

          if (value['status'] == 'jobber_accepted') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];               
            this.notificationsList[key]['comment'] = " accepted your job acceptance";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }
          if (value['status'] == 'jobber_rejected') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = " rejected your job acceptance";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }  
          if (value['status'] == 'jobber_disputed') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = " disputed your job done";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }
                   
          
        });  
        console.log("notificationlist",this.notificationsList);         
      },
      error => {
        console.log(error);
      }
    );    
  }

  //Go to jog details page
  doGotoJobDetailsPage(jobid: any){
    this.router.navigate(['/jobdetails'],{ queryParams: { job_id: jobid }});
    return false;
  } 
  
  onKeyUp(x: any) { 
    let totalout = 0;
    let totalin = 0;  
    this.jobberMoneyList = []; 
    // this.jobberMoneyList = this.jobberMoneyTempList;
    this.jobberMoneyTempList.forEach((value: any, key: any) => {
      if (value['job']['job_title'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['type'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['amount'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['payment_id'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['paystatus'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['paydate'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1
          || value['job_id'].toLowerCase().indexOf(x.target.value.toLowerCase()) !== -1) {
            if (value['type'] == 'job amount returned') {
              totalin = totalin + parseFloat(value['amount']);
              totalin.toFixed(2);
            } else {
              totalout = totalout + parseFloat(value['amount']);
              totalout.toFixed(2);
            }
            this.jobberMoneyList[key] = value;
      }
    });
    this.totalout = totalout; 
    this.totalin = totalin;   
  }  

}
