import { Pipe, PipeTransform } from "@angular/core";



@Pipe({
  name: 'toIso'
})
export class ToIsoPipe implements PipeTransform {
  transform(value: string): unknown {
    if (value) {
      let regex=/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9]) (?:([0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/;
    
      let parts=value.replace(regex,"$1 $2 $3 $4 $5 $6").split(' ');
      
      let converted = new Date(parseInt(parts[0]),parseInt(parts[1])-1,parseInt(parts[2]),parseInt(parts[3]),parseInt(parts[4]),parseInt(parts[5]));
      
      let newValue = converted.toISOString();
      
      return newValue;
    } else {
      return '';
    }
  }
}