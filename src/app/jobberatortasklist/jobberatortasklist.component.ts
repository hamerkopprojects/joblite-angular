import { Component, OnInit } from '@angular/core';
import { ApiService } from '../_services/commonlisting/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { FileUploader, FileLikeObject } from 'ng2-file-upload';

declare var jQuery: any;



@Component({
  selector: 'app-jobberatortasklist',
  templateUrl: './jobberatortasklist.component.html',
  styleUrls: ['./jobberatortasklist.component.css']
})
export class JobberatortasklistComponent implements OnInit {

  jobPostId: any;
  custId: any;  
  taskname: any = [];
  taskList: any = [];
  tasklist1: any = [];
  allData: any = [];
  cancellationData: any = [];  
  jobExpiryData: any = [];  
  jobposttasklist: any;
  checkIfStart: boolean = false;
  progressPercentage: any;
  progressPercentageStyle: any;  
  taskID: any;
  status: any;
  count: any;
  noticount: any;
  notificationsList: any;
  documentsList: any;
  docImagePopup: any;  
  disputeStatus: boolean;
  rejectStatus: boolean;
  currentTaskId: any;
  openChatStatus: boolean; 
  userType: string;
  chattext: any;  
  chatlist: any;
  jobId: any;  
  public uploader: FileUploader = new FileUploader({});    

  constructor(private apiService: ApiService,
              private router: Router,     
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.uploader.onAfterAddingFile = (file) => {
      this.setPreview(file);
     };      
    this.documentsList = [];  
    this.disputeStatus = false;
    this.rejectStatus = false;
    this.openChatStatus = false;
    this.progressPercentage = 0;
    this.progressPercentageStyle = 'width:0%';    
    this.jobPostId = this.route.snapshot.params.id;
    this.custId = localStorage.getItem('user_id'); 
    this.chattext = '';
    this.chatlist = [];       
    this.userType = localStorage.getItem('user_type');
    this.jobId = '';
    this.jobTaskListDetails();
    this.cancellationReasons();
    this.chatList();    
  }

  chatList() {
    this.apiService.chatList(this.jobPostId).subscribe((res) => {
      this.chatlist = res.chats;
    }, error => {
    }
    );      
  }

  chating() {
    console.log(this.jobPostId);
    console.log(this.custId);
    console.log(this.chattext);
    this.apiService.chat(this.jobPostId, this.custId, this.chattext).subscribe((response: any) => {
      this.chattext = '';

      this.apiService.chatList(this.jobPostId).subscribe((res) => {
        this.chatlist = res.chats;
      }, error => {
      }
      );      
    },
      error => {

      }
    );    
  }  

  cancellationReasons() {
    this.apiService.cancellationReasons().subscribe((response: any) => {
      this.cancellationData = response.cancellationreasons;
   
    },

      error => {
        //this.redirectTo('/');
      }
    );

  }  

  currentTaskIdSet(taskId: any) {
    this.currentTaskId = taskId;
  }

  public setPreview(file:any) {
    console.log("file",file);
    file.withCredentials = false;
    let fr = null;
    fr = new FileReader();
    fr.onload = () => {
      //console.log(fr.result);
      //this.profileImg = fr.result;
    }
    fr.readAsDataURL(file._file);
    this.upload();
  }

  getFiles(): FileLikeObject[] {
    return this.uploader.queue.map((fileItem) => {
      return fileItem.file;
    });
  }  

  upload() {
    let filestemp = this.getFiles();
    let file = filestemp[filestemp.length - 1];    
    // files.forEach((file,index) => {
      let formData = new FormData();
      console.log(file.name);
      formData.append('images', file.rawFile, file.name);
      this.apiService.taskdocupload(formData, this.currentTaskId).subscribe((res) => {
        console.log('success');
        console.log(res);
        jQuery('#exampleModalCenter').modal('hide');
        this.jobTaskListDetails();        
  
      }, error => {
        jQuery('#exampleModalCenter').modal('hide');
        this.jobTaskListDetails();        
      }
      );
    // });
  }    

  jobTaskListDetails() {
    var data = {
      "jobpost_id": this.jobPostId
    }
    this.taskList = [];
    this.apiService.jobTaskListDetails(data).subscribe((response: any) => {
      this.jobId = response.jobid;
      this.allData = response.tasks;
      console.log("alldata",this.allData)
      this.taskList = response.tasks.tasks;
      // console.log("joberatortasklist", this.taskList);
      let doneCount = 0;
      this.tasklist1 = [];
      for (let i = 0; i < this.taskList.length; i++) {
        if (this.taskList[i].status == "jobberator_accepted") {
          doneCount = doneCount + 1;
        }        

        if (this.taskList[i].status == "jobberator_rejected") {
            let splitmsg = this.taskList[i].message.split('$^&*#@');
            this.taskList[i].messagefirst = splitmsg[0];
            this.taskList[i].messagesecond = splitmsg[1];            
        }

        if (this.taskList[i].status == "jobber_disputed") {
          this.taskList[i].messagefirst = this.taskList[i].message;
        }

          this.tasklist1.push(this.taskList[i])
          // console.log(this.tasklist1)
        // }
      }
      this.progressPercentage = (doneCount / this.taskList.length) * 100;
      this.progressPercentageStyle = 'width:'+ this.progressPercentage +'%';      
    },

      error => {
        //this.redirectTo('/');
      }
    );

  }

  jobberatorActionChange(taskId: any, status: any, count: any, message: any) {
    this.jobTaskStatusChange(taskId, status, message);

  }

  jobTaskStatusChange(taskId: any, status: any, message: any) {
    this.apiService.jobTaskStatusChange(taskId, status, message).subscribe((response: any) => {
      if (status == 'jobber_accepted_job_cancellation') {
        let usertype = localStorage.getItem('user_type'); 
        if (usertype == "jobberator" || usertype == "both") {
          this.redirectTo('/myjobs/t3');
        } else {
          this.redirectTo('/myjobberjobs');          
        } 
        
      } else if(status == 'jobberator_accepted_cant_reach') {  
        this.userType = localStorage.getItem('user_type');
        this.apiService.jobExpiryDetails(this.jobPostId).subscribe((response: any) => { 
          this.jobExpiryData = response.jobpostInfo.data;
          let expiredStatus = response.jobpostInfo.data.expired_status;
          if (expiredStatus == 'not_expired') {
            if (response.applicants == 'yes') {
              jQuery('#notExpiredText').html("You have "+this.jobExpiryData.advert_valid_upto_days+" days left. Either you can continue adverticement or you can continue with any existing applicant.");        
              jQuery('#notExpiredPopup').modal('show'); 
            }
            if (response.applicants == 'no') {
              jQuery('#notExpiredTextNoApplicant').html("You have "+this.jobExpiryData.advert_valid_upto_days+" days left. Either you can continue adverticement or you can continue with any existing applicant.");        
              jQuery('#notExpiredPopupNoApplicant').modal('show'); 
            }

          } else {
            jQuery('#expiredText').html("Either you can renew adverticement or you can continue with any existing applicant.");         
            jQuery('#expiredPopup').modal('show'); 
          }
          console.log(response.jobpostInfo.data);
          },
          error => {
            this.jobTaskListDetails();
          }
        );                  
      } else {
        if (response.redirect) {
          let usertype = localStorage.getItem('user_type'); 
          if (usertype == "jobberator" || usertype == "both") {
            this.redirectTo('/myjobs/t3');
          } else {
            this.redirectTo('/myjobberjobs');          
          }      
  
        } else {
          this.jobTaskListDetails();        
        }        
      }
    },
      error => {
        this.jobTaskListDetails();
      }
    );

  }

  jobberatorCancelOptions(taskId: any, count: any) {
    this.taskID = taskId;
    this.count = count;
    jQuery('#jobberatorTextDisp').html("Cancel task or cancel job?");
    //jQuery('#jobberatorDetailTextDisp').html("If you cancel this task jobber will get three options 1. Accept(Then you need to give 50% of the task) 2. Redo(Jobber can redo the task) 3. Can't reach agreement(Jobber can request 100% for all done task and 50% of the last task completed). If you cancel the job jobber will get two options 1. Accept(Jobber can request 100% for all done task and 50% of the last task completed) 2. Redo(Jobber can redo the task))");
    jQuery('#dispJobberatorDispute').modal('show');     
  }  

  jobberatorActionConfirmation(taskId: any, status: any, count: any) {
    this.taskID = taskId;
    this.status = status;
    this.count = count;
    this.disputeStatus = false;
    this.rejectStatus = false;
    if(status == 'jobberator_accepted'){
      jQuery('#confirmationText').html("Do you want to Accept?");

    }else if(status == 'jobberator_rejected') {
      this.rejectStatus = true;
      jQuery('#rejectConfirmationText').html("Do you want to Reject?");

    }else if(status == 'jobberator_disputed') {
      this.disputeStatus = true;
      jQuery('#confirmationTextDisp').html("Do you want to Dispute?");

    }

    if(status == 'jobberator_accepted_cant_reach'){
      jQuery('#confirmationText').html("If you accept can't reach agreement of jobber then jobber will get 100% of all the completed tasks amount and 50% of the last disputed task and you can advertise on the platform or find new jobber from applicants.");

    }

    if (!this.disputeStatus) {
      if (this.rejectStatus) {
        jQuery('#rejectConfirmationPopup').modal('show');        
      } else {
        jQuery('#confirmationPopup').modal('show');
      }      
    } else {
      jQuery('#dispConfirmationPopup').modal('show');     
    }
    
  }

  confirmationListener(value:any) {
    jQuery('#confirmationPopup').modal('hide');
    jQuery('#dispConfirmationPopup').modal('hide'); 
    jQuery('#dispJobberatorDispute').modal('hide');   
    jQuery('#rejectConfirmationPopup').modal('hide'); 
    jQuery('#notExpiredPopup').modal('hide'); 
    jQuery('#expiredPopup').modal('hide');               
    if (value == 'jobberator_canceled' ||
        value == 'jobberator_job_canceled' ||
        value == 'jobberator_accepted' ||
        value == 'continue_advt' ||
        value == 'continue_applicant' ||
        value == 'continue_advt_first' ||
        value == 'continue_applicant_first' ||
        value == 'renew_advt') {
      this.status = value;
      value = '';
    }    
   switch(this.status) {
     case 'jobberator_accepted' :
      this.jobberatorActionChange(this.taskID,this.status,this.count,value)
      break;
      case 'jobberator_rejected' :
      this.jobberatorActionChange(this.taskID,this.status,this.count,value)
      break;
      case 'jobberator_disputed' :
        this.jobberatorActionChange(this.taskID,this.status,this.count,value)
      break;
      case 'jobberator_canceled' :
        this.jobberatorActionChange(this.taskID,this.status,this.count,value)
      break;  
      case 'jobberator_accepted_cant_reach' :
        this.jobberatorActionChange(this.taskID,this.status,this.count,value)
      break;  
      case 'jobberator_job_canceled' :
        this.jobberatorActionChange(this.taskID,this.status,this.count,value)
      break;
      case 'continue_advt_first' :
        jQuery('#continueAdvtPopup').modal('show');
      break;
      case 'continue_applicant_first' :
        jQuery('#continueApplicantPopup').modal('show');
      break;
      case 'renew_advt_first' :
        jQuery('#renewAdvtPopup').modal('show');
      case 'continue_advt' :
        jQuery('.modal-backdrop').remove();
        jQuery('#notExpiredPopup').modal('hide'); 
        jQuery('#expiredPopup').modal('hide'); 
        jQuery('#continueAdvtPopup').modal('hide'); 
        jQuery('#continueApplicantPopup').modal('hide');
        setTimeout(() => {
          this.redirectTo('/newadvert/'+this.jobPostId);
        }, 500)
      break;
      case 'continue_applicant' :
        jQuery('.modal-backdrop').remove();
        jQuery('#notExpiredPopup').modal('hide'); 
        jQuery('#expiredPopup').modal('hide'); 
        jQuery('#continueAdvtPopup').modal('hide'); 
        jQuery('#continueApplicantPopup').modal('hide');
        //this.redirectTo('/jobapplicants/'+ this.jobPostId);

        setTimeout(() => {
          this.redirectTo('/continueapplicant/'+ this.jobPostId);
        }, 500)
      break;
      case 'renew_advt' :
        // this.redirectTo('/jobpost/'+this.jobPostId);
        jQuery('.modal-backdrop').remove();
        jQuery('#notExpiredPopup').modal('hide'); 
        jQuery('#expiredPopup').modal('hide'); 
        jQuery('#continueAdvtPopup').modal('hide'); 
        jQuery('#continueApplicantPopup').modal('hide');
        setTimeout(() => {
          this.redirectTo('/renewadvert/'+this.jobPostId);
        }, 500)
      break;                                 
   }
   
    
  
  }

  chatOpen() {
    this.openChatStatus = true;
  }

  chatClose() {
    this.openChatStatus = false;
  }  


  invokeNotification(taskID:any) {
    jQuery('#tasklistNotification').modal('show');
    var param = {
      "task_id" : taskID
    }      
    this.apiService.taskHistory(param)
    .subscribe(
      (data: any) => {
        this.noticount = data.data.count;
        this.notificationsList = data.data.notification_info;
        this.notificationsList.forEach((value: any, key: any) => {

          if (value['status'] == 'started') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name']; 
            this.notificationsList[key]['comment'] = value['jobber_details']['last_name']+" started the task";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          } 
          
          if (value['status'] == 'jobberator_disputed') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" disputed this task";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }
          
          if (value['status'] == 'jobber_disputed') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = value['jobber_details']['last_name']+" disputed this task";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }    
          
          if (value['status'] == 'jobberator_rejected') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" rejected task";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }          

          if (value['status'] == 'jobberator_accepted') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" accepted task";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }

          if (value['status'] == 'jobber_accepted') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobber_details']['last_name']+" accepted task";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }
          if (value['status'] == 'jobber_rejected') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = value['jobber_details']['last_name']+" rejected task";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }          
 
          if (value['status'] == 'completed') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" completed task";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }
   
        });  
        console.log("notificationlist",this.notificationsList);         
      },
      error => {
        console.log(error);
      }
    ); 

  }


  documentHistory(taskID:any) {
    jQuery('#taskDocuments').modal('show');
    var param = {
      "task_id" : taskID
    }      
    this.apiService.taskDocs(param)
    .subscribe(
      (data: any) => {
        this.documentsList = data.data.notification_info;
        this.documentsList.forEach((value: any, key: any) => {

            this.documentsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name']; 
            this.documentsList[key]['comment'] = value['jobber_details']['last_name']+" uploaded a document";
            if (value['path'] == null) {
              this.documentsList[key]['path'] = 'assets/images/slider_04.png';
            } else {
              this.documentsList[key]['path'] = environment.server_url+'' +value['path'];
            }              

   
        });  
        console.log("notificationlist",this.documentsList);         
      },
      error => {
        console.log(error);
      }
    ); 

  }
  
  imagePopup(path: any) {
    this.docImagePopup = path;
    jQuery('#docPopup').modal('show');
    return false;
  }

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;
  }  

}
