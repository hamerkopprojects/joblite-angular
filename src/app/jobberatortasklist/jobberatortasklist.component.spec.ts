import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobberatortasklistComponent } from './jobberatortasklist.component';

describe('JobberatortasklistComponent', () => {
  let component: JobberatortasklistComponent;
  let fixture: ComponentFixture<JobberatortasklistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobberatortasklistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobberatortasklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
