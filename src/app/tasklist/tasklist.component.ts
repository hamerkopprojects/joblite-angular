import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from '../_services/commonlisting/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { FileUploader, FileLikeObject } from 'ng2-file-upload';

declare var jQuery: any;



@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.css']
})
export class TasklistComponent implements OnInit {
  @ViewChild('fileInput')
  fileInputVariable: ElementRef;
  jobPostId: any;
  custId: any;
  taskname: any = [];
  taskList: any = [];
  allData: any = [];
  jobposttasklist: any;
  checkIfStart: boolean = false;
  progressPercentage: any;
  progressPercentageStyle: any;
  taskID: any;
  status: any;
  count: any;
  jobberTaskUserStatus: string;
  noticount: any;
  notificationsList: any;
  documentsList: any;
  docImagePopup: any;
  disputeStatus: boolean;
  currentTaskId: any; 
  openChatStatus: boolean; 
  chattext: any;  
  chatlist: any;
  doingcount: number;
  jobId: any;
  public uploader: FileUploader = new FileUploader({});  



  constructor(private apiService: ApiService,
    private router: Router,    
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.jobberTaskUserStatus = localStorage.getItem('jobber_task_user_status');
    this.uploader.onAfterAddingFile = (file) => {
      this.setPreview(file);
     };  
    this.documentsList = [];   
    this.disputeStatus = false;
    this.openChatStatus = false;
    this.progressPercentage = 0;
    this.progressPercentageStyle = 'width:0%';
    this.jobPostId = this.route.snapshot.params.id;
    this.custId = localStorage.getItem('user_id');
    this.docImagePopup = '';
    this.chattext = '';
    this.chatlist = [];
    this.doingcount = 0;
    this.jobId = '';
    this.jobTaskListDetails();
    this.chatList();
  }

  chatList() {
    this.apiService.chatList(this.jobPostId).subscribe((res) => {
      this.chatlist = res.chats;
    }, error => {
    }
    );      
  }

  chating() {
    console.log(this.jobPostId);
    console.log(this.custId);
    console.log(this.chattext);
    this.apiService.chat(this.jobPostId, this.custId, this.chattext).subscribe((response: any) => {
      this.chattext = '';

      this.apiService.chatList(this.jobPostId).subscribe((res) => {
        this.chatlist = res.chats;
      }, error => {
      }
      );      
    },
      error => {

      }
    );    
  }

  currentTaskIdSet(taskId: any) {
    this.currentTaskId = taskId;
    console.log(this.fileInputVariable.nativeElement.files);
    // this.fileInputVariable.nativeElement.files = [];
    this.fileInputVariable.nativeElement.value = [];
    console.log(this.fileInputVariable.nativeElement.files);    
  }

  public setPreview(file:any) {
    console.log("file",file);
    file.withCredentials = false;
    let fr = null;
    fr = new FileReader();
    fr.onload = () => {
      //console.log(fr.result);
      //this.profileImg = fr.result;
    }
    fr.readAsDataURL(file._file);
    this.upload();
  }

  getFiles(): FileLikeObject[] {
    return this.uploader.queue.map((fileItem) => {
      return fileItem.file;
    });
  }  

  upload() {
    let filestemp = this.getFiles();
    let file = filestemp[filestemp.length - 1];
    // files.forEach((file,index) => {
      let formData = new FormData();
      console.log(file.name);
      formData.append('images', file.rawFile, file.name);
      this.apiService.taskdocupload(formData, this.currentTaskId).subscribe((res) => {
        console.log('success');
        console.log(res);
        jQuery('#exampleModalCenter').modal('hide');
        this.jobTaskListDetails();        
  
      }, error => {
        jQuery('#exampleModalCenter').modal('hide');
        this.jobTaskListDetails();        
      }
      );
    // });
  }  

  jobTaskListDetails() {
    var data = {
      "jobpost_id": this.jobPostId
    }
    this.taskList = [];
    this.apiService.jobTaskListDetails(data).subscribe((response: any) => {
      this.jobId = response.jobid;
      this.allData = response.tasks;
      this.taskList = response.tasks.tasks;
      this.doingcount = response.doingcount;
      let doneCount = 0;
      console.log("task", this.taskList);
      for (let i = 0; i < this.taskList.length; i++) {
        if (this.taskList[i].status == "jobberator_accepted") {
          doneCount = doneCount + 1;
        }
        switch (this.taskList[i].status) {
          case "started":
            this.checkIfStart = true;
            break;
          case "completed":
            this.checkIfStart = true;
            break;
          case "jobberator_rejected":
            let splitmsg = this.taskList[i].message.split('$^&*#@');
            this.taskList[i].messagefirst = splitmsg[0];
            this.taskList[i].messagesecond = splitmsg[1];            
            this.checkIfStart = true;
            break;
          case "jobber_disputed":
            this.taskList[i].messagefirst = this.taskList[i].message;
            this.checkIfStart = true;
            break;
        }
      }
      this.progressPercentage = (doneCount / this.taskList.length) * 100;
      this.progressPercentageStyle = 'width:'+ this.progressPercentage +'%';
    },

      error => {
        //this.redirectTo('/');
      }
    );

  }
  jobActionChange(taskId: any, status: any, count: any, message: any) {

    this.jobTaskStatusChange(taskId, status, message);
    this.taskList[count].status = status;
  }
  jobTaskStatusChange(taskId: any, status: any, message: any) {
    this.apiService.jobTaskStatusChange(taskId, status, message).subscribe((response: any) => {
      // if (status == 'jobber_accepted_job_cancellation' || status == 'jobberator_accepted_cant_reach') {
      if (response.redirect) {
        let usertype = localStorage.getItem('user_type'); 
        // if (usertype == "jobberator" || usertype == "both") {
        //   this.redirectTo('/myjobs');
        // } else {
          this.redirectTo('/myjobberjobs/t3');          
        // }      

      } else {
        this.jobTaskListDetails();        
      }

    },
      error => {
        this.jobTaskListDetails();

      }
    );

  }
  jobActionConfirmation(taskId: any,status: any,count: any) {
    this.taskID = taskId;
    this.status = status;
    this.count = count;
    this.disputeStatus = false;    
    if(status == 'started'){
      jQuery('#confirmationText').html("Do you want to Start?");

    }else if(status == 'completed') {
      jQuery('#confirmationText').html("Do you want to Finish?");

    }else if(status == 'jobber_disputed') {
      this.disputeStatus = true;      
      jQuery('#confirmationTextDisp').html("Do you want to Dispute?");

    }
    if (status == 'jobber_accept_cancellation') {
      jQuery('#confirmationText').html("If you accept cancellation of jobberator you will get 50% of the task amount and can countinue with next task.");
    }
    if (status == 'jobber_cant_reach_agreement') {
      jQuery('#confirmationText').html("If you can't reach agreement you will get 100% amont of completed task and 50% of the last task.");
    }    
    if (status == 'jobber_accepted_job_cancellation') {
      jQuery('#confirmationText').html("You can accept the job cancellation then you get paid 100% payment of what has been completed and approved and 50% of the last task completed.");
    }     
    if (!this.disputeStatus) {
      jQuery('#confirmationPopup').modal('show');
    } else {
      jQuery('#dispConfirmationPopup').modal('show');     
    }
  }
  confirmationListener(value:any) {
    console.log(value);
    jQuery('#confirmationPopup').modal('hide');
    jQuery('#dispConfirmationPopup').modal('hide');     
   switch(this.status) {
     case 'started' :
      this.jobActionChange(this.taskID,this.status,this.count,value)
      break;
    case 'completed' :
      this.jobActionChange(this.taskID,this.status,this.count,value)
      break;
    case 'jobber_disputed' :
      this.jobActionChange(this.taskID,this.status,this.count,value)
      break;
    case 'jobber_accept_cancellation' :
      this.jobActionChange(this.taskID,this.status,this.count,value)
      break;
    case 'jobber_cant_reach_agreement' :
      this.jobActionChange(this.taskID,this.status,this.count,value)
      break; 
    case 'jobber_accepted_job_cancellation' :
      this.jobActionChange(this.taskID,this.status,this.count,value)
      break;            
   }
    
  
  }

  chatOpen() {
    this.openChatStatus = true;
  }

  chatClose() {
    this.openChatStatus = false;
  }  

  invokeNotification(taskID:any) {
    jQuery('#tasklistNotification').modal('show');
    var param = {
      "task_id" : taskID
    }      
    this.apiService.taskHistory(param)
    .subscribe(
      (data: any) => {
        this.noticount = data.data.count;
        this.notificationsList = data.data.notification_info;
        this.notificationsList.forEach((value: any, key: any) => {

          if (value['status'] == 'started') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name']; 
            this.notificationsList[key]['comment'] = value['jobber_details']['last_name']+" started the task";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          } 
          
          if (value['status'] == 'jobberator_disputed') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" disputed this task";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }
          
          if (value['status'] == 'jobber_disputed') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = value['jobber_details']['last_name']+" disputed this task";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }    
          
          if (value['status'] == 'jobberator_rejected') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" rejected task";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }          

          if (value['status'] == 'jobberator_accepted') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name']; 
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" accepted task";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }

          if (value['status'] == 'jobber_accepted') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobber_details']['last_name']+" accepted task";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }
          if (value['status'] == 'jobber_rejected') {
            this.notificationsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name'];              
            this.notificationsList[key]['comment'] = value['jobber_details']['last_name']+" rejected task";
            if (value['jobber_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobber_details']['profile_img'];
            }              
          }          
 
          if (value['status'] == 'completed') {
            this.notificationsList[key]['username'] = value['jobberator_details']['first_name']+" "+value['jobberator_details']['last_name'];               
            this.notificationsList[key]['comment'] = value['jobberator_details']['last_name']+" completed task";
            if (value['jobberator_details']['profile_img'] == null) {
              this.notificationsList[key]['profile_img'] = 'assets/images/slider_04.png';
            } else {
              this.notificationsList[key]['profile_img'] = environment.server_url+'' +value['jobberator_details']['profile_img'];
            }              
          }
   
        });  
        console.log("notificationlist",this.notificationsList);         
      },
      error => {
        console.log(error);
      }
    ); 

  }

  documentHistory(taskID:any) {
    jQuery('#taskDocuments').modal('show');
    var param = {
      "task_id" : taskID
    }      
    this.apiService.taskDocs(param)
    .subscribe(
      (data: any) => {
        this.documentsList = data.data.notification_info;
        this.documentsList.forEach((value: any, key: any) => {

            this.documentsList[key]['username'] = value['jobber_details']['first_name']+" "+value['jobber_details']['last_name']; 
            this.documentsList[key]['comment'] = value['jobber_details']['last_name']+" uploaded a document";
            if (value['path'] == null) {
              this.documentsList[key]['path'] = 'assets/images/slider_04.png';
            } else {
              this.documentsList[key]['path'] = environment.server_url+'' +value['path'];
            }              

   
        });  
        console.log("notificationlist",this.documentsList);         
      },
      error => {
        console.log(error);
      }
    ); 

  }
  
  imagePopup(path: any) {
    this.docImagePopup = path;
    jQuery('#docPopup').modal('show');
    return false;
  }

  redirectTo(uri:string){
    // this.router.navigateByUrl('/temp', {skipLocationChange: false}).then(()=>
    // this.router.navigate([uri]));
    //this.childModal.hide();
    this.router.navigate([uri]);
    return false;
  }  

}
